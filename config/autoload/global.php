<?php
return array(
    'db' => array(
        'driver' => 'Pdo',
        'username' => 'global',
        'password' => 'Global',
        'dsn' => 'mysql:dbname=sfe-wm2a;host=localhost',
        'driver_options' => array(
            1002 => 'SET NAMES \'UTF8\'',
        ),
        'adapters' => array(
            'Db\\wm2asfe' => array(),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\\Db\\Adapter\\Adapter' => 'Zend\\Db\\Adapter\\AdapterServiceFactory',
        ),
    ),
    'zf-mvc-auth' => array(
        'authentication' => array(
            'map' => array(
                'Factelectronica\\V1' => 'sfe',
            ),
        ),
    ),
);

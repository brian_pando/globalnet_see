$(document).ready(function(){
    // script para los select provincias
    $("#departamento").change(function(){
        $.ajax({
            url: 'asociarUbigeo',
            type: 'Post',
            data: {id_depar:$("#departamento").val()},
            success: function (data, textStatus, xhr) {
                $("#provincia").find('option').remove().end();
                $("#provincia").append('<option value="0"> --- Seleccione Provincia --- </option>');
                $.each(data, function(indice,valor){
                    console.log(valor.departamentoId + valor.departamento);
                    $("#provincia").append('<option value="'+valor.provinciaId+'">'+valor.provincia+'</option>');
                });
            },
            error: function(xhr, textstatus, errorThrown){

            }
        });
    });

   // script para los select localidades
    $("#provincia").change(function(){
        $.ajax({
            url: 'asociarUbigeo',
            type: 'Post',
            data: {id_depar:$("#departamento").val(), id_prov:$("#provincia").val()},
            success: function (data, textStatus, xhr) {
                $("#distrito").find('option').remove().end();
                $("#distrito").append('<option value="0"> --- Seleccione Distrito --- </option>');
                $.each(data, function(indice,valor){
                    console.log(valor.provinciaId + valor.provincia);
                    $("#distrito").append('<option value="'+valor.distritoId+'">'+valor.distrito+'</option>');
                });
            },
            error: function(xhr, textstatus, errorThrown){

            }
        });
    });
});
var FormComponents = function () {

    var handleMultiSelect = function () {
        $('#my_multi_select1').multiSelect();
        $('#my_multi_select2').multiSelect();
        $('#my_multi_select3').multiSelect();
        $('#my_multi_select4').multiSelect();
        $('#my_multi_select5').multiSelect();
        $('#my_multi_select6').multiSelect();
        $('#my_multi_select7').multiSelect({
            selectableOptgroup: true
        });
    };
    
    var handleSelect2 = function () {
        function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='/assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }

        $("#country_list").select2({
            placeholder: "Select",
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleMultiSelect();
            handleSelect2();
        }
    };
}();
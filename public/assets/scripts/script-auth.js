jQuery(document).ready(function () {
    App.init();
    Login.init();
    FormComponents.init();
    var action = location.hash.substr(1);
    if (action == 'createaccount') {
        $('.login-form').hide();
        $('.forget-form').hide();
    } else if (action == 'forgetpassword') {
        $('.login-form').hide();
        $('.forget-form').show();
    }
});
<?php
namespace Factelectronica;

use ZF\Apigility\Provider\ApigilityProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Uri\UriFactory;
use Factelectronica\V1\Rest\Comprobantes\ComprobantesEntity;
use Factelectronica\V1\Rest\Comprobantes\ComprobantesMapper;
use Factelectronica\V1\Rest\Comprobantes\ComprobantesDetalleMapper;
use Factelectronica\V1\Rest\Comprobantes\ComprobantesDetalleEntity;
use Factelectronica\V1\Rest\Comprobantes\EmpresaMapper;
use Factelectronica\V1\Rest\Comprobantes\EmpresaEntity;
use Factelectronica\V1\Rest\Comprobantes\TipoDocumentoMapper;
use Factelectronica\V1\Rest\Comprobantes\TipoDocumentoEntity;
use Factelectronica\V1\Rest\Notas\NotasEntity;
use Factelectronica\V1\Rest\Notas\NotasMapper;
use Factelectronica\V1\Rest\Notas\NotasDetalleEntity;
use Factelectronica\V1\Rest\Notas\NotasDetalleMapper;
use Factelectronica\V1\Rest\Bajas\BajasEntity;
use Factelectronica\V1\Rest\Bajas\BajasMapper;
use Factelectronica\V1\Rest\Bajas\BajasDetalleEntity;
use Factelectronica\V1\Rest\Bajas\BajasDetalleMapper;

class Module implements ApigilityProviderInterface
{

    public function getConfig()
    {                        
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {                
        return array(
            'ZF\Apigility\Autoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__
                )
            )
        );
    }

    public function getServiceConfig()
    {                        
        return array(
            'factories' => array(                
                'ComprobantesTable' => function ($sm) {                    
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ComprobantesEntity());
                    return new TableGateway('comprobanteelectronico', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Comprobantes\ComprobantesMapper' => function ($sm) {
                    $tableGateway = $sm->get('ComprobantesTable');
                    return new ComprobantesMapper($tableGateway);
                },
                
                'ComprobantesDetalleTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ComprobantesDetalleEntity());
                    return new TableGateway('comprobantedetalleelectronico', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Comprobantes\ComprobantesDetalleMapper' => function ($sm) {
                    $tableGateway = $sm->get('ComprobantesDetalleTable');
                    return new ComprobantesDetalleMapper($tableGateway);
                },
                
                'NotasTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new NotasEntity());
                    return new TableGateway('comprobanteelectronico', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Notas\NotasMapper' => function ($sm) {
                    $tableGateway = $sm->get('NotasTable');
                    return new NotasMapper($tableGateway);
                },
                
                'NotasDetalleTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new NotasDetalleEntity());
                    return new TableGateway('comprobantedetalleelectronico', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Notas\NotasDetalleMapper' => function ($sm) {
                    $tableGateway = $sm->get('NotasDetalleTable');
                    return new NotasDetalleMapper($tableGateway);
                },
                
                'DocumentoBajaTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new BajasEntity());
                    return new TableGateway('documentobaja', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Bajas\BajasMapper' => function ($sm) {
                    $tableGateway = $sm->get('DocumentoBajaTable');
                    return new BajasMapper($tableGateway);
                },
                
                'BajaDetalleTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new BajasDetalleEntity());
                    return new TableGateway('documentodetallebaja', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Bajas\BajasDetalleMapper' => function ($sm) {
                    $tableGateway = $sm->get('BajaDetalleTable');
                    return new BajasDetalleMapper($tableGateway);
                },
                
                /*'DocumentoResumenTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ResumenesEntity());
                    return new TableGateway('documentoresumen', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Resumenes\ResumenesMapper' => function ($sm) {
                    $tableGateway = $sm->get('DocumentoResumenTable');
                    return new ResumenesMapper($tableGateway);
                },
                
                'ResumenDetalleTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ResumenesDetalleEntity());
                    return new TableGateway('documentodetalleresumen', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Resumenes\ResumenesDetalleMapper' => function ($sm) {
                    $tableGateway = $sm->get('ResumenDetalleTable');
                    return new ResumenesDetalleMapper($tableGateway);
                }*/
                
                'EmpresaTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new EmpresaEntity());
                    return new TableGateway('empresa', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Comprobantes\EmpresaMapper' => function ($sm) {
                    $tableGateway = $sm->get('EmpresaTable');
                    return new EmpresaMapper($tableGateway);
                },
                
                'TipoDocumentoTable' => function ($sm) {
                    $dbAdapter = $sm->get('Db\wm2asfe');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TipoDocumentoEntity());
                    return new TableGateway('tipodocumentos', $dbAdapter, null, $resultSetPrototype);
                },
                'Factelectronica\V1\Rest\Comprobantes\TipoDocumentoMapper' => function ($sm) {
                    $tableGateway = $sm->get('TipoDocumentoTable');
                    return new TipoDocumentoMapper($tableGateway);
                }
            )
        );
    }
    
    public function onBootstrap(MvcEvent $event)
    {        
        UriFactory::registerScheme('chrome-extension', 'Zend\Uri\Uri');
    }
}

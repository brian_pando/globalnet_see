<?php
namespace Factelectronica\V1\Rest\Bajas;

class BajasDetalleEntity
{
    public  $documentoDetalleBajaId, $documentoBajaId, $tipoDocOrigen, $serieDocOrigen, $correlativoDocOrigen, $motivoBaja, $numeroOrden;
    
    public function getArrayCopy()
    {
        return array(
            'documentoDetalleBajaId' => $this->documentoDetalleBajaId,
            'documentoBajaId'       => $this->documentoBajaId,
            'tipoDocOrigen'         => $this->tipoDocOrigen,
            'serieDocOrigen'        => $this->serieDocOrigen,
            'correlativoDocOrigen'  => $this->correlativoDocOrigen,
            'motivoBaja'            => $this->motivoBaja,
            'numeroOrden'           => $this->numeroOrden,
        );
    }
    
    public function exchangeArray(array $array){
        $this->documentoDetalleBajaId = $array["documentoDetalleBajaId"];
        $this->documentoBajaId      = $array["documentoBajaId"];
        $this->tipoDocOrigen        = $array["tipoDocOrigen"];
        $this->serieDocOrigen       = $array['serieDocOrigen'];
        $this->correlativoDocOrigen = $array["correlativoDocOrigen"];
        $this->motivoBaja           = $array["motivoBaja"];
        $this->numeroOrden          = $array["numeroOrden"];
    }
}

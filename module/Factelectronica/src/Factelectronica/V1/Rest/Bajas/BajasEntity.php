<?php
namespace Factelectronica\V1\Rest\Bajas;

class BajasEntity
{
    public $documentoBajaId, $sucursalId, $codigoIdentificador, $fechaEmision, $fechaDocumentoBaja, $fechaCreacion;
    
    public function getArrayCopy()
    {
        return array(
            'documentoBajaId'       => $this->documentoBajaId,
            'sucursalId'            => $this->sucursalId,
            'codigoIdentificador'   => $this->codigoIdentificador,
            'fechaEmision'          => $this->fechaEmision,
            'fechaDocumentoBaja'    => $this->fechaDocumentoBaja,
            'fechaCreacion'         => $this->fechaCreacion,
        );
    }
    
    public function exchangeArray(array $array){
        $this->documentoBajaId      = $array["documentoBajaId"];
        $this->sucursalId           = $array["sucursalId"];
        $this->codigoIdentificador  = $array['codigoIdentificador'];
        $this->fechaEmision         = $array["fechaEmision"];
        $this->fechaDocumentoBaja   = $array["fechaDocumentoBaja"];
        $this->fechaCreacion        = $array["fechaCreacion"];
    }
}

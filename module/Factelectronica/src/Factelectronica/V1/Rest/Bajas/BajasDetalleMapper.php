<?php
namespace Factelectronica\V1\Rest\Bajas;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Factelectronica\V1\Rest\Bajas\BajasDetalleEntity;
 
class BajasDetalleMapper
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function save(BajasDetalleEntity $detalle){
        $data = [
            'documentoBajaId'       => $detalle->documentoBajaId,
            'tipoDocOrigen'         => $detalle->tipoDocOrigen,
            'serieDocOrigen'        => $detalle->serieDocOrigen,
            'correlativoDocOrigen'  => $detalle->correlativoDocOrigen,
            'motivoBaja'            => $detalle->motivoBaja,
            'numeroOrden'           => $detalle->numeroOrden,
			'codigoOrigen'          => $detalle->codigoOrigen,
        ];
        $id = (int) $detalle->documentoDetalleBajaId;
        if($id == 0){
            $this->tableGateway->insert($data);
        }
    }
    
    public function getDataBajaDetalle($docBajaId){
        $id = (int) $docBajaId;
        $data = $this->tableGateway->select(function (Select $select) use($id){
            $select->columns(array('*'))
                   ->where(array('documentoBajaId' => $id));
        });
        return $data->toArray();
    }
}
<?php
namespace Factelectronica\V1\Rest\Bajas;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Factelectronica\V1\Rest\Bajas\BajasEntity;
use Factelectronica\V1\Rest\Bajas\BajasDetalleEntity;
use Core\Controller\PuenteController;

class BajasResource extends AbstractResourceListener
{
    protected $mapper;
    protected $mapperDet;
    protected $mapperEmp;
    protected $mapperTipoDoc;
    protected $comprobantesMapper;
    
    public function __construct($mapper, $mapperDet, $mapperEmp, $mapperTipoDoc, $comprobantesMapper)
    {
        $this->mapper = $mapper;
        $this->mapperDet = $mapperDet;
        $this->mapperEmp = $mapperEmp;
        $this->mapperTipoDoc = $mapperTipoDoc;
        $this->comprobantesMapper = $comprobantesMapper;
    }
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $bajas = new BajasEntity();
        $bajas->sucursalId       = $data->sucursalId;
        $bajas->codigoIdentificador = $data->codigoIdentificador;
        $bajas->fechaEmision     = $data->fechaEmision;
        $bajas->fechaDocumentoBaja = $data->fechaDocumentoBaja;
        $bajas->fechaCreacion    = date('Y-m-d H:i:s');
        $docBajaId = $this->mapper->save($bajas);
        
        $detalle = $data->detalle;
        $count = count($detalle);
        for($i=0;$i<$count;$i++){
			
			//Para boletas,NC,ND
            $pos      = strripos($detalle[$i]['serieDocOrigen'], 'B');
            if ($pos === false) {
                $this->comprobantesMapper->updateMotivoBaja($detalle[$i]['tipoDocOrigen'],$detalle[$i]['serieDocOrigen'],$detalle[$i]['correlativoDocOrigen'],$detalle[$i]['motivoBaja'],$data->sucursalId);
                //codigo que origen 01
                $codigoOrigen=1;
            } else {
                $this->comprobantesMapper->updateMotivoBajaAndEstado($detalle[$i]['tipoDocOrigen'],$detalle[$i]['serieDocOrigen'],$detalle[$i]['correlativoDocOrigen'],$detalle[$i]['motivoBaja'],$data->sucursalId);
                //codigo que origen 03
                $codigoOrigen=3;
            }
			
            $bajaDetalle = new BajasDetalleEntity();
            $bajaDetalle->documentoBajaId = $docBajaId;
            $bajaDetalle->numeroOrden = $detalle[$i]['numeroOrden'];
            $bajaDetalle->tipoDocOrigen = $detalle[$i]['tipoDocOrigen'];
            $bajaDetalle->serieDocOrigen = $detalle[$i]['serieDocOrigen'];
            $bajaDetalle->correlativoDocOrigen = $detalle[$i]['correlativoDocOrigen'];
            $bajaDetalle->motivoBaja = $detalle[$i]['motivoBaja'];
			$bajaDetalle->codigoOrigen=$codigoOrigen;
            $dataDetalle = $this->mapperDet->save($bajaDetalle);
           // $this->comprobantesMapper->updateMotivoBaja($detalle[$i]['tipoDocOrigen'],$detalle[$i]['serieDocOrigen'],$detalle[$i]['correlativoDocOrigen'],$detalle[$i]['motivoBaja'],$data->sucursalId);
        }
        $puenteController = new PuenteController($this->mapper, $this->mapperDet, $this->mapperEmp, $this->mapperTipoDoc);
        $respuesta = $puenteController->generateBajaXml($docBajaId);
        return $respuesta;
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return $this->mapper->fetchOne($id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return $this->mapper->fetchAll();
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}

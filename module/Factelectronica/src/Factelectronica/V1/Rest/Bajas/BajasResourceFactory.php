<?php
namespace Factelectronica\V1\Rest\Bajas;

class BajasResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Factelectronica\V1\Rest\Bajas\BajasMapper');
        $mapperDet = $services->get('Factelectronica\V1\Rest\Bajas\BajasDetalleMapper');
        $mapperEmp = $services->get('Factelectronica\V1\Rest\Comprobantes\EmpresaMapper');
        $mapperTipoDoc = $services->get('Factelectronica\V1\Rest\Comprobantes\TipoDocumentoMapper');
        $comprobantesMapper = $services->get('Factelectronica\V1\Rest\Comprobantes\ComprobantesMapper');
        return new BajasResource($mapper, $mapperDet, $mapperEmp, $mapperTipoDoc, $comprobantesMapper);
    }
}

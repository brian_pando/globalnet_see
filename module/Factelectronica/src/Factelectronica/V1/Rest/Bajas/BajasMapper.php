<?php
namespace Factelectronica\V1\Rest\Bajas;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Factelectronica\V1\Rest\Bajas\BajasEntity;
 
class BajasMapper
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll(){
        $result = $this->tableGateway->select(function (Select $select){
            $select->columns(array('*'));
        });
        return $result;
    }
    
    public function fetchOne($id){
        $id = (int) $id;
        $result = $this->tableGateway->select(function (Select $select) use($id){
            $select->columns(array('*'))->where(array('documentoBajaId' => $id));
        });
        $data = $result->current();
        if(!$data){
            throw new \Exception("El comprobante con id={$id} no se encuentra");
        }
        return $data;
    }
    
    public function save(BajasEntity $baja){
        $data = [
            'sucursalId'            => $baja->sucursalId,
            'codigoIdentificador'   => $baja->codigoIdentificador,
            'fechaEmision'          => $baja->fechaEmision,
            'fechaDocumentoBaja'    => $baja->fechaDocumentoBaja,
            'fechaCreacion'         => $baja->fechaCreacion,
        ];
        $id = (int) $baja->documentoBajaId;
        if($id == 0){
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        }
    }
    
    public function getDataBaja($documentoBajaId){
        $data = $this->tableGateway->select(function (Select $select) use($documentoBajaId){
            $select->columns(array('*'))->where(array('documentoBajaId' => $documentoBajaId));
        });
        return $data->current();
    }
    
    public function updateComprobanteEstadoSunat($documentoBajaId){
        $data = array('estadoSunat' => 'Enviado a la SUNAT');
        $this->tableGateway->update($data, array('documentoBajaId' => $documentoBajaId));
    }
    
    public function saveTicketSunat($documentoBajaId,$ticket){
        $data = array('ticketSunat' => $ticket);
        $this->tableGateway->update($data, array('documentoBajaId' => $documentoBajaId));
    }
}
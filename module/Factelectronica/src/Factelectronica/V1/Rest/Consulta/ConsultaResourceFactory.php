<?php
namespace Factelectronica\V1\Rest\Consulta;

class ConsultaResourceFactory
{
    public function __invoke($services)
    {
        $comprobante   = $services->get('Factelectronica\V1\Rest\Comprobantes\ComprobantesMapper');
        $empresa       = $services->get('Factelectronica\V1\Rest\Comprobantes\EmpresaMapper');
        $tipoDocumento = $services->get('Factelectronica\V1\Rest\Comprobantes\TipoDocumentoMapper');
        return new ConsultaResource($comprobante,$empresa,$tipoDocumento);
    }
}

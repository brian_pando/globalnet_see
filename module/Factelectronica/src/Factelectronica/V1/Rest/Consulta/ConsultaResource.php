<?php
namespace Factelectronica\V1\Rest\Consulta;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\Filter\Decompress;
use Core\Service\feedSoap;
use DOMDocument;
use SoapFault;

class ConsultaResource extends AbstractResourceListener
{
    protected $comprobante;
    
    public function __construct($comprobante,$empresa,$tipoDocumento){
        $this->comprobante  = $comprobante;
        $this->empresa      = $empresa;
        $this->tipoDocTable = $tipoDocumento;
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $infoDoc = $this->comprobante->consultaComprobante($data->serie,$data->correlativo,$data->tipoDocumento,$data->fechaEmision,$data->numeroDocCliente,$data->importeTotal);
        
        if($infoDoc != null){
            $ruc = '20452357268';
            $folio = $data->serie.'-'.$data->correlativo;
            $nomCDR = $ruc.'-'.$data->tipoDocumento.'-'.$folio.'.xml';
            $server = $_SERVER['SERVER_NAME'];
            if($data->tipoDocumento == '01'){
                $nomDocumento = 'factura';
            }elseif($data->tipoDocumento == '03'){
                $nomDocumento = 'boleta';
            }elseif($data->tipoDocumento == '07'){
                $nomDocumento = 'notaCredito';
            }elseif($data->tipoDocumento == '08'){
                $nomDocumento = 'notaDebito';
            }else{
                $nomDocumento = 'bajas';
            }
            
            if($data->tipoDocumento == '01'){
                $nomDocumentoPDF = 'facturas';
            }elseif($data->tipoDocumento == '03'){
                $nomDocumentoPDF = 'boletas';
            }elseif($data->tipoDocumento == '07' || $data->tipoDocumento == '08'){
                $nomDocumentoPDF = 'notas';
            }else{
                $nomDocumento = 'bajas';
            }            
            
            $urlPdf = 'http://'.$server.'/sfe/public/reportes/'.$nomDocumentoPDF.'/docpdf/'.$infoDoc->id;
            $rulCDR = $urlServer = 'http://'.$server.'/sfe/tmp/'.$ruc.'/'.$nomDocumento.'/'.$folio.'/R-'.$nomCDR;
            $urlXmlSunat = $urlServer = 'http://'.$server.'/sfe/tmp/'.$ruc.'/'.$nomDocumento.'/'.$folio.'/'.$nomCDR;
            $respuesta = [
                'codRetorno'        => '0',
                'descripcionRetorno'=> 'El comprobante existe y está aceptado',
                'observaciones'     => [],
                'rutaCDR'           => $rulCDR,
                'ruraXmlSunat'      => $urlXmlSunat,
                'rutaPDF'           => $urlPdf,
            ];
        }else{
            $resultadoConsulta = $this->consultaComprobanteSunat($data->serie,$data->correlativo,$data->tipoDocumento);
            if($resultadoConsulta['codRetorno'] == '0001'){
                $compElectroId = $this->comprobante->traerUltimoComprobante($data->serie,$data->correlativo,$data->tipoDocumento);
                
                $ruc = '20452357268';
                $folio = $data->serie.'-'.$data->correlativo;
                $nomCDR = $ruc.'-'.$data->tipoDocumento.'-'.$folio.'.xml';
                $server = $_SERVER['SERVER_NAME'];
                if($data->tipoDocumento == '01'){
                    $nomDocumento = 'factura';
                }elseif($data->tipoDocumento == '03'){
                    $nomDocumento = 'boleta';
                }elseif($data->tipoDocumento == '07'){
                    $nomDocumento = 'notaCredito';
                }elseif($data->tipoDocumento == '08'){
                    $nomDocumento = 'notaDebito';
                }else{
                    $nomDocumento = 'bajas';
                }
                
                if($data->tipoDocumento == '01'){
                    $nomDocumentoPDF = 'facturas';
                }elseif($data->tipoDocumento == '03'){
                    $nomDocumentoPDF = 'boletas';
                }elseif($data->tipoDocumento == '07' || $data->tipoDocumento == '08'){
                    $nomDocumentoPDF = 'notas';
                }else{
                    $nomDocumento = 'bajas';
                }
                
                $urlPdf = 'http://'.$server.'/sfe/public/reportes/'.$nomDocumentoPDF.'/docpdf/'.$compElectroId;
                $rulCDR = $urlServer = 'http://'.$server.'/sfe/tmp/'.$ruc.'/'.$nomDocumento.'/'.$folio.'/R-'.$nomCDR;
                $urlXmlSunat = $urlServer = 'http://'.$server.'/sfe/tmp/'.$ruc.'/'.$nomDocumento.'/'.$folio.'/'.$nomCDR;
                $respuesta = [
                    'codRetorno'        => '0',
                    'descripcionRetorno'=> 'El comprobante existe y está aceptado',
                    'observaciones'     => [],
                    'rutaCDR'           => $rulCDR,
                    'ruraXmlSunat'      => $urlXmlSunat,
                    'rutaPDF'           => $urlPdf,
                ];
                
                $codRetorno = 0;
                $respuesta  = 'La Factura numero '.$folio.', ha sido aceptada';
                $this->comprobante->updateComprobanteEstadoSunat($compElectroId,$respuesta,$codRetorno);
                
            }else{
                $respuesta = [
                    'codRetorno'         => $resultadoConsulta['codRetorno'],
                    'descripcionRetorno' => $resultadoConsulta['descripcionRetorno'],
                    'observaciones'      => [],
                    'rutaCDR'            => '',
                    'ruraXmlSunat'       => '',
                    'rutaPDF'            => '',
                ];
            }
            
        }
        return $respuesta;
    }
    
    /**
     * funcion que gestiona la consulta a SUNAT de un comprobante
     * 
     * @param string $serie
     * @param string $correlativo
     * @param string $tipoDocumentos
     * @return string[]
     */
    public function consultaComprobanteSunat($serie,$correlativo,$tipoDocumento){
        $wsdlURL     = 'https://www.sunat.gob.pe/ol-it-wsconscpegem/billConsultService';
        $dataEmpresa = $this->empresa->getDataEmpresa();
        $ruc = $dataEmpresa->ruc;
        $user = $dataEmpresa->userSol;
        $pass = $dataEmpresa->passSol;
        try {
            $XMLString = $this->getSopMessage($ruc,$user,$pass,$serie,$correlativo,$tipoDocumento,'getStatus');
            $estadoDoc = $this->soapCall($wsdlURL,'getStatus',$XMLString,null,null);
            if($estadoDoc['codigo'] == '0001'){
                $folderDoc = $this->tipoDocTable->getDataTipoDocumento($tipoDocumento);
                $urlFisica = getcwd().'/tmp/'.$ruc.'/'.$folderDoc.'/'.$serie.'-'.$correlativo;
                $filename = $ruc.'-'.$tipoDocumento.'-'.$serie.'-'.$correlativo;
                
                $XMLString = $this->getSopMessage($ruc,$user,$pass,$serie,$correlativo,$tipoDocumento,'getStatusCdr');
                $this->soapCall($wsdlURL,'getStatusCdr',$XMLString,$urlFisica,$filename);
            }
            $respuesta = [
                'codRetorno'         => $estadoDoc['codigo'],
                'descripcionRetorno' => $estadoDoc['mensaje'],
            ];
            return $respuesta;
        }catch(SoapFault $fault){
            $result = [
                'codRetorno'         => $fault->faultcode,
                'descripcionRetorno' => $fault->faultstring
            ];
            return $result;
        }
    }
    
    /**
     * funcion que contruye el xml para consultar a SUNAT
     * 
     * @param int $ruc
     * @param string $user
     * @param string $pass
     * @param string $serie
     * @param string $correlativo
     * @param string $tipoDocumento
     * @return string
     */
    public function getSopMessage($ruc,$user,$pass,$serie,$correlativo,$tipoDocumento,$nomfuncion){
        $XMLString= '<?xml version="1.0" encoding="UTF-8"?>
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                <soapenv:Header>
                    <wsse:Security>
                        <wsse:UsernameToken>
                            <wsse:Username>' .$ruc.$user. '</wsse:Username>
                            <wsse:Password>' .$pass. '</wsse:Password>
            		    </wsse:UsernameToken>
            	    </wsse:Security>
                </soapenv:Header>
                <soapenv:Body>
                    <ser:'.$nomfuncion.'>
                        <rucComprobante>'.$ruc.'</rucComprobante>
                        <tipoComprobante>'.$tipoDocumento.'</tipoComprobante>
                        <serieComprobante>'.$serie.'</serieComprobante>
                        <numeroComprobante>'.$correlativo.'</numeroComprobante>
                    </ser:'.$nomfuncion.'>
                </soapenv:Body>
            </soapenv:Envelope>';
        return $XMLString;
    }
    
    /**
     * 
     * @param string $wsdlURL
     * @param string $nomfuncion
     * @param string $XMLString
     */
    public function soapCall($wsdlURL,$nomfuncion,$XMLString,$urlFisica,$filename){
        $faultcode = '0';
        $endpoint  = $wsdlURL;
        $uri       = 'http://service.sunat.gob.pe';
        $options=array(
            'trace'      => true,
            'location'   => $endpoint,
            'uri'        => $uri,
            'exceptions' => true,
            'cache_wsdl' => true,
        );
    
        try {
            $client = new feedSoap(null, $options);
            $client->SoapClientCall($XMLString);
            $client->__call($nomfuncion, array(), array());
        }catch (SoapFault $f) {
            $faultcode = $f->faultcode;
            $faultdesc = $f->faultstring;
        }
        
        $result  = $client->__getLastResponse();
        $doc     = new DOMDocument();
        $doc->loadXML($result);
        $code    = $doc->getElementsByTagName('statusCode');
        $message = $doc->getElementsByTagName('statusMessage');
        
        $codigo  = $code->item(0)->nodeValue;
        $mensaje = $message->item(0)->nodeValue;
        if($nomfuncion == 'getStatus'){
            $resultSunat = array('codigo' => $codigo, 'mensaje' => $mensaje);
            return $resultSunat;
        }else{
            $data = $doc->getElementsByTagName('content');
            if($data->length > 0 && $codigo == '0004'){
                $cdr = $data->item(0)->nodeValue;
                if($this->isNotData($cdr)){
                    $response  = base64_decode($cdr);
                    $archivo = $urlFisica.'/R-'.$filename.'.zip';
                    file_put_contents($archivo, $response);
                    
                    $filter = new Decompress(array(
                        'adapter' => 'Zip',
                        'options' => array(
                            'target' => $urlFisica,
                        )
                    ));
                    $filter->filter($archivo);
                }
            }
        }
        
    }
    
    function isNotData($test_string){
        return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $test_string);
    }
    
    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}

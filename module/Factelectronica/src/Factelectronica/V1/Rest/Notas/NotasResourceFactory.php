<?php
namespace Factelectronica\V1\Rest\Notas;

class NotasResourceFactory
{
    public function __invoke($services)
    {
        $mapper = $services->get('Factelectronica\V1\Rest\Notas\NotasMapper');
        $mapperDet = $services->get('Factelectronica\V1\Rest\Notas\NotasDetalleMapper');
        $mapperEmp = $services->get('Factelectronica\V1\Rest\Comprobantes\EmpresaMapper');
        $mapperTipoDoc = $services->get('Factelectronica\V1\Rest\Comprobantes\TipoDocumentoMapper');
        
        return new NotasResource($mapper, $mapperDet, $mapperEmp, $mapperTipoDoc);
    }
}

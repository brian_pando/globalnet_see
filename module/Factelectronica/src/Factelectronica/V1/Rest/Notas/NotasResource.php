<?php
namespace Factelectronica\V1\Rest\Notas;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Factelectronica\V1\Rest\Notas\NotasEntity;
use Factelectronica\V1\Rest\Notas\NotasDetalleEntity;
use Core\Controller\PuenteController;

class NotasResource extends AbstractResourceListener
{
    protected $mapper;
    protected $mapperDet;
    protected $mapperEmp;
    protected $mapperTipoDoc;
    
    public function __construct($mapper, $mapperDet, $mapperEmp, $mapperTipoDoc)
    {
        $this->mapper = $mapper;
        $this->mapperDet = $mapperDet;
        $this->mapperEmp = $mapperEmp;
        $this->mapperTipoDoc = $mapperTipoDoc;
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $existe = $this->mapper->existeComprobante($data->serie,$data->correlativo,$data->tipoDocumento);
        if($existe == 0){            
            $notas = new NotasEntity();
            $notas->sucursalId       = $data->sucursalId;
            $notas->fechaEmision     = $data->fechaEmision;
            $notas->tipoDocumento    = $data->tipoDocumento;
            $notas->codigoTipoDoc    = $data->codigoTipoDoc;
            $notas->serie            = $data->serie;
            $notas->correlativo      = $data->correlativo;
            $notas->tipoDocCliente   = $data->tipoDocCliente;
            $notas->numeroDocCliente = $data->numeroDocCliente;
            $notas->razonSocialCliente   = $data->razonSocialCliente;
            $notas->direccionCliente = $data->direccionCliente;
            $notas->totalVentaGravadas   = $data->totalVentaGravadas;
            $notas->totalVentaInafectas  = $data->totalVentaInafectas;
            $notas->totalVentaExoneradas = $data->totalVentaExoneradas;
            $notas->totalVentaGratuitas  = $data->totalVentaGratuitas;
            $notas->sumatoriaIGV     = $data->sumatoriaIGV;
            $notas->sumatoriaISC     = $data->sumatoriaISC;
            $notas->sumatoriaOtrosTributos = $data->sumatoriaOtrosTributos;
            $notas->sumatoriaOtrosCargos = $data->sumatoriaOtrosCargos;
            $notas->totalDescuentos  = $data->totalDescuentos;
            $notas->detraccion       = $data->detraccion;
            $notas->percepcion       = $data->percepcion;
            $notas->retencion        = $data->retencion;
            $notas->bonificacion     = $data->bonificacion;
            $notas->importeTotal     = $data->importeTotal;
            $notas->descuentoGlobal  = $data->descuentoGlobal;
            $notas->tipoMoneda       = $data->tipoMoneda;
            $notas->motivoNota       = $data->motivoNota;
            $notas->serieDocOrigen   = $data->serieDocOrigen;
            $notas->correlativoDocOrigen = $data->correlativoDocOrigen;
            $notas->tipoDocOrigen    = $data->tipoDocOrigen;
            $notas->estadoComprobante= 'Aprobado';
            $notas->observaciones    = $data->observaciones;
            $notas->fechaCreacion    = date('Y-m-d H:i:s');
            $comprobanteId = $this->mapper->save($notas);                                                                   
            
            $detalle = $data->detalle;
            $count = count($detalle);
            for($i=0;$i<$count;$i++){
                $notaDetalle = new NotasDetalleEntity();
                $notaDetalle->comprobanteId = $comprobanteId;
                $notaDetalle->numeroOrden = $detalle[$i]['numeroOrden'];
                $notaDetalle->unidadMedida = $detalle[$i]['unidadMedida'];
                $notaDetalle->cantidad = $detalle[$i]['cantidad'];
                $notaDetalle->codigo = $detalle[$i]['codigo'];
                $notaDetalle->descripcion = $detalle[$i]['descripcion'];
                $notaDetalle->valorUnitario = $detalle[$i]['valorUnitario'];
                $notaDetalle->precioUnitario = $detalle[$i]['precioUnitario'];
                $notaDetalle->igvItem = $detalle[$i]['igvItem'];
                $notaDetalle->iscItem = $detalle[$i]['iscItem'];
                $notaDetalle->valorVenta = $detalle[$i]['valorVenta'];
                $notaDetalle->valorReferencial = $detalle[$i]['valorReferencial'];
                $notaDetalle->codAfectacionIgv = $detalle[$i]['codAfectacionIgv'];
                $notaDetalle->codTipoOperacion = $detalle[$i]['codTipoOperacion'];
                $dataDetalle = $this->mapperDet->save($notaDetalle);
            }            
            $puenteController = new PuenteController($this->mapper, $this->mapperDet, $this->mapperEmp, $this->mapperTipoDoc);
            $respuesta = $puenteController->generateNoteXml($comprobanteId,$data->tipoDocumento);
            $this->mapper->updateComprobanteEstadoSunat($comprobanteId,$respuesta['descripcionRetorno'],$respuesta['codRetorno']);
            return $respuesta;
        }else{            
            $respuesta = [
                'codRetorno'        => '1',
                'descripcionRetorno'=> 'El comprobante fue registrado previamente con otros datos',
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];
            return $respuesta;
        }
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return $this->mapper->fetchOne($id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        return $this->mapper->fetchAll();
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}

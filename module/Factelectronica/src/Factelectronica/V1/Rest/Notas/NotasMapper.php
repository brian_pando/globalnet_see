<?php
namespace Factelectronica\V1\Rest\Notas;
 
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Factelectronica\V1\Rest\Notas\NotasEntity;
 
class NotasMapper
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll(){
        $result = $this->tableGateway->select(function (Select $select){
            $select->columns(array('*'))
                   ->where->nest->equalTo('tipoDocumento', '07')->or->equalTo('tipoDocumento', '08');
            //$select->where->nest->equalTo('table2.column2', 2)->or->equalTo('table2.column3', 3)->unnest->and->equalTo('table1.column1', 1);
        });
        return $result;
    }
    
    public function fetchOne($id){
        $id = (int) $id;
        $result = $this->tableGateway->select(function (Select $select) use($id){
            $select->columns(array('*'))->where(array('id' => $id));
        });
        $data = $result->current();
        if(!$data){
            throw new \Exception("La Nota con id={$id} no se encuentra");
        }
        var_dump($data); exit;
        return $data;
    }
    
    public function save(NotasEntity $notas){
        $data = [
            'sucursalId'            => $notas->sucursalId,
            'fechaEmision'          => $notas->fechaEmision,
            'tipoDocumento'         => $notas->tipoDocumento,
            'codigoTipoDoc'         => $notas->codigoTipoDoc,
            'serie'                 => $notas->serie,
            'correlativo'           => $notas->correlativo,
            'tipoDocCliente'        => $notas->tipoDocCliente,
            'numeroDocCliente'      => $notas->numeroDocCliente,
            'razonSocialCliente'    => $notas->razonSocialCliente,
            'direccionCliente'      => $notas->direccionCliente,
            'totalVentaGravadas'    => $notas->totalVentaGravadas,
            'totalVentaInafectas'   => $notas->totalVentaInafectas,
            'totalVentaExoneradas'  => $notas->totalVentaExoneradas,
            'totalVentaGratuitas'   => $notas->totalVentaGratuitas,
            'totalDescuentos'       => $notas->totalDescuentos,
            'detraccion'            => $notas->detraccion,
            'percepcion'            => $notas->percepcion,
            'retencion'             => $notas->retencion,
            'bonificacion'          => $notas->bonificacion,
            'importeTotal'          => $notas->importeTotal,
            'tipoMoneda'            => $notas->tipoMoneda,
            'sumatoriaIGV'          => $notas->sumatoriaIGV,
            'sumatoriaISC'          => $notas->sumatoriaISC,
            'sumatoriaOtrosTributos'=> $notas->sumatoriaOtrosTributos,
            'sumatoriaOtrosCargos'  => $notas->sumatoriaOtrosCargos,
            'descuentoGlobal'       => $notas->descuentoGlobal,
            'motivoBaja'            => $notas->motivoBaja,
            'motivoNota'            => $notas->motivoNota,
            'serieDocOrigen'        => $notas->serieDocOrigen,
            'correlativoDocOrigen'  => $notas->correlativoDocOrigen,
            'tipoDocOrigen'         => $notas->tipoDocOrigen,
            'estadoComprobante'     => $notas->estadoComprobante,
            'observaciones'         => $notas->observaciones,
            'fechaCreacion'         => $notas->fechaCreacion,
        ];
        $id = (int) $notas->id;
        if($id == 0){
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        }
    }
    
    public function getDataComprobante($compElectroId, $tipoComprobante){
        $data = $this->tableGateway->select(function (Select $select) use($compElectroId, $tipoComprobante){
            $select->columns(array('*'));
            $select->where(array('id' => $compElectroId,'tipoDocumento' => $tipoComprobante));
        });
            return $data->current();
    }
    
    public function updateComprobanteEstadoSunat($compElectroId,$respuesta,$codRetorno){
        $data = array('estadoSunat' => $respuesta, 'codRetornoSunat' => $codRetorno);
        $this->tableGateway->update($data, array('id' => $compElectroId));
    }
    
    public function updateMotivoBaja($tipoDoc,$serie,$correlativo,$motivoBaja,$sucursalId){
        $data = array('motivoBaja' => $motivoBaja);
        $this->tableGateway->update($data, array('tipoDocumento' => $tipoDoc, 'serie' => $serie, 'correlativo' => $correlativo, 'sucursalId' => $sucursalId));
    }
    
    public function existeComprobante($serie,$correlativo,$tipoDocumento){
        $result = $this->tableGateway->select(function (Select $select) use($serie,$correlativo,$tipoDocumento){
            $select->columns(array('*'))
            ->where(array('serie' => $serie, 'correlativo' => $correlativo, 'tipoDocumento' => $tipoDocumento, 'codRetornoSunat' => 0));
        });
            return $result->count();
    }
}
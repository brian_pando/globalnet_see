<?php
namespace Factelectronica\V1\Rest\Comprobantes;

class ComprobantesDetalleEntity
{
    public  $comprobanteDetalleId, $comprobanteId, $unidadMedida, $cantidad, $codigo,$descripcion, $valorUnitario, $precioUnitario,
     $igvItem, $iscItem, $valorVenta, $valorReferencial, $codAfectacionIgv, $codTipoOperacion, $numeroOrden;
    
    public function getArrayCopy()
    {
        return array(
            'comprobanteDetalleId' => $this->comprobanteDetalleId,
            'comprobanteId' => $this->comprobanteId,
            'unidadMedida'  => $this->unidadMedida,
            'cantidad'      => $this->cantidad,
            'codigo'        => $this->codigo,
            'descripcion'   => $this->descripcion,
            'valorUnitario' => $this->valorUnitario,
            'precioUnitario'=> $this->precioUnitario,
            'igvItem'       => $this->igvItem,
            'iscItem'       => $this->iscItem,
            'valorVenta'    => $this->valorVenta,
            'valorReferencial' => $this->valorReferencial,
            'codAfectacionIgv' => $this->codAfectacionIgv,
            'codTipoOperacion' => $this->codTipoOperacion,
            'numeroOrden'   => $this->numeroOrden,
        );
    }
    
    public function exchangeArray(array $array){
        $this->comprobanteDetalleId = $array["comprobanteDetalleId"];
        $this->comprobanteId = $array["comprobanteId"];
        $this->unidadMedida  = $array["unidadMedida"];
        $this->cantidad      = $array["cantidad"];
        $this->codigo        = $array["codigo"];
        $this->descripcion   = $array['descripcion'];
        $this->valorUnitario = $array['valorUnitario'];
        $this->precioUnitario= $array["precioUnitario"];
        $this->igvItem       = $array["igvItem"];
        $this->iscItem       = $array["iscItem"];
        $this->valorVenta    = $array["valorVenta"];
        $this->valorReferencial = $array["valorReferencial"];
        $this->codAfectacionIgv = $array["codAfectacionIgv"];
        $this->codTipoOperacion = $array["codTipoOperacion"];
        $this->numeroOrden   = $array["numeroOrden"];
    }
}

<?php
namespace Factelectronica\V1\Rest\Comprobantes;
 
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Factelectronica\V1\Rest\Comprobantes\ComprobantesEntity;
 
class ComprobantesMapper
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchAll(){
        $result = $this->tableGateway->select(function (Select $select){
            $select->columns(array('*'))
                   ->where->nest->equalTo('tipoDocumento', '01')->or->equalTo('tipoDocumento', '03');
        });
        return $result;
    }
    
    public function fetchOne($id){
        $id = (int) $id;
        $result = $this->tableGateway->select(function (Select $select) use($id){
            $select->columns(array('*'))->where(array('id' => $id));
        });
        $data = $result->current();
        if(!$data){
            throw new \Exception("El comprobante con id={$id} no se encuentra");
        }
        return $data;
    }
    
    public function save(ComprobantesEntity $comprob){        
        $data = [
            'sucursalId'            => $comprob->sucursalId,
            'fechaEmision'          => $comprob->fechaEmision,
            'tipoDocumento'         => $comprob->tipoDocumento,
            'codigoTipoDoc'         => $comprob->codigoTipoDoc,
            'serie'                 => $comprob->serie,
            'correlativo'           => $comprob->correlativo,
            'tipoDocCliente'        => $comprob->tipoDocCliente,
            'numeroDocCliente'      => $comprob->numeroDocCliente,
            'razonSocialCliente'    => $comprob->razonSocialCliente,
            'direccionCliente'      => $comprob->direccionCliente,
            'totalVentaGravadas'    => $comprob->totalVentaGravadas,
            'totalVentaInafectas'   => $comprob->totalVentaInafectas,
            'totalVentaExoneradas'  => $comprob->totalVentaExoneradas,
            'totalVentaGratuitas'   => $comprob->totalVentaGratuitas,
            'totalDescuentos'       => $comprob->totalDescuentos,
            'detraccion'            => $comprob->detraccion,
            'percepcion'            => $comprob->percepcion,
            'retencion'             => $comprob->retencion,
            'bonificacion'          => $comprob->bonificacion,
            'importeTotal'          => $comprob->importeTotal,
            'tipoMoneda'            => $comprob->tipoMoneda,
            'sumatoriaIGV'          => $comprob->sumatoriaIGV,
            'sumatoriaISC'          => $comprob->sumatoriaISC,
            'sumatoriaOtrosTributos'=> $comprob->sumatoriaOtrosTributos,
            'sumatoriaOtrosCargos'  => $comprob->sumatoriaOtrosCargos,
            'descuentoGlobal'       => $comprob->descuentoGlobal,
            'motivoNota'            => $comprob->motivoNota,
            'motivoBaja'            => $comprob->motivoBaja,
            'serieDocOrigen'        => $comprob->serieDocOrigen,
            'correlativoDocOrigen'  => $comprob->correlativoDocOrigen,
            'tipoDocOrigen'         => $comprob->tipoDocOrigen,
            'estadoComprobante'     => $comprob->estadoComprobante,
			'contado'         => $comprob->contado,
            'financiado'     => $comprob->financiado,
            'observaciones'         => $comprob->observaciones,
            'fechaCreacion'         => $comprob->fechaCreacion,
        ];        
        $id = (int) $comprob->id;
        if($id == 0){
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->lastInsertValue;
            return $id;
        }
    }
    
    public function getDataComprobante($compElectroId, $tipoComprobante){
        $data = $this->tableGateway->select(function (Select $select) use($compElectroId, $tipoComprobante){
            $select->columns(array('*'));
            $select->where(array('id' => $compElectroId,'tipoDocumento' => $tipoComprobante));
        });
        return $data->current();
    }
    
    public function updateComprobanteEstadoSunat($compElectroId,$respuesta,$codRetorno){
        $data = array('estadoSunat' => $respuesta, 'codRetornoSunat' => $codRetorno);
        $this->tableGateway->update($data, array('id' => $compElectroId));
    }
    
    public function updateMotivoBaja($tipoDoc,$serie,$correlativo,$motivoBaja,$sucursalId){
        $data = array('motivoBaja' => $motivoBaja);
        $this->tableGateway->update($data, array('tipoDocumento' => $tipoDoc, 'serie' => $serie, 'correlativo' => $correlativo, 'sucursalId' => $sucursalId));
    }
    public function updateMotivoBajaAndEstado($tipoDoc,$serie,$correlativo,$motivoBaja,$sucursalId){
        $data = array('motivoBaja' => $motivoBaja,'estadoRecepcion'=>3,'estadoEnvioId'=>3);
        $this->tableGateway->update($data, array('tipoDocumento' => $tipoDoc, 'serie' => $serie, 'correlativo' => $correlativo, 'sucursalId' => $sucursalId));
        
    }
    public function existeComprobante($serie,$correlativo,$tipoDocumento){        
        $result = $this->tableGateway->select(function (Select $select) use($serie,$correlativo,$tipoDocumento){
            $select->columns(array('*'))
                   ->where(array('serie' => $serie, 'correlativo' => $correlativo, 'tipoDocumento' => $tipoDocumento, 'codRetornoSunat' => 0));
        });        
        return $result->count();
    }
    
    public function consultaComprobante($serie,$correlativo,$tipoDocumento,$fechaEmision,$numeroDocCliente,$importeTotal){
        $result = $this->tableGateway->select(function (Select $select) use($serie,$correlativo,$tipoDocumento,$fechaEmision,$numeroDocCliente,$importeTotal){
            $select->columns(array('*'))
                   ->join(array('suc' => 'sucursal'), 'suc.sucursalId = comprobanteelectronico.sucursalId', array())
                   ->join(array('emp' => 'empresa'), 'emp.empresaId = suc.empresaId', array('ruc'))
                   ->join(array('tDoc' => 'tipodocumentos'), 'tDoc.codigo = comprobanteelectronico.tipoDocumento', array('nomDocumento' => 'nomRelativo'))
                   ->where(array('serie' => $serie, 'correlativo' => $correlativo, 'tipoDocumento' => $tipoDocumento, 'codRetornoSunat' => 0, 'fechaEmision' => $fechaEmision, 'numeroDocCliente' => $numeroDocCliente, 'importeTotal' => $importeTotal));
        });
        return $result->current();
    }
    
    public function traerUltimoComprobante($serie,$correlativo,$tipoDocumento){
        $data = $this->tableGateway->select(function (Select $select) use($serie,$correlativo,$tipoDocumento){
            $select->columns(array('id'))
                   ->where(array('serie' => $serie,'correlativo' => $correlativo,'tipoDocumento' => $tipoDocumento))
                   ->order('id DESC')
                   ->limit(1);
        });
        $result = $data->current();
        return $result->id;
    }
}

<?php
namespace Factelectronica\V1\Rest\Comprobantes;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Factelectronica\V1\Rest\Comprobantes\ComprobantesEntity;
use Factelectronica\V1\Rest\Comprobantes\ComprobantesDetalleEntity;
use Core\Controller\PuenteController;

class ComprobantesResource extends AbstractResourceListener
{
    protected $mapper;
    protected $mapperDet;
    protected $mapperEmp;
    protected $mapperTipoDoc;
    
    public function __construct($mapper, $mapperDet, $mapperEmp, $mapperTipoDoc)
    {                
        $this->mapper = $mapper;
        $this->mapperDet = $mapperDet;
        $this->mapperEmp = $mapperEmp;
        $this->mapperTipoDoc = $mapperTipoDoc;
    }
    
    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {                                                                                                                          
        $existe = $this->mapper->existeComprobante($data->serie,$data->correlativo,$data->tipoDocumento); // No esta funcionando           
        if( $existe == 0){                     
            $comprobantes = new ComprobantesEntity();
            $comprobantes->sucursalId       = $data->sucursalId;
            $comprobantes->fechaEmision     = $data->fechaEmision;
            $comprobantes->tipoDocumento    = $data->tipoDocumento;
            $comprobantes->codigoTipoDoc    = $data->codigoTipoDoc;
            $comprobantes->serie            = $data->serie;
            $comprobantes->correlativo      = $data->correlativo;
            $comprobantes->tipoDocCliente   = $data->tipoDocCliente;
            $comprobantes->numeroDocCliente = $data->numeroDocCliente;
            $comprobantes->razonSocialCliente   = $data->razonSocialCliente;
            $comprobantes->direccionCliente = $data->direccionCliente;
            $comprobantes->totalVentaGravadas   = $data->totalVentaGravadas;
            $comprobantes->totalVentaInafectas  = $data->totalVentaInafectas;
            $comprobantes->totalVentaExoneradas = $data->totalVentaExoneradas;
            $comprobantes->totalVentaGratuitas  = $data->totalVentaGratuitas;
            $comprobantes->sumatoriaIGV     = $data->sumatoriaIGV;
            $comprobantes->sumatoriaISC     = $data->sumatoriaISC;
            $comprobantes->sumatoriaOtrosTributos = $data->sumatoriaOtrosTributos;
            $comprobantes->sumatoriaOtrosCargos = $data->sumatoriaOtrosCargos;
            $comprobantes->totalDescuentos  = $data->totalDescuentos;
            $comprobantes->detraccion       = $data->detraccion;
            $comprobantes->percepcion       = $data->percepcion;
            $comprobantes->retencion        = $data->retencion;
            $comprobantes->bonificacion     = $data->bonificacion;
            $comprobantes->importeTotal     = $data->importeTotal;
            $comprobantes->descuentoGlobal  = $data->descuentoGlobal;
            $comprobantes->tipoMoneda       = $data->tipoMoneda;
			$comprobantes->contado  = $data->contado;
            $comprobantes->financiado       = $data->financiado;
            $comprobantes->observaciones    = $data->observaciones;
            $comprobantes->estadoComprobante= 'Aprobado';
            $comprobantes->fechaCreacion    = date('Y-m-d H:i:s');                               
            $comprobanteId = $this->mapper->save($comprobantes);                                   

            $detalle = $data->detalle;
            $count = count($detalle);            
            for($i=0;$i<$count;$i++){
                $comproDetalle = new ComprobantesDetalleEntity();
                $comproDetalle->comprobanteId = $comprobanteId;
                $comproDetalle->numeroOrden = $detalle[$i]['numeroOrden'];
                $comproDetalle->unidadMedida = $detalle[$i]['unidadMedida'];
                $comproDetalle->cantidad = $detalle[$i]['cantidad'];
                $comproDetalle->codigo = $detalle[$i]['codigo'];
                $comproDetalle->descripcion = $detalle[$i]['descripcion'];
                $comproDetalle->valorUnitario = $detalle[$i]['valorUnitario'];
                $comproDetalle->precioUnitario = $detalle[$i]['precioUnitario'];
                $comproDetalle->igvItem = $detalle[$i]['igvItem'];
                $comproDetalle->iscItem = $detalle[$i]['iscItem'];
                $comproDetalle->valorVenta = $detalle[$i]['valorVenta'];
                $comproDetalle->valorReferencial = $detalle[$i]['valorReferencial'];
                $comproDetalle->codAfectacionIgv = $detalle[$i]['codAfectacionIgv'];
                $comproDetalle->codTipoOperacion = $detalle[$i]['codTipoOperacion'];
                $dataDetalle = $this->mapperDet->save($comproDetalle);
            }                                              
            $puenteController = new PuenteController($this->mapper, $this->mapperDet, $this->mapperEmp, $this->mapperTipoDoc);
            $respuesta = $puenteController->generateInvoiceXml($comprobanteId,$data->tipoDocumento);                                   
            $this->mapper->updateComprobanteEstadoSunat($comprobanteId,$respuesta['descripcionRetorno'],$respuesta['codRetorno']);
            return $respuesta;
        }else{                                   
            $respuesta = [
                'codRetorno'        => '1',
                'descripcionRetorno'=> 'El comprobante fue registrado previamente con otros datos',
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];
            return $respuesta;
        }
        
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {     
        return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {     
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {     
        return $this->mapper->fetchOne($id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {     
        return $this->mapper->fetchAll();
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {     
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {     
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {        
        return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
}

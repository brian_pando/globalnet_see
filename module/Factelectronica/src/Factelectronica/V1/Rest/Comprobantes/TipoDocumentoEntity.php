<?php
namespace Factelectronica\V1\Rest\Comprobantes;

class TipoDocumentoEntity
{
    public  $documentosId, $codigo, $nombre, $nomRelativo;
    
    public function getArrayCopy()
    {
        return array(
            'documentosId'  => $this->documentosId,
            'codigo'        => $this->codigo,
            'nombre'        => $this->nombre,
            'nomRelativo'   => $this->nomRelativo,
        );
    }
    
    public function exchangeArray(array $array){
        $this->documentosId = (isset($array["documentosId"])) ? $array["documentosId"] : null;
        $this->codigo       = (isset($array["codigo"])) ? $array["codigo"] : null;
        $this->nombre       = (isset($array["nombre"])) ? $array["nombre"] : null;
        $this->nomRelativo  = (isset($array["nomRelativo"])) ? $array["nomRelativo"] : null;
    }
}

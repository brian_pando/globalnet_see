<?php
namespace Factelectronica\V1\Rest\Comprobantes;

class ComprobantesResourceFactory
{
    public function __invoke($services)
    {                
        $mapper = $services->get('Factelectronica\V1\Rest\Comprobantes\ComprobantesMapper');
        $mapperDet = $services->get('Factelectronica\V1\Rest\Comprobantes\ComprobantesDetalleMapper');
        $mapperEmp = $services->get('Factelectronica\V1\Rest\Comprobantes\EmpresaMapper');
        $mapperTipoDoc = $services->get('Factelectronica\V1\Rest\Comprobantes\TipoDocumentoMapper');
        return new ComprobantesResource($mapper,$mapperDet,$mapperEmp, $mapperTipoDoc);
    }
}

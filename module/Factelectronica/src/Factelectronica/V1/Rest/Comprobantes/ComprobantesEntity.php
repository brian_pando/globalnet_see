<?php
namespace Factelectronica\V1\Rest\Comprobantes;

class ComprobantesEntity
{
    public  $id, $sucursalId, $fechaEmision, $tipoDocumento, $codigoTipoDoc, $serie,$correlativo, $tipoDocCliente,
    $numeroDocCliente, $razonSocialCliente, $direccionCliente, $totalVentaGravadas, $totalVentaInafectas, $totalVentaExoneradas,
    $totalVentaGratuitas, $totalDescuentos, $detraccion, $percepcion, $retencion, $bonificacion, $importeTotal, $tipoMoneda,
    $sumatoriaIGV, $sumatoriaISC, $sumatoriaOtrosTributos, $sumatoriaOtrosCargos, $descuentoGlobal, $fechaCreacion, $motivoNota,
    $motivoBaja, $serieDocOrigen, $correlativoDocOrigen, $tipoDocOrigen, $estadoComprobante, $observaciones,$contado,$financiado;
    
    public function getArrayCopy()
    {
        return array(
            'id'                    => $this->id,
            'sucursalId'            => $this->sucursalId,
            'fechaEmision'          => $this->fechaEmision,
            'tipoDocumento'         => $this->tipoDocumento,
            'codigoTipoDoc'         => $this->codigoTipoDoc,
            'serie'                 => $this->serie,
            'correlativo'           => $this->correlativo,
            'tipoDocCliente'        => $this->tipoDocCliente,
            'numeroDocCliente'      => $this->numeroDocCliente,
            'razonSocialCliente'    => $this->razonSocialCliente,
            'direccionCliente'      => $this->direccionCliente,
            'totalVentaGravadas'    => $this->totalVentaGravadas,
            'totalVentaInafectas'   => $this->totalVentaInafectas,
            'totalVentaExoneradas'  => $this->totalVentaExoneradas,
            'totalVentaGratuitas'   => $this->totalVentaGratuitas,
            'totalDescuentos'       => $this->totalDescuentos,
            'detraccion'            => $this->detraccion,
            'percepcion'            => $this->percepcion,
            'retencion'             => $this->retencion,
            'bonificacion'          => $this->bonificacion,
            'importeTotal'          => $this->importeTotal,
            'tipoMoneda'            => $this->tipoMoneda,
            'sumatoriaIGV'          => $this->sumatoriaIGV,
            'sumatoriaISC'          => $this->sumatoriaISC,
            'sumatoriaOtrosTributos'=> $this->sumatoriaOtrosTributos,
            'sumatoriaOtrosCargos'  => $this->sumatoriaOtrosCargos,
            'descuentoGlobal'       => $this->descuentoGlobal,
            'fechaCreacion'         => $this->fechaCreacion,
            'motivoNota'            => $this->motivoNota,
            'motivoBaja'            => $this->motivoBaja,
            'serieDocOrigen'        => $this->serieDocOrigen,
            'correlativoDocOrigen'  => $this->correlativoDocOrigen,
            'tipoDocOrigen'         => $this->tipoDocOrigen,
            'estadoComprobante'     => $this->estadoComprobante,
			 'contado'         => 		$this->contado,
            'financiado'     => 		$this->financiado,
            'observaciones'         => $this->observaciones,
        );
    }
    
    public function exchangeArray(array $array){
        $this->id                       = $array["id"];
        $this->sucursalId               = $array["sucursalId"];
        $this->fechaEmision             = $array["fechaEmision"];
        $this->tipoDocumento            = $array["tipoDocumento"];
        $this->codigoTipoDoc            = $array["codigoTipoDoc"];
        $this->serie                    = $array['serie'];
        $this->correlativo              = $array['correlativo'];
        $this->tipoDocCliente           = $array["tipoDocCliente"];
        $this->numeroDocCliente         = $array["numeroDocCliente"];
        $this->razonSocialCliente       = $array["razonSocialCliente"];
        $this->direccionCliente         = $array["direccionCliente"];
        $this->totalVentaGravadas       = $array["totalVentaGravadas"];
        $this->totalVentaInafectas      = $array['totalVentaInafectas'];
        $this->totalVentaExoneradas     = $array["totalVentaExoneradas"];
        $this->totalVentaGratuitas      = $array['totalVentaGratuitas'];
        $this->totalDescuentos          = $array['totalDescuentos'];
        $this->detraccion               = $array['detraccion'];
        $this->percepcion               = $array['percepcion'];
        $this->retencion                = $array['retencion'];
        $this->bonificacion             = $array['bonificacion'];
        $this->importeTotal             = $array["importeTotal"];
        $this->sumatoriaIGV             = $array['sumatoriaIGV'];
        $this->sumatoriaISC             = $array['sumatoriaISC'];
        $this->sumatoriaOtrosTributos   = $array['sumatoriaOtrosTributos'];
        $this->sumatoriaOtrosCargos     = $array['sumatoriaOtrosCargos'];
        $this->descuentoGlobal          = $array['descuentoGlobal'];
        $this->tipoMoneda               = $array["tipoMoneda"];
        $this->fechaCreacion            = $array["fechaCreacion"];
        $this->motivoNota               = $array["motivoNota"];
        $this->motivoBaja               = $array["motivoBaja"];
        $this->serieDocOrigen           = $array["serieDocOrigen"];
        $this->correlativoDocOrigen     = $array['correlativoDocOrigen'];
        $this->tipoDocOrigen            = $array['tipoDocOrigen'];
        $this->estadoComprobante        = $array['estadoComprobante'];
		$this->contado            = $array['contado'];
        $this->financiado        = $array['financiado'];
        $this->observaciones            = $array['observaciones'];
    }
}

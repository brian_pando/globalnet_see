<?php
namespace Factelectronica\V1\Rest\Comprobantes;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
 
class EmpresaMapper
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function getDataEmpresa(){
        $data = $this->tableGateway->select(function (Select $select){
            $select->columns(array('*'));
        });
        return $data->current();
    }
}
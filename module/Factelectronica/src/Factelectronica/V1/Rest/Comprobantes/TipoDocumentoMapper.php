<?php
namespace Factelectronica\V1\Rest\Comprobantes;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
 
class TipoDocumentoMapper
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function getDataTipoDocumento($codigo){
        $result = $this->tableGateway->select(function (Select $select) use($codigo){
            $select->columns(array('nomRelativo'))->where(array('codigo' => $codigo));
        });
        $data = $result->current();
        if(!$data){
            throw new \Exception("El tipo de documento {$codigo} no se encuentra");
        }
        return $data->nomRelativo;
    }
}
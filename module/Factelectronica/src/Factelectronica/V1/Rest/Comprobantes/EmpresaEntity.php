<?php
namespace Factelectronica\V1\Rest\Comprobantes;

class EmpresaEntity
{
    public  $empresaId, $razonSocial, $nombreComercial, $ruc, $direccion,$ubigeo, $estado, $userSol, $passSol, $fechaCreacion;
    
    public function getArrayCopy()
    {
        return array(
            'empresaId'         => $this->empresaId,
            'razonSocial'       => $this->razonSocial,
            'nombreComercial'   => $this->nombreComercial,
            'ruc'               => $this->ruc,
            'direccion'         => $this->direccion,
            'ubigeo'            => $this->ubigeo,
            'estado'            => $this->estado,
            'userSol'           => $this->userSol,
            'passSol'           => $this->passSol,
            'fechaCreacion'     => $this->fechaCreacion,
        );
    }
    
    public function exchangeArray(array $array){
        $this->empresaId        = $array["empresaId"];
        $this->razonSocial      = $array["razonSocial"];
        $this->nombreComercial  = $array["nombreComercial"];
        $this->ruc              = $array["ruc"];
        $this->direccion        = $array["direccion"];
        $this->ubigeo           = $array['ubigeo'];
        $this->estado           = $array['estado'];
        $this->userSol          = $array['userSol'];
        $this->passSol          = $array['passSol'];
        $this->fechaCreacion    = $array["fechaCreacion"];
    }
}

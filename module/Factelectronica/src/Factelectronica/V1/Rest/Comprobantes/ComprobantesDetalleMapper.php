<?php
namespace Factelectronica\V1\Rest\Comprobantes;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Factelectronica\V1\Rest\Comprobantes\ComprobantesDetalleEntity;
 
class ComprobantesDetalleMapper
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function save(ComprobantesDetalleEntity $detalle){
        $data = [
            'comprobanteId' => $detalle->comprobanteId,
            'unidadMedida'  => $detalle->unidadMedida,
            'cantidad'      => $detalle->cantidad,
            'codigo'        => $detalle->codigo,
            'descripcion'   => $detalle->descripcion,
            'valorUnitario' => $detalle->valorUnitario,
            'precioUnitario'=> $detalle->precioUnitario,
            'igvItem'       => $detalle->igvItem,
            'iscItem'       => $detalle->iscItem,
            'valorVenta'    => $detalle->valorVenta,
            'valorReferencial' => $detalle->valorReferencial,
            'codAfectacionIgv' => $detalle->codAfectacionIgv,
            'codTipoOperacion' => $detalle->codTipoOperacion,
            'numeroOrden'   => $detalle->numeroOrden,
        ];
        
        $id = (int) $detalle->comprobanteDetalleId;
        if($id == 0){
            $this->tableGateway->insert($data);
        }
    }
    
    public function getDataDetalle($comprobanteEleId){
        $id = (int) $comprobanteEleId;
        $data = $this->tableGateway->select(function (Select $select) use($id){
            $select->columns(array('*'))
                   ->where(array('comprobanteId' => $id));
        });
        return $data->toArray();
    }
}

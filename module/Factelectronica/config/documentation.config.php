<?php
return array(
    'Factelectronica\\V1\\Rest\\Comprobantes\\Controller' => array(
        'description' => 'Administracion de comprobantes',
        'collection' => array(
            'description' => 'Listado de comprobantes',
            'GET' => array(
                'description' => 'Muestra el listado de comprobantes',
                'response' => '{
   "_links": {
       "self": {
           "href": "/comprobantes"
       },
       "first": {
           "href": "/comprobantes?page={page}"
       },
       "prev": {
           "href": "/comprobantes?page={page}"
       },
       "next": {
           "href": "/comprobantes?page={page}"
       },
       "last": {
           "href": "/comprobantes?page={page}"
       }
   }
   "_embedded": {
       "comprobantes": [
           {
               "_links": {
                   "self": {
                       "href": "/comprobantes[/:comprobantes_id]"
                   }
               }
              "sucursalId": "Id de la sucursal",
              "fechaEmision": "Fecha de emisión",
              "tipoDocumento": "Tipo de documento",
              "codigoTipoDoc": "Código del Tipo de documento",
              "serie": "Serie del comprobante",
              "correlativo": "Correlatio del comprobante",
              "tipoDocCliente": "Tipo de documento del cliente",
              "numeroDocCliente": "Número de documento del cliente",
              "razonSocialCliente": "Nombre o Razón Social del cliente",
              "direccionCliente": "Dirección del Cliente",
              "totalVentaGravadas": "Total de ventas gravadas",
              "totalVentaInafectas": "Total de ventas inafectas",
              "totalVentaExoneradas": "Total de ventas exoneradas",
              "totalVentaGratuitas": "Total de ventas gratuitas",
              "sumatoriaIGV": "Total IGV",
              "sumatoriaISC": "Total ISC",
              "sumatoriaOtrosTributos": "Total de otros tributos",
              "sumatoriaOtrosCargos": "Total de otros cargos",
              "totalDescuentos": "Total descuentos",
              "importeTotal": "Importe Total",
              "tipoMoneda": "Tipo de Moneda"
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Inserta un comprobante',
                'request' => '{
   "sucursalId": "Id de la sucursal",
   "fechaEmision": "Fecha de emisión",
   "tipoDocumento": "Tipo de documento",
   "codigoTipoDoc": "Código del Tipo de documento",
   "serie": "Serie del comprobante",
   "correlativo": "Correlatio del comprobante",
   "tipoDocCliente": "Tipo de documento del cliente",
   "numeroDocCliente": "Número de documento del cliente",
   "razonSocialCliente": "Nombre o Razón Social del cliente",
   "direccionCliente": "Dirección del Cliente",
   "totalVentaGravadas": "Total de ventas gravadas",
   "totalVentaInafectas": "Total de ventas inafectas",
   "totalVentaExoneradas": "Total de ventas exoneradas",
   "totalVentaGratuitas": "Total de ventas gratuitas",
   "sumatoriaIGV": "Total IGV",
   "sumatoriaISC": "Total ISC",
   "sumatoriaOtrosTributos": "Total de otros tributos",
   "sumatoriaOtrosCargos": "Total de otros cargos",
   "totalDescuentos": "Total descuentos",
   "importeTotal": "Importe Total",
   "tipoMoneda": "Tipo de Moneda"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/comprobantes[/:comprobantes_id]"
       }
   }
   "sucursalId": "Id de la sucursal",
   "fechaEmision": "Fecha de emisión",
   "tipoDocumento": "Tipo de documento",
   "codigoTipoDoc": "Código del Tipo de documento",
   "serie": "Serie del comprobante",
   "correlativo": "Correlatio del comprobante",
   "tipoDocCliente": "Tipo de documento del cliente",
   "numeroDocCliente": "Número de documento del cliente",
   "razonSocialCliente": "Nombre o Razón Social del cliente",
   "direccionCliente": "Dirección del Cliente",
   "totalVentaGravadas": "Total de ventas gravadas",
   "totalVentaInafectas": "Total de ventas inafectas",
   "totalVentaExoneradas": "Total de ventas exoneradas",
   "totalVentaGratuitas": "Total de ventas gratuitas",
   "sumatoriaIGV": "Total IGV",
   "sumatoriaISC": "Total ISC",
   "sumatoriaOtrosTributos": "Total de otros tributos",
   "sumatoriaOtrosCargos": "Total de otros cargos",
   "totalDescuentos": "Total descuentos",
   "importeTotal": "Importe Total",
   "tipoMoneda": "Tipo de Moneda"
}',
            ),
        ),
        'entity' => array(
            'description' => 'Comprobantes',
            'GET' => array(
                'description' => 'Duevuelve un comprobante especifico',
                'response' => '{
   "_links": {
       "self": {
           "href": "/comprobantes[/:comprobantes_id]"
       }
   }
   "sucursalId": "Id de la sucursal"
}',
            ),
        ),
    ),
    'Factelectronica\\V1\\Rest\\Notas\\Controller' => array(
        'description' => 'Administracion de notas de credito y debito',
        'collection' => array(
            'description' => 'Listado de notas',
            'GET' => array(
                'description' => 'Muestra el listado de notas',
                'response' => '{
   "_links": {
       "self": {
           "href": "/notas"
       },
       "first": {
           "href": "/notas?page={page}"
       },
       "prev": {
           "href": "/notas?page={page}"
       },
       "next": {
           "href": "/notas?page={page}"
       },
       "last": {
           "href": "/notas?page={page}"
       }
   }
   "_embedded": {
       "notas": [
           {
               "_links": {
                   "self": {
                       "href": "/notas[/:notas_id]"
                   }
               }
              "sucursalId": "Id de la sucursal",
              "fechaEmision": "Fecha de emisión",
              "tipoDocumento": "Tipo de documento",
              "codigoTipoDoc": "Código del Tipo de Nota de credito o debito, mirar catalogo N° 9 Código de tipo de notas de SUNAT",
              "serie": "Serie de la nota",
              "correlativo": "Correlatio de la nota",
              "tipoDocCliente": "Tipo de documento del cliente",
              "numeroDocCliente": "Número de documento del cliente",
              "numeroDocCliente": "Número de documento del cliente",
              "razonSocialCliente": "Nombre o Razón Social del cliente",
              "direccionCliente": "Dirección del Cliente",
              "totalVentaGravadas": "Total de ventas gravadas",
              "totalVentaInafectas": "Total de ventas inafectas",
              "totalVentaExoneradas": "Total de ventas exoneradas",
              "totalVentaGratuitas": "Total de ventas gratuitas",
              "sumatoriaIGV": "Total IGV",
              "sumatoriaISC": "Total ISC",
              "sumatoriaOtrosTributos": "Total de otros tributos",
              "sumatoriaOtrosCargos": "Total de otros cargos",
              "totalDescuentos": "Total descuentos",
              "importeTotal": "Importe Total",
              "tipoMoneda": "Tipo de Moneda",
              "motivoNota": "Motivo de la nota Credito/Debito",
              "serieDocOrigen": "Serie del documento de originó la nota",
              "correlativoDocOrigen": "Correlativo del documento que originó la nota",
              "tipoDocOrigen": "Tipo documento que originó la nota"
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Inserta una nota',
                'request' => '{
   "sucursalId": "Id de la sucursal",
   "fechaEmision": "Fecha de emisión",
   "tipoDocumento": "Tipo de documento",
   "codigoTipoDoc": "Código del Tipo de Nota de credito o debito, mirar catalogo N° 9 Código de tipo de notas de SUNAT",
   "serie": "Serie de la nota",
   "correlativo": "Correlatio de la nota",
   "tipoDocCliente": "Tipo de documento del cliente",
   "numeroDocCliente": "Número de documento del cliente",
   "numeroDocCliente": "Número de documento del cliente",
   "razonSocialCliente": "Nombre o Razón Social del cliente",
   "direccionCliente": "Dirección del Cliente",
   "totalVentaGravadas": "Total de ventas gravadas",
   "totalVentaInafectas": "Total de ventas inafectas",
   "totalVentaExoneradas": "Total de ventas exoneradas",
   "totalVentaGratuitas": "Total de ventas gratuitas",
   "sumatoriaIGV": "Total IGV",
   "sumatoriaISC": "Total ISC",
   "sumatoriaOtrosTributos": "Total de otros tributos",
   "sumatoriaOtrosCargos": "Total de otros cargos",
   "totalDescuentos": "Total descuentos",
   "importeTotal": "Importe Total",
   "tipoMoneda": "Tipo de Moneda",
   "motivoNota": "Motivo de la nota Credito/Debito",
   "serieDocOrigen": "Serie del documento de originó la nota",
   "correlativoDocOrigen": "Correlativo del documento que originó la nota",
   "tipoDocOrigen": "Tipo documento que originó la nota"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/notas[/:notas_id]"
       }
   }
   "sucursalId": "Id de la sucursal",
   "fechaEmision": "Fecha de emisión",
   "tipoDocumento": "Tipo de documento",
   "codigoTipoDoc": "Código del Tipo de Nota de credito o debito, mirar catalogo N° 9 Código de tipo de notas de SUNAT",
   "serie": "Serie de la nota",
   "correlativo": "Correlatio de la nota",
   "tipoDocCliente": "Tipo de documento del cliente",
   "numeroDocCliente": "Número de documento del cliente",
   "numeroDocCliente": "Número de documento del cliente",
   "razonSocialCliente": "Nombre o Razón Social del cliente",
   "direccionCliente": "Dirección del Cliente",
   "totalVentaGravadas": "Total de ventas gravadas",
   "totalVentaInafectas": "Total de ventas inafectas",
   "totalVentaExoneradas": "Total de ventas exoneradas",
   "totalVentaGratuitas": "Total de ventas gratuitas",
   "sumatoriaIGV": "Total IGV",
   "sumatoriaISC": "Total ISC",
   "sumatoriaOtrosTributos": "Total de otros tributos",
   "sumatoriaOtrosCargos": "Total de otros cargos",
   "totalDescuentos": "Total descuentos",
   "importeTotal": "Importe Total",
   "tipoMoneda": "Tipo de Moneda",
   "motivoNota": "Motivo de la nota Credito/Debito",
   "serieDocOrigen": "Serie del documento de originó la nota",
   "correlativoDocOrigen": "Correlativo del documento que originó la nota",
   "tipoDocOrigen": "Tipo documento que originó la nota"
}',
            ),
        ),
        'entity' => array(
            'description' => 'Notas',
            'GET' => array(
                'description' => 'Duevuelve una nota especifica',
                'response' => '{
   "_links": {
       "self": {
           "href": "/notas[/:notas_id]"
       }
   }
   "sucursalId": "Id de la sucursal",
   "fechaEmision": "Fecha de emisión",
   "tipoDocumento": "Tipo de documento",
   "codigoTipoDoc": "Código del Tipo de Nota de credito o debito, mirar catalogo N° 9 Código de tipo de notas de SUNAT",
   "serie": "Serie de la nota",
   "correlativo": "Correlatio de la nota",
   "tipoDocCliente": "Tipo de documento del cliente",
   "numeroDocCliente": "Número de documento del cliente",
   "numeroDocCliente": "Número de documento del cliente",
   "razonSocialCliente": "Nombre o Razón Social del cliente",
   "direccionCliente": "Dirección del Cliente",
   "totalVentaGravadas": "Total de ventas gravadas",
   "totalVentaInafectas": "Total de ventas inafectas",
   "totalVentaExoneradas": "Total de ventas exoneradas",
   "totalVentaGratuitas": "Total de ventas gratuitas",
   "sumatoriaIGV": "Total IGV",
   "sumatoriaISC": "Total ISC",
   "sumatoriaOtrosTributos": "Total de otros tributos",
   "sumatoriaOtrosCargos": "Total de otros cargos",
   "totalDescuentos": "Total descuentos",
   "importeTotal": "Importe Total",
   "tipoMoneda": "Tipo de Moneda",
   "motivoNota": "Motivo de la nota Credito/Debito",
   "serieDocOrigen": "Serie del documento de originó la nota",
   "correlativoDocOrigen": "Correlativo del documento que originó la nota",
   "tipoDocOrigen": "Tipo documento que originó la nota"
}',
            ),
        ),
    ),
    'Factelectronica\\V1\\Rest\\Bajas\\Controller' => array(
        'description' => 'Administración de documentos de comunicación de baja',
        'collection' => array(
            'description' => 'Listado total de documentos de comunicación de baja',
            'GET' => array(
                'description' => 'Listado de documentos de comunicación de baja',
                'response' => '{
   "_links": {
       "self": {
           "href": "/bajas"
       },
       "first": {
           "href": "/bajas?page={page}"
       },
       "prev": {
           "href": "/bajas?page={page}"
       },
       "next": {
           "href": "/bajas?page={page}"
       },
       "last": {
           "href": "/bajas?page={page}"
       }
   }
   "_embedded": {
       "bajas": [
           {
               "_links": {
                   "self": {
                       "href": "/bajas[/:bajas_id]"
                   }
               }
              "codigoIdentificador": "Identificador  del  documento Comunicación  de baja. El formato que deberá seguir es el siguiente: <RA>-<Fecha  de generación del archivo YYYYMMDD>-<Número Correlativo hasta 5 posiciones>. Por ejemplo, si el resumen fue generado el 23/03/2011, el identificador debe ser: RA-20110323-1",
              "sucursalId": "Id de la sucursal",
              "fechaEmision": "Fecha del documento que se dará de baja",
              "fechaDocumentoBaja": "Fecha en la cual se generó la Comunicación de baja."
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'inserción de documento de baja',
                'request' => '{
   "codigoIdentificador": "Identificador  del  documento Comunicación  de baja. El formato que deberá seguir es el siguiente: <RA>-<Fecha  de generación del archivo YYYYMMDD>-<Número Correlativo hasta 5 posiciones>. Por ejemplo, si el resumen fue generado el 23/03/2011, el identificador debe ser: RA-20110323-1",
   "sucursalId": "Id de la sucursal",
   "fechaEmision": "Fecha del documento que se dará de baja",
   "fechaDocumentoBaja": "Fecha en la cual se generó la Comunicación de baja."
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/bajas[/:bajas_id]"
       }
   }
   "codigoIdentificador": "Identificador  del  documento Comunicación  de baja. El formato que deberá seguir es el siguiente: <RA>-<Fecha  de generación del archivo YYYYMMDD>-<Número Correlativo hasta 5 posiciones>. Por ejemplo, si el resumen fue generado el 23/03/2011, el identificador debe ser: RA-20110323-1",
   "sucursalId": "Id de la sucursal",
   "fechaEmision": "Fecha del documento que se dará de baja",
   "fechaDocumentoBaja": "Fecha en la cual se generó la Comunicación de baja."
}',
            ),
        ),
    ),
    'Factelectronica\\V1\\Rest\\Consulta\\Controller' => array(
        'description' => 'Consulta de doumentos',
        'collection' => array(
            'description' => 'consulta de documento',
            'GET' => array(
                'description' => 'consulta de documento',
                'response' => '{
   "_links": {
       "self": {
           "href": "/consulta"
       },
       "first": {
           "href": "/consulta?page={page}"
       },
       "prev": {
           "href": "/consulta?page={page}"
       },
       "next": {
           "href": "/consulta?page={page}"
       },
       "last": {
           "href": "/consulta?page={page}"
       }
   }
   "_embedded": {
       "consulta": [
           {
               "_links": {
                   "self": {
                       "href": "/consulta[/:consulta_id]"
                   }
               }
              "tipoDocumento": "Tipo de documeno (Factura, Boleta, Nota, etc)",
              "serie": "serie del documento",
              "correlativo": "correlativo  del documento",
              "numeroDocCliente": "Número de documento del cliente",
              "fechaEmision": "Fecha de emisión del documento",
              "importeTotal": "Importe total del dpcumento"
           }
       ]
   }
}',
            ),
            'POST' => array(
                'description' => 'Obtener datos del documento',
                'request' => '{
   "tipoDocumento": "Tipo de documeno (Factura, Boleta, Nota, etc)",
   "serie": "serie del documento",
   "correlativo": "correlativo  del documento",
   "numeroDocCliente": "Número de documento del cliente",
   "fechaEmision": "Fecha de emisión del documento",
   "importeTotal": "Importe total del dpcumento"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/consulta[/:consulta_id]"
       }
   }
   "tipoDocumento": "Tipo de documeno (Factura, Boleta, Nota, etc)",
   "serie": "serie del documento",
   "correlativo": "correlativo  del documento",
   "numeroDocCliente": "Número de documento del cliente",
   "fechaEmision": "Fecha de emisión del documento",
   "importeTotal": "Importe total del dpcumento"
}',
            ),
        ),
    ),
);

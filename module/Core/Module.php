<?php

namespace Core;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Core\Model\CompElectTable;
use Core\Model\DocResumenTable;
use Core\Model\DocBajaTable;

use Core\Model\ResumenDiarioTable;
use Core\Model\ResumenDetalleDiarioTable;
use Core\Model\ComprobanteElectronicoTable;
use Core\Model\EmpresaTable;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getServiceConfig(){
        return array(
            'factories' => array(
				'Core\Model\ResumenDiarioTable' => function ($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new ResumenDiarioTable($dbAdapter);
                return $table;
                },
                'Core\Model\ComprobanteElectronicoTable' => function ($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new ComprobanteElectronicoTable($dbAdapter);
                return $table;
                },
                'Core\Model\ResumenDetalleDiarioTable' => function ($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new ResumenDetalleDiarioTable($dbAdapter);
                return $table;
                },
                'Core\Model\EmpresaTable' => function ($sm) {
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $table = new EmpresaTable($dbAdapter);
                return $table;
                },
                'Core\Model\CompElectTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new CompElectTable($dbAdapter);
                    return $table;
                },
                'Core\Model\DocResumenTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new DocResumenTable($dbAdapter);
                    return $table;
                },
                'Core\Model\DocBajaTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new DocBajaTable($dbAdapter);
                    return $table;
                }
            ),
            'aliases' => array(
                'zfdb_adapter' => 'Zend\Db\Adapter\Adapter',
            ),
        );
    }
}
<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Core\Controller\Puente'    => 'Core\Controller\PuenteController',
			'Core\Controller\Resumen'    => 'Core\Controller\ResumenController',
        ),
    ),
    'router' => array(
        'routes' => array(
			'resumen' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/resumen[/:action]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Core\Controller',                        
                        'controller' => 'Resumen',
                        //vista: core/resumen/genera-resumen
                        'action'     => 'generarResumen',
                    ),
                ),
            ),
            'core' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/core',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Core\Controller',
                        'controller'    => 'Puente',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/][:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[0-9]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/core'          => __DIR__ . '/../view/layout/sfe.phtml',
            'error/404'            => __DIR__ . '/../view/error/404.phtml',
            'error/index'          => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            'core' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);


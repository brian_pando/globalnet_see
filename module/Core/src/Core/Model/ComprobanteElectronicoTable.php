<?php
namespace Core\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
//use Zend\Form\Annotation\Object;
use ZfTable\Decorator\Condition\Plugin\NotEqual;
class ComprobanteElectronicoTable extends AbstractTableGateway
{
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'comprobanteelectronico';    
    
   
   
    public  function  getComprobanteElectronico($fechaDocumentoResumen){     
        $data=$this->select(function (Select $select) use($fechaDocumentoResumen){
            $select->columns(array('*'));
            $select->where->nest()
            ->equalTo('tipoDocumento','03')
            ->or
            ->equalTo('tipoDocOrigen', '03')
            ->unnest()  // bracket closed
            ->equalTo('fechaEmision', $fechaDocumentoResumen);            
             $select->where('estadoEnvioId != 1 and estadoEnvioId != 4 ');
            
        });      
        return $data->toArray();
    }
    public function listaFechaDocResumen(){              
        $data=$this->select(function (Select $select){
            $select->columns(array('*'));
            $select->where->nest()  // nest -> "("
            ->equalTo('tipoDocumento','03')
            ->or
            ->equalTo('tipoDocOrigen', '03')
            ->unnest();  // unnest -> ")"
            $select->where('estadoEnvioId != 1  and estadoEnvioId != 4 ');
            $select->group('fechaEmision');
            
        });           
        return $data->toArray();
    }
    public function updateEstadoComprobanteElectronico($listaComprobantes,$estadoEnvioId)
    {           
       foreach ($listaComprobantes as $comprobante)
       {          
           $data = array('estadoEnvioId' => $estadoEnvioId);
           $this->update($data, array('id' => $comprobante['comprobanteElectronicoId']));
           
       }
       
    }
   
}
?>

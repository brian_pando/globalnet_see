<?php
namespace Core\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
//use Zend\Form\Annotation\Object;
class ResumenDetalleDiarioTable extends AbstractTableGateway
{
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'resumendetallediario';  
    
    
    public  function  getComprobanteElectronicoResumenDetalle($resumenDiarioId){
        
        $data=$this->select(function (Select $select) use($resumenDiarioId){
            $select->columns(array('*'));
            $select->where(array('resumenDiarioId' => $resumenDiarioId));
            
        });
        return $data->toArray();
    }
    
   
    
   
}
?>

<?php
namespace Core\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;  
// use Zend\Form\Annotation\Object;
class EmpresaTable extends AbstractTableGateway
{
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'empresa';    
    public function getDatosEmpresa(){      
        $data = $this->select(function(Select $select){
            $select->columns(array('razonSocial','ruc','direccion','ubigeo'))
            ->where(array('empresaId' => 1));
        });
        return $data->current();
    }
   
    
   
    
   
}
?>

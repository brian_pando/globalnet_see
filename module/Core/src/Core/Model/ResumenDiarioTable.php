<?php
namespace Core\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
//use Zend\Form\Annotation\Object;
class ResumenDiarioTable extends AbstractTableGateway
{
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'resumendiario';    
    public function maxNumeroIdentificador($fechaEmision){      
        $data=$this->select(function (Select $select) use($fechaEmision){            
        $select->columns([
                'max' => new Expression('MAX(?)', [['numeroIdentificador' => Expression::TYPE_IDENTIFIER]])                
          ])
          ->where(array('fechaEmision' => $fechaEmision));
        });    
        $result = $data->current();
        if($result['max']){
            $max=$result['max']+1;
        }else{
            $max=1;
        }        
        return $max;
    }
    public function guardarResumen($parametros){   
             
         $sql = new Sql($this->adapter);
         $insert = $sql->insert('resumendiario');
                         
         //RC-20180118-00001
         $codigoIdentificador="RC-".str_replace("-","",$parametros['fechaEmision'])."-".str_pad($parametros['numeroIdentificador'], 5, "0", STR_PAD_LEFT);
         $data = array(
             'numeroIdentificador' => $parametros['numeroIdentificador'],
             'fechaEmision'  => $parametros['fechaEmision'],
             'fechaDocumentoResumen'=>$parametros['fechaDocumentoResumen'],
             'codigoIdentificador'=>$codigoIdentificador,
             'fechaCreacion'=>$parametros['fechaEmision']
         );
         $insert->values($data);      
         $statement = $sql->prepareStatementForSqlObject($insert);
         $results = $statement->execute();      
         $id=$results->getGeneratedValue();
         $insertDetalle = $sql->insert('resumendetallediario');
         foreach ($parametros['resumenDetalle'] as $detalleResumen){            
             
             $detalle=array(
                 'resumenDiarioId'=>$id,
                 'comprobanteElectronicoId'=>$detalleResumen['id'],
                 'fechaEmision'=>$detalleResumen['fechaEmision'],
                 'tipoDocumento'=>$detalleResumen['tipoDocumento'],
                 'codigoTipoDoc'=>$detalleResumen['codigoTipoDoc'],
                 'serie'=>$detalleResumen['serie'],
                 'correlativo'=>$detalleResumen['correlativo'],
                 'tipoDocCliente'=>$detalleResumen['tipoDocCliente'],
                 'numeroDocCliente'=>$detalleResumen['numeroDocCliente'],
                 'razonSocialCliente'=>$detalleResumen['razonSocialCliente'],
                 'direccionCliente'=>$detalleResumen['direccionCliente']    ,
                 'totalVentaGravadas'=>$detalleResumen['totalVentaGravadas'],
                 'totalVentaInafectas'=>$detalleResumen['totalVentaInafectas'],
                 'totalVentaExoneradas'=>$detalleResumen['totalVentaExoneradas'],
                 'totalVentaGratuitas'=>$detalleResumen['totalVentaGratuitas'],
                 'sumatoriaIGV'=>$detalleResumen['sumatoriaIGV'],
                 'sumatoriaISC'=>$detalleResumen['sumatoriaISC'],
                 'sumatoriaOtrosTributos'=>$detalleResumen['sumatoriaOtrosTributos'],
                 'sumatoriaOtrosCargos'=>$detalleResumen['sumatoriaOtrosCargos'],
                 'totalDescuentos'=>$detalleResumen['totalDescuentos'],
                 'importeTotal'=>$detalleResumen['importeTotal'],
                 'tipoMoneda'=>$detalleResumen['tipoMoneda'],
                 'motivoNota'=>$detalleResumen['motivoNota'],
                 'motivoBaja'=>$detalleResumen['motivoBaja'],
                 'serieDocOrigen'=>$detalleResumen['serieDocOrigen'],
                 'correlativoDocOrigen'=>$detalleResumen['correlativoDocOrigen'],
                 'tipoDocOrigen'=>$detalleResumen['tipoDocOrigen'],
                 'estadoItem'=>$detalleResumen['estadoEnvioId']==0?1:$detalleResumen['estadoEnvioId']
                 
             );
             $insertDetalle->values($detalle);
             $statement = $sql->prepareStatementForSqlObject($insertDetalle)->execute();
         }        
       
         return $id;
                         
        
    }
    public function getResumenDiarioId($resumenDiarioId){        
        $data=$this->select(function (Select $select) use($resumenDiarioId){            
            $select->columns(array('*'));
            $select->where(array('resumenDiarioId' => $resumenDiarioId));
        });        
        return $data->current();
        
    }
    
	public function getConsultaResumenDiario(){
        $data=$this->select(function (Select $select) {
            $select->columns(array('*'));
            $select->where(array('estadoSunat' => 4));
            
        });
        return $data->toArray();
            
    }
	
    public function saveTicketSunat($idResumenDiario,$ticket){
       
        $data = array('ticketSunat' => $ticket);
        $this->update($data, array('resumenDiarioId' => $idResumenDiario));
        
       
       // $this->tableGateway->update($data, array('resumenDiarioId' => $documentoBajaId));
    }
    public function updateResumenDiario($resumenDiarioId,$resultado){        
        $data = array('estadoSunat' => $resultado['codRetorno'],'rutaCDR'=>$resultado['rutaCDR'],'rutaXmlSunat'=>$resultado['rutaXmlSunat']);
        $this->update($data, array('resumenDiarioId' => $resumenDiarioId));

    }
   
    
   
}
?>

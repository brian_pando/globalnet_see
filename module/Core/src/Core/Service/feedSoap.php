<?php
namespace Core\Service;

use SoapClient;
use DOMDocument;

class feedSoap extends SoapClient
{
    var $XMLStr = "";
    
    function setXMLStr ($value){
        $this->XMLStr = $value; 
    }
    
    function getXMLStr(){
        return $this->XMLStr; 
    }

    function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $request = $this->XMLStr;
        $dom = new DOMDocument('1.0');

        try
        {
            $dom->loadXML($request);
        }
        catch (DOMException $e)
        {
            die($e->code);
        }

        $request = $dom->saveXML();
        
        //doRequest
        return parent::__doRequest($request, $location, $action, $version, $one_way = 0);
    }

    function SoapClientCall($SOAPXML){
        return $this->setXMLStr ($SOAPXML);
    }
}

<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use SimpleXMLElement;
use Zend\Filter\Compress;
use Zend\Filter\Decompress;
use SoapFault;
use Core\Service\feedSoap;

use DOMDocument;
use Core\Service\XMLSecurityDSig;
use Core\Service\XMLSecurityKey;


class ResumenController extends AbstractActionController
{
    
    protected $resumenDiarioTable;  
    protected $resumenDiarioDetalleTable;
    protected $comprobanteElectronicoTable;
    protected $empresaTable;
  
    public function ResumenDiarioTable(){
         if (!$this->resumenDiarioTable){
             $sm = $this->getServiceLocator();
             $this->resumenDiarioTable = $sm->get('Core\Model\ResumenDiarioTable');
         }
         return $this->resumenDiarioTable;
    }
    public function ResumenDiarioDetalleTable(){
        if (!$this->resumenDiarioDetalleTable){
            $sm = $this->getServiceLocator();
            $this->resumenDiarioDetalleTable = $sm->get('Core\Model\ResumenDetalleDiarioTable');
        }
        return $this->resumenDiarioDetalleTable;
    }
    public function ComprobanteElectronicoTable(){
        if (!$this->comprobanteElectronicoTable){
            $sm = $this->getServiceLocator();
            $this->comprobanteElectronicoTable = $sm->get('Core\Model\ComprobanteElectronicoTable');
        }
        return $this->comprobanteElectronicoTable;
    }  
    public function EmpresaTable(){
        if (!$this->empresaTable){
            $sm = $this->getServiceLocator();
            $this->empresaTable = $sm->get('Core\Model\EmpresaTable');
        }
        return $this->empresaTable;
    } 

	
	 public function consultarTicketAction()
    {
        
        //Para escribir el XML.
        /* $urlFisica = dirname(__FILE__);
         file_put_contents($urlFisica."/soaps.txt", $XMLString);*/                
         //hmlg
         //$wsdlURL = 'https://www.sunat.gob.pe/ol-ti-itcpgem-sqa/billService';
         //test
         $rucEmpresa="20452357268";
         $nomfuncion="getStatus";
         $projectName = explode('/',$_SERVER['REQUEST_URI'])[1];

         //  -------  pruebas  -----
         $wsdlURL = 'https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService';     
         $user = $rucEmpresa.'MODDATOS';
         $password = 'moddatos';        
         
         //  ------   produccion  -----
         if ($projectName == 'sfe') {            
            $wsdlURL = 'https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService'; 
            $user = $rucEmpresa.'WLOPEZ29';
            $password = 'wilopez29';
        }        

        $listaResumenConsultar=$this->getConsultaResumenDiario(); 
		//var_dump($listaResumenConsultar); exit;
        foreach ($listaResumenConsultar as $resumen)
        {            
            $resumenDiarioId=$resumen['resumenDiarioId'];
            $codigoIdentificador=$resumen['codigoIdentificador'];
            $filename=$rucEmpresa.'-'.$codigoIdentificador;
            $ticket=$resumen['ticketSunat'];
            //Ruta Local
            //$urlFisica = dirname(__FILE__);
            //Ruta Server
            $urlFisica = $this->crearFolder($rucEmpresa,$codigoIdentificador);
            $XMLString = $this->getSopConsulta("", "", $user, $password, $nomfuncion,$ticket);
            $resultado=$this->soapCallConsulta($wsdlURL, $callFunction="SendBill", $XMLString,$filename, $resumenDiarioId, $urlFisica, "");   
			//var_dump($resultado);exit;
            if($resultado['codRetorno']===0)
            {
                $updateResumenDiario=$this->updateResumenDiario($resumenDiarioId, $resultado);
                $listaComprobantes=$this->ResumenDiarioDetalleTable()->getComprobanteElectronicoResumenDetalle($resumenDiarioId);
                $comprobanteUpdateEstado=$this->updateEstadoComprobanteElectronico($listaComprobantes,1);
                return $resultado;
                
            }
			else if($resultado['codRetorno']===1)
            {
                $updateResumenDiario=$this->updateResumenDiario($resumenDiarioId, $resultado);
                $listaComprobantes=$this->ResumenDiarioDetalleTable()->getComprobanteElectronicoResumenDetalle($resumenDiarioId);
                $comprobanteUpdateEstado=$this->updateEstadoComprobanteElectronico($listaComprobantes,0);
                return $resultado;
                
            }
            else if($resultado['codRetorno']===4)
            {
                return $resultado;
            }
            else
            {
                $updateResumenDiario=$this->updateResumenDiario($resumenDiarioId, $resultado);
                $listaComprobantes=$this->ResumenDiarioDetalleTable()->getComprobanteElectronicoResumenDetalle($resumenDiarioId);
                $comprobanteUpdateEstado=$this->updateEstadoComprobanteElectronico($listaComprobantes,0);
                return $resultado;
            }
          
           
        }
                
    } 
	
    public function generarResumenAction()
    {                		
        $dateTime = new \DateTime("America/Lima");
        $fechaEmision=$dateTime->format('Y-m-d');        
        $resultado=array();
        $listaFechaDocResumen=$this->listaFechaDocResumen();                    
        foreach ($listaFechaDocResumen as $fechaDocResumen){ 
            $listaComprobantes=$this->getComprobanteElectronico($fechaDocResumen['fechaEmision']);            
            $maxNumeroIdentificador=$this->maxNumeroIdentificador($fechaEmision);                        
            $parametros=array(
                'numeroIdentificador'=>$maxNumeroIdentificador,  
                'fechaEmision'=>$fechaEmision,
                'fechaDocumentoResumen'=>$fechaDocResumen['fechaEmision'],
                'resumenDetalle'=>$listaComprobantes
            );
            $idResumenDiario=$this->guardarResumen($parametros);               
            $xmlResumen=$this->generarResumenXml($idResumenDiario);
            $resultado[]=$xmlResumen;          
        }
        $view = new ViewModel(array('resultado' => $resultado));
        return $view;
    } 
    public  function  listaFechaDocResumen(){
        return  $this->ComprobanteElectronicoTable()->listaFechaDocResumen();
    }
    public function maxNumeroIdentificador($fechaEmision){     
        return $this->ResumenDiarioTable()->maxNumeroIdentificador($fechaEmision);
    }
    public  function guardarResumen($parametros){
        return $this->ResumenDiarioTable()->guardarResumen($parametros); 
    }
    public  function  getResumenDiarioId($resumenDiarioId){
        return $this->ResumenDiarioTable()->getResumenDiarioId($resumenDiarioId);       
    }
    public  function  updateResumenDiario($resumenDiarioId,$resultado){
        return $this->ResumenDiarioTable()->updateResumenDiario($resumenDiarioId,$resultado);
    }
	public  function  getConsultaResumenDiario(){
        return $this->ResumenDiarioTable()->getConsultaResumenDiario();
    }
    public function getComprobanteElectronico($fechaDocumentoResumen){
        return $this->ComprobanteElectronicoTable()->getComprobanteElectronico($fechaDocumentoResumen); 
    }  
    public function updateEstadoComprobanteElectronico($listaComprobantes,$estadoEnvioId){
        return $this->ComprobanteElectronicoTable()->updateEstadoComprobanteElectronico($listaComprobantes,$estadoEnvioId);
    }
    public function generarResumenXml($idResumenDiario){
        $resumenDiario=$this->ResumenDiarioTable()->getResumenDiarioId($idResumenDiario);         
        //cabecer resumen diario
        $resumenDiarioId=$resumenDiario['resumenDiarioId'];
        $codigoIdentificador=$resumenDiario['codigoIdentificador'];
        $fechaEmision=$resumenDiario['fechaEmision'];
        $fechaDocumentoResumen=$resumenDiario['fechaDocumentoResumen'];      
        
        //Datos de la empresa
        $datosEmpresa        = $this->EmpresaTable()->getDatosEmpresa();        
        $rucEmpresa         = $datosEmpresa['ruc'];
        $nomEmpresa         = $datosEmpresa['razonSocial'];
        $codigoPostal       = $datosEmpresa['ubigeo'];
        $dirEmpresa         = $datosEmpresa['direccion'];
        $departamento       = 'AYACUCHO';
        $provincia          = 'HUAMANGA';
        $distrito           = 'AYACUCHO';
        
        $xml = new SimpleXMLElement('<SummaryDocuments xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns="urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1" />');
        $extUES=$xml->addChild('hack:ext:UBLExtensions');
            $extUE=$extUES->addChild('hack:ext:UBLExtension');
                $extEC=$extUE->addChild('hack:ext:ExtensionContent');         
        $xml->addChild('hack:cbc:UBLVersionID', '2.0');
        $xml->addChild('hack:cbc:CustomizationID', '1.1');
        $xml->addChild('hack:cbc:ID', $codigoIdentificador);       
        $xml->addChild('hack:cbc:ReferenceDate', $fechaDocumentoResumen);
        $xml->addChild('hack:cbc:IssueDate', $fechaEmision);
        
        $cacS=$xml->addChild('hack:cac:Signature');
            $cacS->addChild('hack:cbc:ID','IDSignSP');
            $cacSP=$cacS->addChild('hack:cac:SignatoryParty');
                $cacPI=$cacSP->addChild('hack:cac:PartyIdentification');
                    $cacPI->addChild('hack:cbc:ID',$rucEmpresa);
                $cacPN=$cacSP->addChild('hack:cac:PartyName');
                    $cacPN->addChild('hack:cbc:Name',  htmlspecialchars($nomEmpresa));
            $cacDSA=$cacS->addChild('hack:cac:DigitalSignatureAttachment');
                $cacER=$cacDSA->addChild('hack:cac:ExternalReference');
                    $cacER->addChild('hack:cbc:URI', '#SignatureSP');                
                
        
        $cacASP=$xml->addChild('hack:cac:AccountingSupplierParty');
            $cacASP->addChild('hack:cbc:CustomerAssignedAccountID',$rucEmpresa);
            $cacASP->addChild('hack:cbc:AdditionalAccountID','6');
                $cacP=$cacASP->addChild('hack:cac:Party');
                    $cacPLE=$cacP->addChild('hack:cac:PartyLegalEntity');
                        $cacPLE->addChild('hack:cbc:RegistrationName', htmlspecialchars($nomEmpresa));
                        
       $listaComprobantes=$this->ResumenDiarioDetalleTable()->getComprobanteElectronicoResumenDetalle($idResumenDiario);
             
       $nroOrden=1;
       foreach ($listaComprobantes as $comprobante){          
          
           $serie     = $comprobante['serie'];
           $correlativo    = $comprobante['correlativo'];
           $tipoDocumento  = $comprobante['tipoDocumento'];
           $numeroDocCliente    = $comprobante['numeroDocCliente'];
           $tipoDocCliente  = $comprobante['tipoDocCliente'];
           $importeTotal = $comprobante['importeTotal'];
           $tipoMoneda = $comprobante['tipoMoneda'];
           $totalVentaGravadas = $comprobante['totalVentaGravadas']!=0?$comprobante['totalVentaGravadas']:"0.00";
           $totalVentaExoneradas   = $comprobante['totalVentaExoneradas'];
           $totalVentaInafectas = $comprobante['totalVentaInafectas'];
           $totalIGV           = $comprobante['sumatoriaIGV']!=null?$comprobante['sumatoriaIGV']:"0.00";
           $totalISC        =$comprobante['sumatoriaISC']!=null?$comprobante['sumatoriaISC']:"0.00";
           $estadoItem= $comprobante['estadoItem'];          
                     
           ///////-------//////
           $serieDocOrigen=$comprobante['serieDocOrigen'];
           $correlativoDocOrigen=$comprobante['correlativoDocOrigen'];
           $tipoDocOrigen=$comprobante['tipoDocOrigen'];
           
      
           $sacSDL = $xml->addChild('hack:sac:SummaryDocumentsLine');
                $sacSDL->addChild('hack:cbc:LineID', $nroOrden);
                $sacSDL->addChild('hack:cbc:DocumentTypeCode', $tipoDocumento);
                $sacSDL->addChild('hack:cbc:ID', $serie."-".$correlativo);
               
              $cacACP=$sacSDL->addChild('hack:cac:AccountingCustomerParty');
                    $cacACP->addChild('hack:cbc:CustomerAssignedAccountID', $numeroDocCliente);
                    $cacACP->addChild('hack:cbc:AdditionalAccountID', $tipoDocCliente);
              $cacS=$sacSDL->addChild('hack:cac:Status');
                $cacS->addChild('hack:cbc:ConditionCode',$estadoItem);
              $sacTA = $sacSDL->addChild('hack:sac:TotalAmount', $importeTotal);
                $sacTA->addAttribute('currencyID', $tipoMoneda);
                
             
				
			 if($totalVentaGravadas==='0.00'){
                
                 $sacBP = $sacSDL->addChild('hack:sac:BillingPayment');
                 $cbcPA=$sacBP->addChild('hack:cbc:PaidAmount','5.93');
                 $cbcPA->addAttribute('currencyID', $tipoMoneda);
                 $cbcIID = $sacBP->addChild('hack:cbc:InstructionID', '05');
             }else{
                 
                 $sacBP = $sacSDL->addChild('hack:sac:BillingPayment');
                 $cbcPA=$sacBP->addChild('hack:cbc:PaidAmount',$totalVentaGravadas);
                 $cbcPA->addAttribute('currencyID', $tipoMoneda);
                 $cbcIID = $sacBP->addChild('hack:cbc:InstructionID', '01');
             } 	
             /*   
              $sacBP = $sacSDL->addChild('hack:sac:BillingPayment');
                $cbcPA=$sacBP->addChild('hack:cbc:PaidAmount',$totalVentaExoneradas);
                    $cbcPA->addAttribute('currencyID', $tipoMoneda);
                $cbcIID = $sacBP->addChild('hack:cbc:InstructionID', '02');
            
              $sacBP = $sacSDL->addChild('hack:sac:BillingPayment');
                $cbcPA=$sacBP->addChild('hack:cbc:PaidAmount',$totalVentaInafectas);
                    $cbcPA->addAttribute('currencyID', $tipoMoneda);
                $cbcIID = $sacBP->addChild('hack:cbc:InstructionID', '03');
               */ 
              $cacTTO = $sacSDL->addChild('hack:cac:TaxTotal');
                    $cbcTAI=$cacTTO->addChild('hack:cbc:TaxAmount',$totalISC);
                        $cbcTAI->addAttribute('currencyID',$tipoMoneda);                  
                    $cacTST=$cacTTO->addChild('hack:cac:TaxSubtotal');
                        $cbcTATS=$cacTST->addChild('hack:cbc:TaxAmount',$totalISC);
                            $cbcTATS->addAttribute('currencyID',$tipoMoneda);
                        $cacTCI = $cacTST->addChild('hack:cac:TaxCategory');
                            $cacTSCH = $cacTCI->addChild('hack:cac:TaxScheme');
                                $cacTSCH->addChild('hack:cbc:ID','2000');
                                $cacTSCH->addChild('hack:cbc:Name','ISC');
                                $cacTSCH->addChild('hack:cbc:TaxTypeCode','EXC');
                                
              $cacTTO = $sacSDL->addChild('hack:cac:TaxTotal');
                    $cbcTAI=$cacTTO->addChild('hack:cbc:TaxAmount',$totalIGV);
                         $cbcTAI->addAttribute('currencyID',$tipoMoneda);
                    $cacTST=$cacTTO->addChild('hack:cac:TaxSubtotal');
                        $cbcTATS=$cacTST->addChild('hack:cbc:TaxAmount',$totalIGV);
                             $cbcTATS->addAttribute('currencyID',$tipoMoneda);
                         $cacTCI = $cacTST->addChild('hack:cac:TaxCategory');
                             $cacTSCH = $cacTCI->addChild('hack:cac:TaxScheme');
                                $cacTSCH->addChild('hack:cbc:ID','1000');
                                $cacTSCH->addChild('hack:cbc:Name','IGV');
                                $cacTSCH->addChild('hack:cbc:TaxTypeCode','VAT');
                                
              if($serieDocOrigen)
              {    
              $cacBR=$sacSDL->addChild('hack:cac:BillingReference');              
                    $cacIDR=$cacBR->addChild('hack:cac:InvoiceDocumentReference');
                        $cacIDR->addChild('hack:cbc:ID',$serieDocOrigen."-".$correlativoDocOrigen);
                        $cacIDR->addChild('hack:cbc:DocumentTypeCode',$tipoDocOrigen);
              }
                              
              $nroOrden++;
           
       }                        
       $data=$xml->asXML();       
       $tipoComprobante=21; //codigo de la base de datos.
      //$urlFisica = dirname(__FILE__);
       $urlFisica = $this->crearFolder($rucEmpresa,$codigoIdentificador);       
       $filename=$rucEmpresa.'-'.$codigoIdentificador;
       $documnt = 'secundario'; // identificar que es una factura o boleta, sirve para despues ubicar la firma digital
       $resultado = $this->SignDocument($filename, $data, $resumenDiarioId, $documnt, $urlFisica, $rucEmpresa,$tipoComprobante,$listaComprobantes);
     
       if($resultado['codRetorno']===0){
           $comprobanteUpdateEstado=$this->updateEstadoComprobanteElectronico($listaComprobantes,1);
           $resumenDiarioUpdate=$this->updateResumenDiario($resumenDiarioId, $resultado);           
       }
	   else if($resultado['codRetorno']===1){
           $comprobanteUpdateEstado=$this->updateEstadoComprobanteElectronico($listaComprobantes,0);
           $resumenDiarioUpdate=$this->updateResumenDiario($resumenDiarioId, $resultado);           
       }
       else if($resultado['codRetorno']===4){
           $resultado['codRetorno']=4;
           $comprobanteUpdateEstado=$this->updateEstadoComprobanteElectronico($listaComprobantes,4);
           $resumenDiarioUpdate=$this->updateResumenDiario($resumenDiarioId, $resultado);  
       }
		else{			 
           $comprobanteUpdateEstado=$this->updateEstadoComprobanteElectronico($listaComprobantes,$resultado['codRetorno']);
           $resumenDiarioUpdate=$this->updateResumenDiario($resumenDiarioId, $resultado['codRetorno']);
		}	
       return $resultado;    
             
    }
    public function crearFolder($rucEmpresa,$codigoIdentificador){
        //$folderDoc =  $this->tipoDocTable->getDataTipoDocumento($tipoDocumento);
        $folderDoc='resumen';
        /*** Proceso de creacion de carpetas ***/
        $carpeta = getcwd().'/tmp/'.$rucEmpresa.'/'.$folderDoc.'/'.$codigoIdentificador;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
        return $carpeta;
    }
    public function SignDocument($filename, $xml, $resumenDiarioId, $documnt, $urlFisica, $rucEmpresa,$tipoComprobante,$listaComprobantes){
        
        $doc = new DOMDocument();
        $doc->formatOutput=false;
        $doc->preseveWhiteSpace=false;
        $doc->loadXML($xml);
        $doc->encoding='ISO-8859-1';
        $doc->standalone='no';
        
        /* Codigo para ubicar en que parte del documento XML se insertara la firma dgital */
        if($documnt == 'principal'){
            $tagToSign = $doc->getElementsByTagName("ExtensionContent")->item(1);
        }else{
            $tagToSign = $doc->getElementsByTagName("ExtensionContent")->item(0);
        }
        
        /* Proceso de construccion de la firma */
        
        $objDSig= new XMLSecurityDSig();
        $objDSig->setCanonicalMethod(XMLSecurityDSig::C14N_COMMENTS);
        $objDSig->addReference($doc, XMLSecurityDSig::SHA1, array('http://www.w3.org/2000/09/xmldsig#enveloped-signature', XMLSecurityDSig::C14N_COMMENTS));
        $objKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type'=>'private'));
        /* load private key */
        $objKey->loadKey(getcwd() . '/Certificate/'.$rucEmpresa.'/certificadokey.pem', TRUE);        
        $objDSig->sign($objKey, $tagToSign);
        /* Add associated public key */
        $objDSig->add509Cert(file_get_contents(getcwd() . '/Certificate/'.$rucEmpresa.'/certificado.pem'));
        $objDSig->appendSignature($tagToSign);
        $archivo = $urlFisica.'/'.$filename.'.xml';
        
        $doc->save($archivo);
        $url = str_replace('\\', '/', getcwd());
        $filter = new Compress(array(
            'adapter' => 'Zip',
            'options' => array(
                'archive' => $urlFisica.'/'.$filename.'.zip',
                'target'  => $urlFisica,
            ),
        ));
        
        $filter->filter($archivo);
        
             
      //  $comprobanteUpdateEstado=$this->updateEstadoComprobanteElectronico($listaComprobantes);
        $resultado=$this->sendDocSunat($filename, $resumenDiarioId, $documnt, $urlFisica, $rucEmpresa, $tipoComprobante);     
        return $resultado;
        
    }
    public function sendDocSunat($filename, $resumenDiarioId, $documnt, $urlFisica, $rucEmpresa, $tipoComprobante){
        $fileName = $filename.'.zip';       
        $urlFile = $urlFisica.'/'.$fileName;
       
        #fileName:20452357268-RC-20180305-00001.zip
        
        if(file_exists($urlFile)){
            $file = file_get_contents($urlFile);
          
        }
        $projectName = explode('/',$_SERVER['REQUEST_URI'])[1];
        //  -------  pruebas  -----
        $wsdlURL = 'https://e-beta.sunat.gob.pe/ol-ti-itcpfegem-beta/billService';
        $user = $rucEmpresa.'MODDATOS';
        $password = 'moddatos';   
        //hmlg
        //$wsdlURL = 'https://www.sunat.gob.pe/ol-ti-itcpgem-sqa/billService';        
                
        //  ------   produccion  -----
        if ($projectName == 'sfe') {            
            $wsdlURL = 'https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService'; 
            $user = $rucEmpresa.'WLOPEZ29';
            $password = 'wilopez29';
        }
        $faultcode = '0';
        try {
            if($documnt == 'principal'){
                $nomfuncion="sendBill";
                $XMLString = $this->getSopMessage($file, $fileName, $user, $password, $nomfuncion,"");
                $response = $this->soapCall($wsdlURL,$nomfuncion,$XMLString,$filename, $resumenDiarioId, $urlFisica, $tipoComprobante);
                return $response;
            }else{
                $nomfuncion="sendSummary";
                $XMLString = $this->getSopMessage($file, $fileName, $user, $password, $nomfuncion,"");               
                $responseTicket = $this->soapCall($wsdlURL,$nomfuncion,$XMLString,$filename, $resumenDiarioId, $urlFisica, $tipoComprobante);               
                $ticket = $responseTicket['ticket'];                
                if($ticket){
                    $this->resumenDiarioTable->saveTicketSunat($resumenDiarioId,$ticket);                    
                    $nomfuncion="getStatus";                  
                    $XMLString = $this->getSopMessage($file, $fileName, $user, $password, $nomfuncion,$ticket);                   
                    $response = $this->soapCall($wsdlURL,$nomfuncion,$XMLString,$filename, $resumenDiarioId, $urlFisica, $tipoComprobante);
                   
                    return $response;
                }else{
                    return $responseTicket;
                }
            }
        }
        catch(SoapFault $fault){
            $result = [
                'codRetorno'        => $fault->faultcode,
                'descripcionRetorno'=> $fault->faultstring,
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];
            return $result;
        }
        
        
    }
    
    public function getSopMessage($file, $fileName, $user, $pass, $nomfuncion,$ticket){
        $XMLString= '<?xml version="1.0" encoding="UTF-8"?>
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                <soapenv:Header>
                    <wsse:Security>
                        <wsse:UsernameToken>
                            <wsse:Username>' .$user. '</wsse:Username>
                            <wsse:Password>' .$pass. '</wsse:Password>
            		    </wsse:UsernameToken>
            	    </wsse:Security>
                </soapenv:Header>
                <soapenv:Body>
                    <ser:' .$nomfuncion. '>';
      
        if($nomfuncion=='getStatus'){
            $XMLString = $XMLString .'<ticket>'. $ticket .'</ticket>';
        }else {
            $XMLString = $XMLString .'<fileName>' .$fileName. '</fileName>
                        <contentFile>'. base64_encode($file) .'</contentFile>';
        }
        $XMLString = $XMLString .'</ser:'. $nomfuncion. '>
                </soapenv:Body>
            </soapenv:Envelope>';
        return $XMLString;
    }
      
    public function soapCall($wsdlURL, $callFunction="SendBill", $XMLString,$filename, $resumenDiarioId, $urlFisica, $tipoComprobante){
         
        $faultcode = '0';
        $endpoint  = $wsdlURL;
        $uri       = 'http://service.sunat.gob.pe';
        $options=array(
            'trace'      => true,
            'location'   => $endpoint,
            'uri'        => $uri,
            'exceptions' => true,
            'cache_wsdl' => true,
        );
        
        try {
            $client = new feedSoap(null, $options);
            $client->SoapClientCall($XMLString);
            $client->__call($callFunction, array(), array());
            
        }catch (SoapFault $f) {
            if($f->faultcode=='HTTP'){
                $faultcode = '-1';
                $faultdesc = $f->faultstring;
            }else{
                $faultcode = $f->faultcode;
                $faultdesc = $f->faultstring;
            }
        }
       
        $ticket = '0';
        if($faultcode=='0'){
            $result = $client->__getLastResponse();
            
            $doc = new DOMDocument();
            $doc->loadXML($result);
            $data = $doc->getElementsByTagName('ticket');
           
            if($data->length>0){
                $ticket = $data->item(0)->nodeValue;
                $resultSunat  = array('ticket'=> $ticket);
                return $resultSunat;
            }else{
                $data = $doc->getElementsByTagName('applicationResponse');
               
                if($data->length>0){
                    $databin = $data->item(0)->nodeValue;
                }else{
                    $data = $doc->getElementsByTagName('content');
                 
                    if($data->length>0){
                        $databin = $data->item(0)->nodeValue;
                     
                    }
                }
                //var_dump($faultcode,$faultdesc,$data,$databin,$this->isNotData($databin)); exit;
                if($this->isNotData($databin)  && $databin!=null){
                    $response = base64_decode($databin);                    
                    $archivo = $urlFisica.'/R-'.$filename.'.zip';
                    file_put_contents($archivo, $response);
                    $resultSunat = $this->readResult($filename, $resumenDiarioId, $urlFisica, $tipoComprobante);
                    return $resultSunat;
                }                
                else{
                      $result = [
                        'codRetorno'        => '4',
                        'descripcionRetorno'=> 'No existe respuesta de parte de sunat',
                        'observaciones'     => [],
                        'rutaCDR'           => '',
                        'rutaPDF'           => '',
                    ];
                    
                    
                    return $result;
                }
            }
            
        }else{
            $codigoRetorno = explode('Client.', $faultcode);
            $codido = $codigoRetorno[1];
            if($codido > 0 && $codido < 2000){
                $codRetorno = 1;
            }elseif($codido > 1999 && $codido < 4000){
                $codRetorno = 2;
            }elseif($codido > 3999){
                $codRetorno = 3;
            }
            $result = [
                'codRetorno'        => $codRetorno,
                'descripcionRetorno'=> $faultdesc,
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];  
                        
            return $result;
        }
    }
   
   public function soapCall2($wsdlURL, $callFunction="SendBill", $XMLString,$filename, $resumenDiarioId, $urlFisica, $tipoComprobante){
         
        $faultcode = '0';
        $endpoint  = $wsdlURL;
        $uri       = 'http://service.sunat.gob.pe';
        $options=array(
            'trace'      => true,
            'location'   => $endpoint,
            'uri'        => $uri,
            'exceptions' => true,
            'cache_wsdl' => true,
        );
        
        try {
            $client = new feedSoap(null, $options);
            $client->SoapClientCall($XMLString);
            $client->__call($callFunction, array(), array());
            
        }catch (SoapFault $f) {
            if($f->faultcode=='HTTP'){
                $faultcode = '-1';
                $faultdesc = $f->faultstring;
            }else{
                $faultcode = $f->faultcode;
                $faultdesc = $f->faultstring;
            }
        }
       
        $ticket = '0';
        if($faultcode=='0'){
            $result = $client->__getLastResponse();
             var_dump($result); exit;
            $doc = new DOMDocument();
            $doc->loadXML($result);
            $data = $doc->getElementsByTagName('ticket');
           
            if($data->length>0){
                $ticket = $data->item(0)->nodeValue;
                $resultSunat  = array('ticket'=> $ticket);
                return $resultSunat;
            }else{
                $data = $doc->getElementsByTagName('applicationResponse');
               
                if($data->length>0){
                    $databin = $data->item(0)->nodeValue;
                }else{
                    $data = $doc->getElementsByTagName('content');
                    var_dump($data->item(0)); exit();
                    if($data->length>0){
                        $databin = $data->item(0)->nodeValue;
                     
                    }
                }
               // var_dump($faultcode,$faultdesc,$data,$databin,$this->isNotData($databin)); exit;
                if($this->isNotData($databin)){
                    $response = base64_decode($databin);                    
                    $archivo = $urlFisica.'/R-'.$filename.'.zip';
                    file_put_contents($archivo, $response);
                    $resultSunat = $this->readResult($filename, $resumenDiarioId, $urlFisica, $tipoComprobante);
                    return $resultSunat;
                }                
                else{
                    $result = [
                        'codRetorno'        => '0',
                        'descripcionRetorno'=> 'El documento fue dado de baja',
                        'observaciones'     => [],
                        'rutaCDR'           => '',
                        'rutaPDF'           => '',
                    ];
                    
                    
                    return $result;
                }
            }
            
        }else{
            $codigoRetorno = explode('Client.', $faultcode);
            $codido = $codigoRetorno[1];
            if($codido > 0 && $codido < 2000){
                $codRetorno = 1;
            }elseif($codido > 1999 && $codido < 4000){
                $codRetorno = 2;
            }elseif($codido > 3999){
                $codRetorno = 3;
            }
            $result = [
                'codRetorno'        => $codRetorno,
                'descripcionRetorno'=> $faultdesc,
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];  
                        
            return $result;
        }
    }
    
    function isNotData($test_string){
        return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $test_string);
    }
    
    function readResult($filename, $resumenDiarioId, $urlFisica, $tipoComprobante){
        $newFile = $urlFisica.'/R-'.$filename.'.zip';
        $filter = new Decompress(array(
            'adapter' => 'Zip',
            'options' => array(
                'target' => $urlFisica,
            )
        ));
        $filter->filter($newFile);
        $archive = $urlFisica.'/R-'.$filename.'.xml';
        $xmlSunat = $urlFisica.'/'.$filename.'.xml';
        if (file_exists($archive)) {
            $xml = file_get_contents($archive);            
            $xml_parser = xml_parser_create();
            xml_parse_into_struct($xml_parser, $xml, $vals);
            xml_parser_free($xml_parser);            
            $observaciones = array();
            foreach ($vals as $xml_elem) {
                $x_tag=$xml_elem['tag'];
                if($x_tag == 'CBC:RESPONSECODE'){
                    $codido = $xml_elem['value'];
                    if($codido == 0){
                        $codRetorno = 0;
                    }elseif($codido > 0 && $codido < 2000){
                        $codRetorno = 1;
                    }elseif($codido > 1999 && $codido < 4000){
                        $codRetorno = 2;
                    }elseif($codido > 3999){
                        $codRetorno = 3;
                    }
                }
                if ($x_tag == 'CBC:DESCRIPTION') {
                    $descripcionRetorno = $xml_elem['value'];
                }
                if($x_tag == 'CBC:NOTE'){
                    $observaciones[] = $xml_elem['value'];
                }
            }
            $nonSFE = 'sfe';
            $explodeUrl = explode($nonSFE, $archive);
            $explodeUrlXmlSunat = explode($nonSFE, $xmlSunat);
            $server = $_SERVER['SERVER_NAME'];
            $urlServer = 'http://'.$server.'/'.$nonSFE.$explodeUrl[1];
            $urlXmlSunat = 'http://'.$server.'/'.$nonSFE.$explodeUrlXmlSunat[1];           
            ##$urlPdf = 'http://'.$server.'/'.$nonSFE.'/public/reportes/'.$nomDocumento.'/docpdf/'.$resumenDiarioId;
            $urlPdf='';
            $result = [
                'codRetorno'        => $codRetorno,
                'descripcionRetorno'=> $descripcionRetorno,
                'observaciones'     => $observaciones,
                'rutaCDR'           => $urlServer,
                'rutaXmlSunat'      => $urlXmlSunat,
                'rutaPDF'           => $urlPdf,
            ];
            return $result;
        } else {
           // return 'El servicio de Sunat no responde';
			 $result = [
                'codRetorno'        => '4',
                'descripcionRetorno'=> 'No existe respuesta de parte de sunat',
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];
			return $result;
        }
    }
    public function getSopConsulta($file, $fileName, $user, $pass, $nomfuncion,$ticket){
        $XMLString= '<?xml version="1.0" encoding="UTF-8"?>
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                <soapenv:Header>
                    <wsse:Security>
                        <wsse:UsernameToken>
                            <wsse:Username>' .$user. '</wsse:Username>
                            <wsse:Password>' .$pass. '</wsse:Password>
            		    </wsse:UsernameToken>
            	    </wsse:Security>
                </soapenv:Header>
                <soapenv:Body>
                    <ser:' .$nomfuncion. '>';
        
        if($nomfuncion=='getStatus'){
            $XMLString = $XMLString .'<ticket>'. $ticket .'</ticket>';
        }else {
            $XMLString = $XMLString .'<fileName>' .$fileName. '</fileName>
                        <contentFile>'. base64_encode($file) .'</contentFile>';
        }
        $XMLString = $XMLString .'</ser:'. $nomfuncion. '>
                </soapenv:Body>
            </soapenv:Envelope>';
        return $XMLString;
    }
	
	public function soapCallConsulta($wsdlURL, $callFunction="SendBill", $XMLString,$filename, $resumenDiarioId, $urlFisica, $tipoComprobante){
      
        $faultcode = '0';
        $endpoint  = $wsdlURL;
        $uri       = 'http://service.sunat.gob.pe';
        $options=array(
            'trace'      => true,
            'location'   => $endpoint,
            'uri'        => $uri,
            'exceptions' => true,
            'cache_wsdl' => true,
        );
        
        try {
            $client = new feedSoap(null, $options);
            $client->SoapClientCall($XMLString);
            $client->__call($callFunction, array(), array());
            
        }catch (SoapFault $f) {
            if($f->faultcode=='HTTP'){
                $faultcode = '-1';
                $faultdesc = $f->faultstring;
            }else{
                $faultcode = $f->faultcode;
                $faultdesc = $f->faultstring;
            }
        }
        
        $ticket = '0';
        if($faultcode=='0'){
            $result = $client->__getLastResponse();
            /* $urlFisica = dirname(__FILE__);
             file_put_contents($urlFisica."/respuesta.txt", $result);
             exit();
             */
            $doc = new DOMDocument();
            $doc->loadXML($result);
            //busca el tag ticket.
            $data = $doc->getElementsByTagName('ticket');
            
            if($data->length>0){
                $ticket = $data->item(0)->nodeValue;
                $resultSunat  = array('ticket'=> $ticket);
                return $resultSunat;
            }else{
                $data = $doc->getElementsByTagName('applicationResponse');
                
                
                if($data->length>0){
                    $databin = $data->item(0)->nodeValue;
                }else{
                    $data = $doc->getElementsByTagName('content');
                    
                    
                    if($data->length>0){
                        $databin = $data->item(0)->nodeValue;
                        
                    }
                }
                //var_dump($faultcode,$faultdesc,$data,$databin,$this->isNotData($databin)); exit;
                
               
                if($this->isNotData($databin) && $databin!=null){
                    $response = base64_decode($databin);
                
                    //Local Ruta
                   // $urlFisica = dirname(__FILE__); 
                   
                    $archivo = $urlFisica.'/R-'.$filename.'.zip';
                    file_put_contents($archivo, $response); 
                    
                  
                    $resultSunat = $this->readResult($filename, $resumenDiarioId, $urlFisica, $tipoComprobante);
                    return $resultSunat;
                    
                }
                else{                                   
                    $result = [
                        'codRetorno'        => '4',
                        'descripcionRetorno'=> 'No existe respuesta de parte de sunat',
                        'observaciones'     => [],
                        'rutaCDR'           => '',
                        'rutaPDF'           => '',
                    ];
                    
                    
                    return $result;
                }
            }
            
        }else{
            $codigoRetorno = explode('Client.', $faultcode);
            $codido = $codigoRetorno[1];
            if($codido > 0 && $codido < 2000){
                $codRetorno = 1;
            }elseif($codido > 1999 && $codido < 4000){
                $codRetorno = 2;
            }elseif($codido > 3999){
                $codRetorno = 3;
            }
            $result = [
                'codRetorno'        => $codRetorno,
                'descripcionRetorno'=> $faultdesc,
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];
            
            return $result;
        }
    }
}











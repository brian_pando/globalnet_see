<?php
namespace Core\Controller;
use Zend\Mvc\Controller\AbstractActionController;

use Core\Model\Comprobante;

use SimpleXMLElement;
use Zend\Filter\Compress;
use Zend\Filter\Decompress;

use Zend\Soap\Client;
use SoapHeader;
use SoapVar;
use SoapFault;
use SoapParam;

use DOMDocument;
use Core\Service\XMLSecurityDSig;
use Core\Service\XMLSecurityKey;

use SoapClient;
use Core\Service\feedSoap;

use DOMPDFModule\View\Model\PdfModel;

class PuenteController extends AbstractActionController
{
    public $dbAdapter;
    protected $compElectTable;
    protected $compElectDetTable;
    protected $empresaTable;
    protected $tipoDocTable;
    
    public function __construct($compElectTable, $compElectDetTable, $empresaTable, $tipoDocTable){
        $this->compElectTable = $compElectTable;
        $this->compElectDetTable = $compElectDetTable;
        $this->empresaTable = $empresaTable;
        $this->tipoDocTable = $tipoDocTable;
    }

    public function generateInvoiceXml($compElectroId,$tipoComprobante){
        $cabecera = $this->compElectTable->getDataComprobante($compElectroId, $tipoComprobante);
        $comprobanteEleId   = $cabecera->id;
        $fechaEmision       = $cabecera->fechaEmision;
        $tipoDocumento      = $cabecera->tipoDocumento;
        $serie              = $cabecera->serie;
        $correlativo        = $cabecera->correlativo;
        $serieCorrelativo   = $serie.'-'.$correlativo;
        $tipoDocCliente     = $cabecera->tipoDocCliente;
        $ruc                = $cabecera->numeroDocCliente;
        $razonSocial        = $cabecera->razonSocialCliente;
        $totalVenGra        = $cabecera->totalVentaGravadas;
        $totalVenIna        = $cabecera->totalVentaInafectas;
        $totalVenExo        = $cabecera->totalVentaExoneradas;
        $totalVenGratis     = $cabecera->totalVentaGratuitas;
        $totalDescuento     = $cabecera->totalDescuentos;
        $detraccion         = $cabecera->detraccion;
        $percepcion         = $cabecera->percepcion;
        $retencion          = $cabecera->retencion;
        $bonificacion       = $cabecera->bonificacion;
        $importeTotal       = $cabecera->importeTotal;
        $importeTotalLetra  = $this->numerotexto($importeTotal);
        $tipoMoneda         = $cabecera->tipoMoneda;
        $sumatoriaIGV       = $cabecera->sumatoriaIGV;
        $descuentoGlobal    = $cabecera->descuentoGlobal;
        
        //Datos de la empresa
        $dataEmpresa        = $this->empresaTable->getDataEmpresa();
        $rucEmpresa         = $dataEmpresa->ruc;
        $nomEmpresa         = $dataEmpresa->razonSocial;
        $codigoPostal       = $dataEmpresa->ubigeo;
        $dirEmpresa         = $dataEmpresa->direccion;
        $departamento       = 'AYACUCHO';
        $provincia          = 'HUAMANGA';
        $distrito           = 'AYACUCHO';
        
        $xml = new SimpleXMLElement('<Invoice xmlns="urn:oasis:names:specification:ubl:schema:xsd:Invoice-2" '
                . 'xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" '
                . 'xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" '
                . 'xmlns:ccts="urn:un:unece:uncefact:documentation:2" '
                . 'xmlns:ds="http://www.w3.org/2000/09/xmldsig#" '
                . 'xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" '
                . 'xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" '
                . 'xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" '
                . 'xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" '
                . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />');

        $extUES = $xml->addChild('hack:ext:UBLExtensions');
            $extUE = $extUES->addChild('hack:ext:UBLExtension');
                $extEC = $extUE->addChild('hack:ext:ExtensionContent');
                    $sacAI = $extEC->addChild('hack:sac:AdditionalInformation');
                        $sacAM = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacAM->addChild('hack:cbc:ID', '1001');
                            $esp = $sacAM->addChild('hack:cbc:PayableAmount', $totalVenGra);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                    if($totalVenIna > '0.00'){
                        $sacIna = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacIna->addChild('hack:cbc:ID', '1002');
                            $esp = $sacIna->addChild('hack:cbc:PayableAmount', $totalVenIna);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                    }
                    if($totalVenExo > '0.00'){
                        $sacExo = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacExo->addChild('hack:cbc:ID', '1003');
                            $esp = $sacExo->addChild('hack:cbc:PayableAmount', $totalVenExo);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                    }
                    if($totalVenGratis > '0.00'){
                        $sacGra = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacGra->addChild('hack:cbc:ID', '1004');
                            $esp = $sacGra->addChild('hack:cbc:PayableAmount', $totalVenGratis);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                        $sacLeyenda = $sacAI->addChild('hack:sac:AdditionalProperty');
                            $sacLeyenda->addChild('hack:cbc:ID', '1002');
                            $esp = $sacLeyenda->addChild('hack:cbc:Value', 'TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE');
                    }
                    if($totalDescuento > '0.00'){
                        $sacDes = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacDes->addChild('hack:cbc:ID', '2005');
                            $esp = $sacDes->addChild('hack:cbc:PayableAmount', $totalDescuento);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                    }
                    if($detraccion > '0.00'){
                        $sacDet = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacDet->addChild('hack:cbc:ID', '2003');
                            $esp = $sacDet->addChild('hack:cbc:PayableAmount', $detraccion);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                    }
                    if($percepcion > '0.00'){
                        $sacPer = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacPer->addChild('hack:cbc:ID', '2001');
                            $esp = $sacPer->addChild('hack:cbc:PayableAmount', $percepcion);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                            $per = $sacPer->addChild('hack:sac:TotalAmount', (floatval($importeTotal)+floatval($percepcion)));
                            $per->addAttribute('currencyID', $tipoMoneda);
                    }
                    if($retencion > '0.00'){
                        $sacRet = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacRet->addChild('hack:cbc:ID', '2002');
                            $esp = $sacRet->addChild('hack:cbc:PayableAmount', $retencion);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                    }
                    if($bonificacion > '0.00'){
                        $sacBon = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacBon->addChild('hack:cbc:ID', '2004');
                            $esp = $sacBon->addChild('hack:cbc:PayableAmount', $bonificacion);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                    }
                        $sacAP = $sacAI->addChild('hack:sac:AdditionalProperty');
                            $sacAP->addChild('hack:cbc:ID', '1000');
                            $sacAP->addChild('hack:cbc:Value', htmlspecialchars($importeTotalLetra));
                    if($percepcion > '0.00'){        
                        $sacAPP = $sacAI->addChild('hack:sac:AdditionalProperty');
                            $sacAPP->addChild('hack:cbc:ID', '2000');
                            $sacAPP->addChild('hack:cbc:Value', 'COMPROBANTE DE PERCEPCION');
                    }
            $extUE2 = $extUES->addChild('hack:ext:UBLExtension');
                $extUE2->addChild('hack:ext:ExtensionContent');

        $xml->addChild('hack:cbc:UBLVersionID', '2.0');
        $xml->addChild('hack:cbc:CustomizationID', '1.0');
        $xml->addChild('hack:cbc:ID', $serieCorrelativo);
        $xml->addChild('hack:cbc:IssueDate', $fechaEmision);
        $xml->addChild('hack:cbc:InvoiceTypeCode', $tipoDocumento);
        $xml->addChild('hack:cbc:DocumentCurrencyCode', $tipoMoneda);

        $cacS = $xml->addChild('hack:cac:Signature');
            $cacS->addChild('hack:cbc:ID', 'IDSignSP');
            $cacSP = $cacS->addChild('hack:cac:SignatoryParty');
                $cacPI = $cacSP->addChild('hack:cac:PartyIdentification');
                    $cacPI->addChild('hack:cbc:ID', $rucEmpresa);
                $cacPN = $cacSP->addChild('hack:cac:PartyName');
                    $cacPN->addChild('hack:cbc:Name', htmlspecialchars($nomEmpresa));
            $cacDSA = $cacS->addChild('hack:cac:DigitalSignatureAttachment');
                $cacER = $cacDSA->addChild('hack:cac:ExternalReference');
                    $cacER->addChild('hack:cbc:URI', '#SignatureSP');

        $cacASP = $xml->addChild('hack:cac:AccountingSupplierParty');
            $cacASP->addChild('hack:cbc:CustomerAssignedAccountID', $rucEmpresa);
            $cacASP->addChild('hack:cbc:AdditionalAccountID', '6');
            $cacP = $cacASP->addChild('hack:cac:Party');
                $cacPA = $cacP->addChild('hack:cac:PostalAddress');
                    $cacPA->addChild('hack:cbc:ID', $codigoPostal);
                    $cacPA->addChild('hack:cbc:StreetName', htmlspecialchars($dirEmpresa));
                    $cacPA->addChild('hack:cbc:CitySubdivisionName', 'CALLE CENTRAL');
                    $cacPA->addChild('hack:cbc:CityName', $departamento);
                    $cacPA->addChild('hack:cbc:CountrySubentity', $provincia);
                    $cacPA->addChild('hack:cbc:District', $distrito);
                    $cacC = $cacPA->addChild('hack:cac:Country');
                        $cacC->addChild('hack:cbc:IdentificationCode', 'PE');
                $cacPLE = $cacP->addChild('hack:cac:PartyLegalEntity');
                    $cacPLE->addChild('hack:cbc:RegistrationName', htmlspecialchars($nomEmpresa));

        $cacACP = $xml->addChild('hack:cac:AccountingCustomerParty');
            $cacACP->addChild('hack:cbc:CustomerAssignedAccountID', $ruc);
            $cacACP->addChild('hack:cbc:AdditionalAccountID', $tipoDocCliente);
            $cacP2 = $cacACP->addChild('hack:cac:Party');
                $cacPLE2 = $cacP2->addChild('hack:cac:PartyLegalEntity');
                    $cacPLE2->addChild('hack:cbc:RegistrationName', htmlspecialchars($razonSocial));

        $cacTT = $xml->addChild('hack:cac:TaxTotal');
            $cbcTA = $cacTT->addChild('hack:cbc:TaxAmount', $sumatoriaIGV);
                $cbcTA->addAttribute('currencyID', $tipoMoneda);
            $cacTS = $cacTT->addChild('hack:cac:TaxSubtotal');
                $cbcTA2 = $cacTS->addChild('hack:cbc:TaxAmount', $sumatoriaIGV);
                    $cbcTA2->addAttribute('currencyID', $tipoMoneda);
                $cacTC = $cacTS->addChild('hack:cac:TaxCategory');
                    $cacTSC = $cacTC->addChild('hack:cac:TaxScheme');
                        $cacTSC->addChild('hack:cbc:ID', '1000');
                        $cacTSC->addChild('hack:cbc:Name', 'IGV');
                        $cacTSC->addChild('hack:cbc:TaxTypeCode', 'VAT');

        $cacLMT = $xml->addChild('hack:cac:LegalMonetaryTotal');
        if($descuentoGlobal > '0.00'){
            $cbcATA = $cacLMT->addChild('hack:cbc:AllowanceTotalAmount', $descuentoGlobal);
                $cbcATA->addAttribute('currencyID', $tipoMoneda);
        }
            $cbcPA = $cacLMT->addChild('hack:cbc:PayableAmount', $importeTotal);
                $cbcPA->addAttribute('currencyID', $tipoMoneda);
        
        $dataDetalle = $this->compElectDetTable->getDataDetalle($comprobanteEleId);
        $numItem = 1;
        foreach ($dataDetalle as $detalle){
            if(empty($detalle['unidadMedida'])){
                $unidadMedida = 'NIU';
            }else{
                $unidadMedida = $detalle['unidadMedida'];
            }
            $cantidad = $detalle['cantidad'];
            $valorVenta = $detalle['valorVenta'];
            $precioUnitario = $detalle['precioUnitario'];
            $igvItem = $detalle['igvItem'];
            $iscItem = $detalle['iscItem'];
            $codigo = $detalle['codigo'];
            $descripcion = $detalle['descripcion'];
            $valorUnitario = $detalle['valorUnitario'];
            $valorReferencial = $detalle['valorReferencial'];
            $codAfectacionIgv = $detalle['codAfectacionIgv'];
            $codTipoOperacion = $detalle['codTipoOperacion'];
            
            $cacII = $xml->addChild('hack:cac:InvoiceLine');
                $cacII->addChild('hack:cbc:ID', $numItem);
                $cbcIQ = $cacII->addChild('hack:cbc:InvoicedQuantity', $cantidad);
                    $cbcIQ->addAttribute('unitCode', $unidadMedida);
                $cbcLEA = $cacII->addChild('hack:cbc:LineExtensionAmount', $valorVenta);
                    $cbcLEA->addAttribute('currencyID', $tipoMoneda);
                $cacPR = $cacII->addChild('hack:cac:PricingReference');
                    $cacACPR = $cacPR->addChild('hack:cac:AlternativeConditionPrice');
                        $cacPRA = $cacACPR->addChild('hack:cbc:PriceAmount', $precioUnitario);
                            $cacPRA->addAttribute('currencyID', $tipoMoneda);
                        $cacACPR->addChild('hack:cbc:PriceTypeCode', '01');
                if($codTipoOperacion == '02'){ // esto se usar� en caso sea un bonificaci�n o un regalo
                    $cacACPR = $cacPR->addChild('hack:cac:AlternativeConditionPrice');
                        $cacPRA = $cacACPR->addChild('hack:cbc:PriceAmount', $valorReferencial);
                            $cacPRA->addAttribute('currencyID', $tipoMoneda);
                        $cacACPR->addChild('hack:cbc:PriceTypeCode', $codTipoOperacion);
                }
            if($iscItem != 0){
                $cacTTO = $cacII->addChild('hack:cac:TaxTotal');
                    $cbcTAI = $cacTTO->addChild('hack:cbc:TaxAmount', $iscItem);
                        $cbcTAI->addAttribute('currencyID', $tipoMoneda);
                    $cacTST = $cacTTO->addChild('hack:cac:TaxSubtotal');
                        $cbcTATS = $cacTST->addChild('hack:cbc:TaxAmount', $iscItem);
                            $cbcTATS->addAttribute('currencyID', $tipoMoneda);
                        $cacTCI = $cacTST->addChild('hack:cac:TaxCategory');
                            $cacTCI->addChild('hack:cbc:TierRange', '03');
                            $cacTSCH = $cacTCI->addChild('hack:cac:TaxScheme');
                                $cacTSCH->addChild('hack:cbc:ID', '2000');
                                $cacTSCH->addChild('hack:cbc:Name', 'ISC');
                                $cacTSCH->addChild('hack:cbc:TaxTypeCode', 'EXC');
            }
                $cacTTO = $cacII->addChild('hack:cac:TaxTotal');
                    $cbcTAI = $cacTTO->addChild('hack:cbc:TaxAmount', $igvItem);
                        $cbcTAI->addAttribute('currencyID', $tipoMoneda);
                    $cacTST = $cacTTO->addChild('hack:cac:TaxSubtotal');
                        $cbcTATS = $cacTST->addChild('hack:cbc:TaxAmount', $igvItem);
                            $cbcTATS->addAttribute('currencyID', $tipoMoneda);
                        $cacTCI = $cacTST->addChild('hack:cac:TaxCategory');
                            $cacTCI->addChild('hack:cbc:TaxExemptionReasonCode', $codAfectacionIgv);
                            $cacTSCH = $cacTCI->addChild('hack:cac:TaxScheme');
                                $cacTSCH->addChild('hack:cbc:ID', '1000');
                                $cacTSCH->addChild('hack:cbc:Name', 'IGV');
                                $cacTSCH->addChild('hack:cbc:TaxTypeCode', 'VAT');
                $cacI = $cacII->addChild('hack:cac:Item');
                    $cacI->addChild('hack:cbc:Description', htmlspecialchars($descripcion));
                    $cacSII = $cacI->addChild('hack:cac:SellersItemIdentification');
                        $cacSII->addChild('hack:cbc:ID', $codigo);
                $cacPRI = $cacII->addChild('hack:cac:Price');
                    $cbcPAM = $cacPRI->addChild('hack:cbc:PriceAmount', $valorUnitario);
                        $cbcPAM->addAttribute('currencyID', $tipoMoneda);
            $numItem++;
        }
        $data = $xml->asXml();

        /******** Creacion de carpeta para el documento **********/
        $urlFisica = $this->createFolder($rucEmpresa,$tipoDocumento,$serieCorrelativo);
        
        $filename = $rucEmpresa.'-'.$tipoDocumento.'-'.$serieCorrelativo;
        $documnt = 'principal'; // identificar que es una factura o boleta, sirve para despues ubicar la firma digital
        $resultado = $this->SignDocument($filename, $data, $compElectroId, $documnt, $urlFisica, $rucEmpresa,$tipoComprobante);
        return $resultado;
    }

    public function generateNoteXml($compElectroId,$tipoComprobante){
        $cabecera = $this->compElectTable->getDataComprobante($compElectroId, $tipoComprobante);
        
        /****** Extras para analizar ******/
        $totalVenIna        = $cabecera->totalVentaInafectas;
        $totalVenExo        = $cabecera->totalVentaExoneradas;
        $totalVenGratis     = $cabecera->totalVentaGratuitas;
        $totalDescuento     = $cabecera->totalDescuentos;
        $detraccion         = $cabecera->detraccion;
        $percepcion         = $cabecera->percepcion;
        $retencion          = $cabecera->retencion;
        $bonificacion       = $cabecera->bonificacion;
        /******* extras fin *******/
        
        $comprobanteEleId   = $cabecera->id;
        $fechaEmision       = $cabecera->fechaEmision;
        $tipoDocumento      = $cabecera->tipoDocumento;
        $serie              = $cabecera->serie;
        $correlativo        = $cabecera->correlativo;
        $serieCorrelativoCN = $serie.'-'.$correlativo;
        $totalVenGra        = $cabecera->totalVentaGravadas;
        $sumatoriaIGV       = $cabecera->sumatoriaIGV;
        $descuentoGlobal    = $cabecera->descuentoGlobal;
        $importeTotal       = $cabecera->importeTotal;
        $importeTotalLetra  = $this->numerotexto($importeTotal);
        $motivo             = $cabecera->motivoNota;
        $tipoMoneda         = $cabecera->tipoMoneda;
        
        // datos de la factura referenciada
        $serieCorrelOrigen  = $cabecera->serieDocOrigen.'-'.$cabecera->correlativoDocOrigen;
        $tipoDocOrigen      = $cabecera->tipoDocOrigen;
        $codigoTipoDoc      = $cabecera->codigoTipoDoc;
        
        //Datos de la empresa
        $dataEmpresa        = $this->empresaTable->getDataEmpresa();
        $rucEmpresa         = $dataEmpresa->ruc;
        $nomEmpresa         = $dataEmpresa->razonSocial;
        $codigoPostal       = $dataEmpresa->ubigeo;
        $dirEmpresa         = $dataEmpresa->direccion;
        $departamento       = 'AYACUCHO';
        $provincia          = 'HUAMANGA';
        $distrito           = 'AYACUCHO';
        
        //Datos del cliente
        $tipoDocCliente     = $cabecera->tipoDocCliente;
        $ruc                = $cabecera->numeroDocCliente;
        $razonSocial        = $cabecera->razonSocialCliente;        

        if($tipoDocumento == '07'){
            $xml = new SimpleXMLElement('<CreditNote xmlns="urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" />');
        }else{
            $xml = new SimpleXMLElement('<DebitNote xmlns="urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:ccts="urn:un:unece:uncefact:documentation:2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" />');
        }
        $extUES = $xml->addChild('hack:ext:UBLExtensions');
            $extUE = $extUES->addChild('hack:ext:UBLExtension');
                $extEC = $extUE->addChild('hack:ext:ExtensionContent');
                    $sacAI = $extEC->addChild('hack:sac:AdditionalInformation');
                        $sacAM = $sacAI->addChild('hack:sac:AdditionalMonetaryTotal');
                            $sacAM->addChild('hack:cbc:ID', '1001');
                            $esp = $sacAM->addChild('hack:cbc:PayableAmount', $totalVenGra);
                            $esp->addAttribute('currencyID', $tipoMoneda);
                        $sacAP = $sacAI->addChild('hack:sac:AdditionalProperty');
                            $sacAP->addChild('hack:cbc:ID', '1000');
                            $sacAP->addChild('hack:cbc:Value', htmlspecialchars($importeTotalLetra));
            $extUE2 = $extUES->addChild('hack:ext:UBLExtension');
                $extUE2->addChild('hack:ext:ExtensionContent');

        $xml->addChild('hack:cbc:UBLVersionID', '2.0');
        $xml->addChild('hack:cbc:CustomizationID', '1.0');
        $xml->addChild('hack:cbc:ID', $serieCorrelativoCN);
        $xml->addChild('hack:cbc:IssueDate', $fechaEmision);
        $xml->addChild('hack:cbc:DocumentCurrencyCode', $tipoMoneda);

        $cacDR = $xml->addChild('hack:cac:DiscrepancyResponse');
            $cacDR->addChild('hack:cbc:ReferenceID', $serieCorrelOrigen);
            $cacDR->addChild('hack:cbc:ResponseCode', $codigoTipoDoc);
            $cacDR->addChild('hack:cbc:Description', htmlspecialchars($motivo));

        $cacBR = $xml->addChild('hack:cac:BillingReference');
            $cacIDR = $cacBR->addChild('hack:cac:InvoiceDocumentReference');
                $cacIDR->addChild('hack:cbc:ID', $serieCorrelOrigen);
                $cacIDR->addChild('hack:cbc:DocumentTypeCode', $tipoDocOrigen); // 01 = tipo de documento factura

        $cacS = $xml->addChild('hack:cac:Signature');
            $cacS->addChild('hack:cbc:ID', 'IDSignST');
            $cacSP = $cacS->addChild('hack:cac:SignatoryParty');
                $cacPI = $cacSP->addChild('hack:cac:PartyIdentification');
                    $cacPI->addChild('hack:cbc:ID', $rucEmpresa);
                $cacPN = $cacSP->addChild('hack:cac:PartyName');
                    $cacPN->addChild('hack:cbc:Name', htmlspecialchars($nomEmpresa));
            $cacDSA = $cacS->addChild('hack:cac:DigitalSignatureAttachment');
                $cacER = $cacDSA->addChild('hack:cac:ExternalReference');
                    $cacER->addChild('hack:cbc:URI', '#SignatureSP');

        $cacASP = $xml->addChild('hack:cac:AccountingSupplierParty');
            $cacASP->addChild('hack:cbc:CustomerAssignedAccountID', $rucEmpresa);
            $cacASP->addChild('hack:cbc:AdditionalAccountID', '6');
            $cacP = $cacASP->addChild('hack:cac:Party');
                $cacPA = $cacP->addChild('hack:cac:PostalAddress');
                    $cacPA->addChild('hack:cbc:ID', $codigoPostal);
                    $cacPA->addChild('hack:cbc:StreetName', htmlspecialchars($dirEmpresa));
                    $cacPA->addChild('hack:cbc:CitySubdivisionName', 'CALLE CENTRAL');
                    $cacPA->addChild('hack:cbc:CityName', $departamento);
                    $cacPA->addChild('hack:cbc:CountrySubentity', $provincia);
                    $cacPA->addChild('hack:cbc:District', $distrito);
                    $cacC = $cacPA->addChild('hack:cac:Country');
                        $cacC->addChild('hack:cbc:IdentificationCode', 'PE');
                $cacPLE = $cacP->addChild('hack:cac:PartyLegalEntity');
                    $cacPLE->addChild('hack:cbc:RegistrationName', htmlspecialchars($nomEmpresa));

        $cacACP = $xml->addChild('hack:cac:AccountingCustomerParty');
            $cacACP->addChild('hack:cbc:CustomerAssignedAccountID', $ruc);
            $cacACP->addChild('hack:cbc:AdditionalAccountID', $tipoDocCliente);
            $cacP2 = $cacACP->addChild('hack:cac:Party');
                $cacPLE2 = $cacP2->addChild('hack:cac:PartyLegalEntity');
                    $cacPLE2->addChild('hack:cbc:RegistrationName', $razonSocial);

        $cacTT = $xml->addChild('hack:cac:TaxTotal');
            $cbcTA = $cacTT->addChild('hack:cbc:TaxAmount', $sumatoriaIGV);
                $cbcTA->addAttribute('currencyID', $tipoMoneda);
            $cacTS = $cacTT->addChild('hack:cac:TaxSubtotal');
                $cbcTA2 = $cacTS->addChild('hack:cbc:TaxAmount', $sumatoriaIGV);
                    $cbcTA2->addAttribute('currencyID', $tipoMoneda);
                $cacTC = $cacTS->addChild('hack:cac:TaxCategory');
                    $cacTSC = $cacTC->addChild('hack:cac:TaxScheme');
                        $cacTSC->addChild('hack:cbc:ID', '1000');
                        $cacTSC->addChild('hack:cbc:Name', 'IGV');
                        $cacTSC->addChild('hack:cbc:TaxTypeCode', 'VAT');

        if($tipoDocumento == '07'){
            $cacLMT = $xml->addChild('hack:cac:LegalMonetaryTotal');
        }else{
            $cacLMT = $xml->addChild('hack:cac:RequestedMonetaryTotal');
        }
        
        if($descuentoGlobal > '0.00'){
            $cbcATA = $cacLMT->addChild('hack:cbc:AllowanceTotalAmount', $descuentoGlobal);
                $cbcATA->addAttribute('currencyID', $tipoMoneda);
        }
            $cbcPA = $cacLMT->addChild('hack:cbc:PayableAmount', $importeTotal);
                $cbcPA->addAttribute('currencyID', $tipoMoneda);

        $dataDetalle = $this->compElectDetTable->getDataDetalle($comprobanteEleId);

        $numItem = 1;
        foreach ($dataDetalle as $detalle){
            if(empty($detalle['unidadMedida'])){
                $unidadMedida = 'NIU';
            }else{
                $unidadMedida = $detalle['unidadMedida'];
            }
            $cantidad = $detalle['cantidad'];
            $valorVenta = $detalle['valorVenta'];
            $precioUnitario = $detalle['precioUnitario'];
            $igvItem = $detalle['igvItem'];
            $iscItem = $detalle['iscItem'];
            $codigo = $detalle['codigo'];
            $descripcion = $detalle['descripcion'];
            $valorUnitario = $detalle['valorUnitario'];
            $valorReferencial = $detalle['valorReferencial'];
            $codAfectacionIgv = $detalle['codAfectacionIgv'];
            $codTipoOperacion = $detalle['codTipoOperacion'];
            

            if($tipoDocumento == '07'){
                $cacCNI = $xml->addChild('hack:cac:CreditNoteLine');
            }else{
                $cacCNI = $xml->addChild('hack:cac:DebitNoteLine');
            }
                $cacCNI->addChild('hack:cbc:ID', $numItem);
                if($tipoDocumento == '07'){
                    $cbcIQ = $cacCNI->addChild('hack:cbc:CreditedQuantity', $cantidad);
                }else{
                    $cbcIQ = $cacCNI->addChild('hack:cbc:DebitedQuantity', $cantidad);
                }
                    $cbcIQ->addAttribute('unitCode', $unidadMedida);
                $cbcLEA = $cacCNI->addChild('hack:cbc:LineExtensionAmount', $valorVenta);
                    $cbcLEA->addAttribute('currencyID', $tipoMoneda);
                $cacPR = $cacCNI->addChild('hack:cac:PricingReference');
                    $cacACPR = $cacPR->addChild('hack:cac:AlternativeConditionPrice');
                        $cacPRA = $cacACPR->addChild('hack:cbc:PriceAmount', $precioUnitario);
                            $cacPRA->addAttribute('currencyID', $tipoMoneda);
                        $cacACPR->addChild('hack:cbc:PriceTypeCode', '01');
                if($codTipoOperacion == '02'){ // esto se usar� en caso sea un bonificaci�n o un regalo
                    $cacACPR = $cacPR->addChild('hack:cac:AlternativeConditionPrice');
                        $cacPRA = $cacACPR->addChild('hack:cbc:PriceAmount', $valorReferencial);
                            $cacPRA->addAttribute('currencyID', $tipoMoneda);
                        $cacACPR->addChild('hack:cbc:PriceTypeCode', $codTipoOperacion);
                }
                $cacTTO = $cacCNI->addChild('hack:cac:TaxTotal');
                    $cbcTAI = $cacTTO->addChild('hack:cbc:TaxAmount', $igvItem);
                        $cbcTAI->addAttribute('currencyID', $tipoMoneda);
                    $cacTST = $cacTTO->addChild('hack:cac:TaxSubtotal');
                        $cbcTATS = $cacTST->addChild('hack:cbc:TaxAmount', $igvItem);
                            $cbcTATS->addAttribute('currencyID', $tipoMoneda);
                        $cacTCI = $cacTST->addChild('hack:cac:TaxCategory');
                            $cacTCI->addChild('hack:cbc:TaxExemptionReasonCode', $codAfectacionIgv);
                            $cacTSCH = $cacTCI->addChild('hack:cac:TaxScheme');
                                $cacTSCH->addChild('hack:cbc:ID', '1000');
                                $cacTSCH->addChild('hack:cbc:Name', 'IGV');
                                $cacTSCH->addChild('hack:cbc:TaxTypeCode', 'VAT');
                $cacI = $cacCNI->addChild('hack:cac:Item');
                    $cacI->addChild('hack:cbc:Description', htmlspecialchars($descripcion));
                    $cacSII = $cacI->addChild('hack:cac:SellersItemIdentification');
                        $cacSII->addChild('hack:cbc:ID', $codigo);
                $cacPRI = $cacCNI->addChild('hack:cac:Price');
                    $cbcPAM = $cacPRI->addChild('hack:cbc:PriceAmount', $valorUnitario);
                        $cbcPAM->addAttribute('currencyID', $tipoMoneda);
            $numItem++;
        }
        $data = $xml->asXml();
        
        /******** Creacion de carpeta para el documento **********/
        $urlFisica = $this->createFolder($rucEmpresa,$tipoDocumento,$serieCorrelativoCN);
        
        $filename = $rucEmpresa.'-'.$tipoDocumento.'-'.$serieCorrelativoCN;
        $documnt = 'principal'; // principal = factura, boleta, nota de credito o debito, sino documento de baja o resumen de boletas, esto sirve para despues ubicar la firma digital
        $resultado = $this->SignDocument($filename, $data, $comprobanteEleId, $documnt, $urlFisica, $rucEmpresa, $tipoComprobante);
        return $resultado;
    }
    
    public function generateBajaXml($docBajaId){
        $cabecera = $this->compElectTable->fetchOne($docBajaId);

        $codigoIdentificador = $cabecera->codigoIdentificador;
        $fechaEmision = $cabecera->fechaEmision;
        $fechaDocumentoBaja = $cabecera->fechaDocumentoBaja;
        
        //Datos de la empresa
        $dataEmpresa        = $this->empresaTable->getDataEmpresa();
        $rucEmpresa         = $dataEmpresa->ruc;
        $nomEmpresa         = $dataEmpresa->razonSocial;
        $codigoPostal       = $dataEmpresa->ubigeo;
        $dirEmpresa         = $dataEmpresa->direccion;
        $departamento       = 'AYACUCHO';
        $provincia          = 'HUAMANGA';
        $distrito           = 'AYACUCHO';
        
        $xml = new SimpleXMLElement('<VoidedDocuments xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns="urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1" />');

        $extUES = $xml->addChild('hack:ext:UBLExtensions');
            $extUE = $extUES->addChild('hack:ext:UBLExtension');
                $extEC = $extUE->addChild('hack:ext:ExtensionContent');
                
        $xml->addChild('hack:cbc:UBLVersionID', '2.0');
        $xml->addChild('hack:cbc:CustomizationID', '1.0');
        $xml->addChild('hack:cbc:ID', $codigoIdentificador);
        $xml->addChild('hack:cbc:ReferenceDate', $fechaDocumentoBaja);
        $xml->addChild('hack:cbc:IssueDate', $fechaEmision);
        
        $cacS = $xml->addChild('hack:cac:Signature');
            $cacS->addChild('hack:cbc:ID', 'IDSignSP');
            $cacSP = $cacS->addChild('hack:cac:SignatoryParty');
                $cacPI = $cacSP->addChild('hack:cac:PartyIdentification');
                    $cacPI->addChild('hack:cbc:ID', $rucEmpresa);
                $cacPN = $cacSP->addChild('hack:cac:PartyName');
                    $cacPN->addChild('hack:cbc:Name', htmlspecialchars($nomEmpresa));
            $cacDSA = $cacS->addChild('hack:cac:DigitalSignatureAttachment');
                $cacER = $cacDSA->addChild('hack:cac:ExternalReference');
                    $cacER->addChild('hack:cbc:URI', '#SignatureSP');
                    
        $cacASP = $xml->addChild('hack:cac:AccountingSupplierParty');
            $cacASP->addChild('hack:cbc:CustomerAssignedAccountID', $rucEmpresa);
            $cacASP->addChild('hack:cbc:AdditionalAccountID', '6');
            $cacP = $cacASP->addChild('hack:cac:Party');
                $cacPLE = $cacP->addChild('hack:cac:PartyLegalEntity');
                    $cacPLE->addChild('hack:cbc:RegistrationName', htmlspecialchars($nomEmpresa));
            
        $dataDetalle = $this->compElectDetTable->getDataBajaDetalle($docBajaId);
        foreach ($dataDetalle as $detBaja){
            $numeroOrden = $detBaja['numeroOrden'];
            $tipoDocOrigen = $detBaja['tipoDocOrigen'];
            $serieDocOrigen = $detBaja['serieDocOrigen'];
            $correlativoDocOrigen = $detBaja['correlativoDocOrigen'];
            $motivoBaja = $detBaja['motivoBaja'];
            
            $sacVDL = $xml->addChild('hack:sac:VoidedDocumentsLine');
                $sacVDL->addChild('hack:cbc:LineID', $numeroOrden);
                $sacVDL->addChild('hack:cbc:DocumentTypeCode', $tipoDocOrigen);
                $sacVDL->addChild('hack:sac:DocumentSerialID', $serieDocOrigen);
                $sacVDL->addChild('hack:sac:DocumentNumberID', $correlativoDocOrigen);
                $sacVDL->addChild('hack:sac:VoidReasonDescription', $motivoBaja);
        }
        $data = $xml->asXml();
        
        /******** Creacion de carpeta para el documento **********/
        $tipoDocumento = 20; // 20 representa al tipo de documento de baja lo elegi a mi criterio
        $urlFisica = $this->createFolder($rucEmpresa,$tipoDocumento,$codigoIdentificador);
        
        $filename = $rucEmpresa.'-'.$codigoIdentificador;
        $documnt = 'secundario'; // identificar que es una factura o boleta, sirve para despues ubicar la firma digital
        $resultado = $this->SignDocument($filename, $data, $codigoIdentificador, $documnt, $urlFisica, $rucEmpresa);
        return $resultado;
    }
    
    public function generateResumenXml($docResumenId){
        $cabecera = $this->compElectTable->fetchOne($docResumenId);

        $codigoIdentificador    = $cabecera->codigoIdentificador;
        $fechaEmision           = $cabecera->fechaEmision;
        $fechaDocumentoResumen  = $cabecera->fechaDocumentoResumen;
        
        //Datos de la empresa
        $dataEmpresa        = $this->empresaTable->getDataEmpresa();
        $rucEmpresa         = $dataEmpresa->ruc;
        $nomEmpresa         = $dataEmpresa->razonSocial;
        $codigoPostal       = $dataEmpresa->ubigeo;
        $dirEmpresa         = $dataEmpresa->direccion;
        $departamento       = 'AYACUCHO';
        $provincia          = 'HUAMANGA';
        $distrito           = 'AYACUCHO';

        $xml = new SimpleXMLElement('<SummaryDocuments xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:qdt="urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2" xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2" xmlns:sac="urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1" xmlns:udt="urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2" xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2" xmlns:ext="urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns="urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1" />');

        $extUES = $xml->addChild('hack:ext:UBLExtensions');
            $extUE = $extUES->addChild('hack:ext:UBLExtension');
                $extEC = $extUE->addChild('hack:ext:ExtensionContent');
        
        $xml->addChild('hack:cbc:UBLVersionID', '2.0');
        $xml->addChild('hack:cbc:CustomizationID', '1.0');
        $xml->addChild('hack:cbc:ID', $codigoIdentificador);
        $xml->addChild('hack:cbc:ReferenceDate', $fechaDocumentoResumen);
        $xml->addChild('hack:cbc:IssueDate', $fechaEmision);
        
        $cacS = $xml->addChild('hack:cac:Signature');
            $cacS->addChild('hack:cbc:ID', 'IDSignSP');
            $cacSP = $cacS->addChild('hack:cac:SignatoryParty');
                $cacPI = $cacSP->addChild('hack:cac:PartyIdentification');
                    $cacPI->addChild('hack:cbc:ID', $rucEmpresa);
                $cacPN = $cacSP->addChild('hack:cac:PartyName');
                    $cacPN->addChild('hack:cbc:Name', htmlspecialchars($nomEmpresa));
            $cacDSA = $cacS->addChild('hack:cac:DigitalSignatureAttachment');
                $cacER = $cacDSA->addChild('hack:cac:ExternalReference');
                    $cacER->addChild('hack:cbc:URI', '#SignatureSP');
        
        $cacASP = $xml->addChild('hack:cac:AccountingSupplierParty');
            $cacASP->addChild('hack:cbc:CustomerAssignedAccountID', $rucEmpresa);
            $cacASP->addChild('hack:cbc:AdditionalAccountID', '6');
            $cacP = $cacASP->addChild('hack:cac:Party');
                $cacPLE = $cacP->addChild('hack:cac:PartyLegalEntity');
                    $cacPLE->addChild('hack:cbc:RegistrationName', htmlspecialchars($nomEmpresa));

        $dataDetalle = $this->compElectDetTable->getDataResumenDetalle($docResumenId);
        foreach ($dataDetalle as $detResu){
            $numeroOrden        = $detResu['numeroOrden'];
            $tipoDocOrigen      = $detResu['tipoDocOrigen'];
            $serieDocOrigen     = $detResu['serieDocOrigen'];
            $inicioCorrelativo  = $detResu['inicioCorrelativo'];
            $finCorrelativo     = $detResu['finCorrelativo'];
            $valorVentaGravada  = $detResu['valorVentaGravada'];
            $valorVentaExonerada = $detResu['valorVentaExonerada'];
            $valorVentaInafecta = $detResu['valorVentaInafecta'];
            $totalOtrosCargos   = $detResu['totalOtrosCargos'];
            $totalOtrosTributos = $detResu['totalOtrosTributos'];
            $totalIGV           = $detResu['totalIGV'];
            $totalISC           = $detResu['totalISC'];
            $importeTotal       = $detResu['importeTotal'];
            $tipoMoneda         = $detResu['tipoMoneda'];
            
            $sacSDL = $xml->addChild('hack:sac:SummaryDocumentsLine');
                $sacSDL->addChild('hack:cbc:LineID', $numeroOrden);
                $sacSDL->addChild('hack:cbc:DocumentTypeCode', $tipoDocOrigen);
                $sacSDL->addChild('hack:sac:DocumentSerialID', $serieDocOrigen);
                $sacSDL->addChild('hack:sac:StartDocumentNumberID', $inicioCorrelativo);
                $sacSDL->addChild('hack:sac:EndDocumentNumberID', $finCorrelativo);
                $sacTA = $sacSDL->addChild('hack:sac:TotalAmount', $importeTotal);
                    $sacTA->addAttribute('currencyID', $tipoMoneda);
                    
                $sacBP = $sacSDL->addChild('hack:sac:BillingPayment');
                    $cbcPA = $sacBP->addChild('hack:cbc:PaidAmount', $valorVentaGravada);
                        $cbcPA->addAttribute('currencyID', $tipoMoneda);
                    $cbcIID = $sacBP->addChild('hack:cbc:InstructionID', '01');
                
                $sacBP = $sacSDL->addChild('hack:sac:BillingPayment');
                    $cbcPA = $sacBP->addChild('hack:cbc:PaidAmount', $valorVentaExonerada);
                        $cbcPA->addAttribute('currencyID', $tipoMoneda);
                    $cbcIID = $sacBP->addChild('hack:cbc:InstructionID', '02');
                
                $sacBP = $sacSDL->addChild('hack:sac:BillingPayment');
                    $cbcPA = $sacBP->addChild('hack:cbc:PaidAmount', $valorVentaInafecta);
                        $cbcPA->addAttribute('currencyID', $tipoMoneda);
                    $cbcIID = $sacBP->addChild('hack:cbc:InstructionID', '03');
                    
                $cacAC = $sacSDL->addChild('hack:cac:AllowanceCharge');
                    $cacAC->addChild('hack:cbc:ChargeIndicator', 'true');
                    $cbcAAC = $cacAC->addChild('hack:cbc:Amount', $totalOtrosCargos);
                        $cbcAAC->addAttribute('currencyID', $tipoMoneda);
                        
                $cacTTO = $sacSDL->addChild('hack:cac:TaxTotal');
                    $cbcTAI = $cacTTO->addChild('hack:cbc:TaxAmount', $totalISC);
                        $cbcTAI->addAttribute('currencyID', $tipoMoneda);
                    $cacTST = $cacTTO->addChild('hack:cac:TaxSubtotal');
                        $cbcTATS = $cacTST->addChild('hack:cbc:TaxAmount', $totalISC);
                            $cbcTATS->addAttribute('currencyID', $tipoMoneda);
                        $cacTCI = $cacTST->addChild('hack:cac:TaxCategory');
                            $cacTSCH = $cacTCI->addChild('hack:cac:TaxScheme');
                                $cacTSCH->addChild('hack:cbc:ID', '2000');
                                $cacTSCH->addChild('hack:cbc:Name', 'ISC');
                                $cacTSCH->addChild('hack:cbc:TaxTypeCode', 'EXC');
                                
                $cacTTO = $sacSDL->addChild('hack:cac:TaxTotal');
                    $cbcTAI = $cacTTO->addChild('hack:cbc:TaxAmount', $totalIGV);
                        $cbcTAI->addAttribute('currencyID', $tipoMoneda);
                    $cacTST = $cacTTO->addChild('hack:cac:TaxSubtotal');
                        $cbcTATS = $cacTST->addChild('hack:cbc:TaxAmount', $totalIGV);
                            $cbcTATS->addAttribute('currencyID', $tipoMoneda);
                        $cacTCI = $cacTST->addChild('hack:cac:TaxCategory');
                            $cacTSCH = $cacTCI->addChild('hack:cac:TaxScheme');
                                $cacTSCH->addChild('hack:cbc:ID', '1000');
                                $cacTSCH->addChild('hack:cbc:Name', 'IGV');
                                $cacTSCH->addChild('hack:cbc:TaxTypeCode', 'VAT');

        }
        $data = $xml->asXml();
        
        /******** Creacion de carpeta para el documento **********/
        $tipoDocumento = 21; // 21 representa al tipo de documento de resumenes lo elegi a mi criterio
        $urlFisica = $this->createFolder($rucEmpresa,$tipoDocumento,$codigoIdentificador);
        
        $filename = $rucEmpresa.'-'.$codigoIdentificador;
        $documnt = 'secundario'; // identificar que es una factura o boleta, sirve para despues ubicar la firma digital
        $resultado = $this->SignDocument($filename, $data, $codigoIdentificador, $documnt, $urlFisica, $rucEmpresa);
        return $resultado;
    }

    public function SignDocument($filename, $xml, $compElectroId, $documnt, $urlFisica, $rucEmpresa, $tipoComprobante){
        $doc = new DOMDocument();
        $doc->formatOutput = false;
        $doc->preserveWhiteSpace = false;
        $doc->loadXML($xml);
        $doc->encoding = 'ISO-8859-1';
        $doc->standalone = 'no';
    
        /* Codigo para ubicar en que parte del documento XML se insertara la firma dgital */
        if($documnt == 'principal'){
            $tagToSign = $doc->getElementsByTagName("ExtensionContent")->item(1);
        }else{
            $tagToSign = $doc->getElementsByTagName("ExtensionContent")->item(0);
        }
    
        /* Proceso de construccion de la firma */
        $objDSig = new XMLSecurityDSig();
        $objDSig->setCanonicalMethod(XMLSecurityDSig::C14N_COMMENTS);
        $objDSig->addReference($doc, XMLSecurityDSig::SHA1, array('http://www.w3.org/2000/09/xmldsig#enveloped-signature', XMLSecurityDSig::C14N_COMMENTS));
        $objKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA1, array('type'=>'private'));
        /* load private key */
        $objKey->loadKey(getcwd() . '/Certificate/'.$rucEmpresa.'/certificadokey.pem', TRUE);
        $objDSig->sign($objKey, $tagToSign);
        /* Add associated public key */
        $objDSig->add509Cert(file_get_contents(getcwd() . '/Certificate/'.$rucEmpresa.'/certificado.pem'));
        $objDSig->appendSignature($tagToSign);
        $archivo = $urlFisica.'/'.$filename.'.xml';
        
        $doc->save($archivo);
    
        $url = str_replace('\\', '/', getcwd());
        $filter = new Compress(array(
            'adapter' => 'Zip',
            'options' => array(
                'archive' => $urlFisica.'/'.$filename.'.zip',
                'target'  => $urlFisica,
            ),
        ));
        $filter->filter($archivo);
        $result = $this->sendDocSunat($filename, $compElectroId, $documnt, $urlFisica, $rucEmpresa, $tipoComprobante);
        return $result;
    }
        
    public function sendDocSunat($filename, $compElectroId, $documnt, $urlFisica, $rucEmpresa, $tipoComprobante){
        $fileName = $filename.'.zip';
        $urlFile = $urlFisica.'/'.$fileName;
        if(file_exists($urlFile)){
            $file = file_get_contents($urlFile);
        }
        $wsdlURL = 'https://e-factura.sunat.gob.pe/ol-ti-itcpfegem/billService';
        $user = $rucEmpresa.'WLOPEZ29';
        $password = 'wilopez29';
        $faultcode = '0';
        try {
            if($documnt == 'principal'){
                $nomfuncion="sendBill";
                $XMLString = $this->getSopMessage($file, $fileName, $user, $password, $nomfuncion);
                $response = $this->soapCall($wsdlURL,$nomfuncion,$XMLString,$filename, $compElectroId, $urlFisica, $tipoComprobante);
                return $response;
            }else{
                $nomfuncion="sendSummary";
                $XMLString = $this->getSopMessage($file, $fileName, $user, $password, $nomfuncion);
                $responseTicket = $this->soapCall($wsdlURL,$nomfuncion,$XMLString,$filename, $compElectroId, $urlFisica, $tipoComprobante);
                $ticket = $responseTicket['ticket'];
                if($ticket){
                    $this->compElectTable->saveTicketSunat($compElectroId,$ticket);
                    $nomfuncion="getStatus";
                    $XMLString = $this->getSopMessage($file, $fileName, $user, $password, $nomfuncion);
                    $response = $this->soapCall($wsdlURL,$nomfuncion,$XMLString,$filename, $compElectroId, $urlFisica, $tipoComprobante);
                    return $response;
                }
            }
            
        }
        catch(SoapFault $fault){
            $result = [
                'codRetorno'        => $fault->faultcode,
                'descripcionRetorno'=> $fault->faultstring,
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];
            return $result;
        }
    }
    
    public function getSopMessage($file, $fileName, $user, $pass, $nomfuncion){
        $XMLString= '<?xml version="1.0" encoding="UTF-8"?>
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://service.sunat.gob.pe" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                <soapenv:Header>
                    <wsse:Security>
                        <wsse:UsernameToken>
                            <wsse:Username>' .$user. '</wsse:Username>
                            <wsse:Password>' .$pass. '</wsse:Password>
            		    </wsse:UsernameToken>
            	    </wsse:Security>
                </soapenv:Header>
                <soapenv:Body>
                    <ser:' .$nomfuncion. '>';
                    if($nomfuncion=='getStatus'){
                        $XMLString = $XMLString .'<ticket>'. $fileName .'</ticket>';
                    }else {
                        $XMLString = $XMLString .'<fileName>' .$fileName. '</fileName>
                        <contentFile>'. base64_encode($file) .'</contentFile>';
                    }
                    $XMLString = $XMLString .'</ser:'. $nomfuncion. '>
                </soapenv:Body>
            </soapenv:Envelope>';
        return $XMLString;
    }
    
    public function soapCall($wsdlURL, $callFunction="SendBill", $XMLString,$filename, $compElectroId, $urlFisica, $tipoComprobante){
        $faultcode = '0';
        $endpoint  = $wsdlURL;
        $uri       = 'http://service.sunat.gob.pe';
        $options=array(
            'trace'      => true,
            'location'   => $endpoint,
            'uri'        => $uri,
            'exceptions' => true,
            'cache_wsdl' => true,
        );
    
        try {
            $client = new feedSoap(null, $options);
            $client->SoapClientCall($XMLString);
            $client->__call($callFunction, array(), array());
        }catch (SoapFault $f) {
            if($f->faultcode=='HTTP'){
                $faultcode = '-1';
                $faultdesc = $f->faultstring;
            }else{
                $faultcode = $f->faultcode;
                $faultdesc = $f->faultstring;
            }
        }
        
        $ticket = '0';
        if($faultcode=='0'){
            $result = $client -> __getLastResponse();
            $doc = new DOMDocument();
            $doc->loadXML($result);
            $data = $doc->getElementsByTagName('ticket');
            if($data->length>0){
                $ticket = $data->item(0)->nodeValue;
                $resultSunat  = array('ticket'=> $ticket);
                return $resultSunat;
            }else{
                $data = $doc->getElementsByTagName('applicationResponse');
                if($data->length>0){
                    $databin = $data->item(0)->nodeValue;
                }else{
                    $data = $doc->getElementsByTagName('content');
                    if($data->length>0){
                        $databin = $data->item(0)->nodeValue;
                    }
                }
                //var_dump($faultcode,$faultdesc,$data,$databin,$this->isNotData($databin)); exit;
                if($this->isNotData($databin)){
                    $response = base64_decode($databin);
                    $archivo = $urlFisica.'/R-'.$filename.'.zip';
                    file_put_contents($archivo, $response);
                    $resultSunat = $this->readResult($filename, $compElectroId, $urlFisica, $tipoComprobante);
                    return $resultSunat;
                }else{
                    $result = [
                        'codRetorno'        => '0',
                        'descripcionRetorno'=> 'El documento fue dado de baja',
                        'observaciones'     => [],
                        'rutaCDR'           => '',
                        'rutaPDF'           => '',
                    ];
                    return $result;
                }
            }
            
        }else{
            $codigoRetorno = explode('Client.', $faultcode);
            $codido = $codigoRetorno[1];
            if($codido > 0 && $codido < 2000){
                $codRetorno = 1;
            }elseif($codido > 1999 && $codido < 4000){
                $codRetorno = 2;
            }elseif($codido > 3999){
                $codRetorno = 3;
            }
            $result = [
                'codRetorno'        => $codRetorno,
                'descripcionRetorno'=> $faultdesc,
                'observaciones'     => [],
                'rutaCDR'           => '',
                'rutaPDF'           => '',
            ];
            return $result;
        }
    }
    
    function isNotData($test_string){
        return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $test_string);
    }
    
    function readResult($filename, $compElectroId, $urlFisica, $tipoComprobante){
        $newFile = $urlFisica.'/R-'.$filename.'.zip';
        $filter = new Decompress(array(
            'adapter' => 'Zip',
            'options' => array(
                'target' => $urlFisica,
            )
        ));
        $filter->filter($newFile);
        $archive = $urlFisica.'/R-'.$filename.'.xml';
        $xmlSunat = $urlFisica.'/'.$filename.'.xml';
        if (file_exists($archive)) {
            $xml = file_get_contents($archive);
    
            $xml_parser = xml_parser_create();
            xml_parse_into_struct($xml_parser, $xml, $vals);
            xml_parser_free($xml_parser);
    
            $observaciones = array();
            foreach ($vals as $xml_elem) {
                $x_tag=$xml_elem['tag'];
                if($x_tag == 'CBC:RESPONSECODE'){
                    $codido = $xml_elem['value'];
                    if($codido == 0){
                        $codRetorno = 0;
                    }elseif($codido > 0 && $codido < 2000){
                        $codRetorno = 1;
                    }elseif($codido > 1999 && $codido < 4000){
                        $codRetorno = 2;
                    }elseif($codido > 3999){
                        $codRetorno = 3;
                    }
                }
                if ($x_tag == 'CBC:DESCRIPTION') {
                    $descripcionRetorno = $xml_elem['value'];
                }
                if($x_tag == 'CBC:NOTE'){
                    $observaciones[] = $xml_elem['value'];
                }
            }
            $nonSFE = 'sfe';
            $explodeUrl = explode($nonSFE, $archive);
            $explodeUrlXmlSunat = explode($nonSFE, $xmlSunat);
            $server = $_SERVER['SERVER_NAME'];
            $urlServer = 'http://'.$server.'/'.$nonSFE.$explodeUrl[1];
            $urlXmlSunat = 'http://'.$server.'/'.$nonSFE.$explodeUrlXmlSunat[1];
            if($tipoComprobante == '01'){
                $nomDocumento = 'facturas';
            }elseif($tipoComprobante == '03'){
                $nomDocumento = 'boletas';
            }elseif($tipoComprobante == '07' || $tipoComprobante == '08'){
                $nomDocumento = 'notas';
            }else{
                $nomDocumento = 'bajas';
            }
            $urlPdf = 'http://'.$server.'/'.$nonSFE.'/public/reportes/'.$nomDocumento.'/docpdf/'.$compElectroId;
            $result = [
                'codRetorno'        => $codRetorno,
                'descripcionRetorno'=> $descripcionRetorno,
                'observaciones'     => $observaciones,
                'rutaCDR'           => $urlServer,
                'ruraXmlSunat'      => $urlXmlSunat,
                'rutaPDF'           => $urlPdf,
            ];
            return $result;
        } else {
            return 'El servicio de Sunat no responde';
        }
    }

    function numerotexto($numero) {
        $extras= array("/[\$]/","/ /","/,/","/-/");
        $limpio=preg_replace($extras,"",$numero);
        $partes=explode(".",$limpio);
        if (count($partes)>2) {
            return "Error, el n&uacute;mero no es correcto";
            exit();
        }
        $digitos_piezas=chunk_split($partes[0],1,"#");
        $digitos_piezas=substr($digitos_piezas,0,strlen($digitos_piezas)-1);
        $digitos=explode("#",$digitos_piezas);
        $todos=count($digitos);
        $grupos=ceil (count($digitos)/3);
        $unidad = array    ('un','dos','tres','cuatro','cinco','seis','siete','ocho','nueve');
        $decenas = array ('diez','once','doce', 'trece','catorce','quince');
        $decena = array    ('dieci','veinti','treinta','cuarenta','cincuenta','sesenta','setenta','ochenta','noventa');
        $centena = array    ('ciento','doscientos','trescientos','cuatrocientos','quinientos','seiscientos','setecientos','ochocientos','novecientos');
        $resto=$todos;
        if(($partes[0] * 1) != 0){
            for ($i=1; $i<=$grupos; $i++) {
                if ($resto>=3) {
                    $corte=3; } else {
                        $corte=$resto;
                    }
                    $offset=(($i*3)-3)+$corte;
                    $offset=$offset*(-1);
                    $num=implode("",array_slice ($digitos,$offset,$corte));
                    $resultado[$i] = "";
                    $cen = (int) ($num / 100);              //Cifra de las centenas
                    $doble = $num - ($cen*100);             //Cifras de las decenas y unidades
                    $dec = (int)($num / 10) - ($cen*10);    //Cifra de laa decenas
                    $uni = $num - ($dec*10) - ($cen*100);   //Cifra de las unidades
                    if ($cen > 0) {
                        if ($num == 100) $resultado[$i] = "cien";
                        else $resultado[$i] = $centena[$cen-1].' ';
                    }//end if
                    if ($doble>0) {
                        if ($doble == 20) {
                            $resultado[$i] .= " veinte";
                        }elseif (($doble < 16) and ($doble>9)) {
                            $resultado[$i] .= $decenas[$doble-10];
                        }else {
                            if($dec==0)
                            {}
                            else
                            {
                                $resultado[$i] .=' '. $decena[$dec-1];
                            }
                        }//end if
                        if ($dec>2 and $uni<>0) $resultado[$i] .=' y ';
                        if (($uni>0) and ($doble>15) or ($dec==0)) {
                            if ($i==1 && $uni == 1) $resultado[$i].="uno";
                            else $resultado[$i].=$unidad[$uni-1];
                        }
                    }
                    switch ($i) {
                        case 2:
                            $resultado[$i].= ($resultado[$i]=="") ? "" : " mil ";
                            break;
                        case 3:
                            $resultado[$i].= ($num==1) ? " mill&oacute;n " : " millones ";
                            break;
                    }
                    $resto-=$corte;
            }
        }else{
            $resultado[1] = 'cero';
        }
        $resultado_inv= array_reverse($resultado, TRUE);
        $final="";
        foreach ($resultado_inv as $parte){
            $final.=$parte;
        }
        $posicion_punto= strrpos($numero,".");
        if($posicion_punto){

            $dec_2=substr($numero,$posicion_punto + 1 ,2);
        }else {
            $dec_2='00';
        }
        //$posicion_punto=strpos('.',$dec_2);
        if(!$final){
            $final='CERO ';
        }else{
            if(strlen($dec_2)==1){$dec_2=$dec_2."0";};
            $final = $final." con ".$dec_2."/100  ";
        }
        $final = strtoupper($final);
        return $final;
    }
    
    public function createFolder($rucEmpresa,$tipoDocumento,$serieCorrelativo){
        $folderDoc =  $this->tipoDocTable->getDataTipoDocumento($tipoDocumento);
        /*** Proceso de creacion de carpetas ***/
        $carpeta = getcwd().'/tmp/'.$rucEmpresa.'/'.$folderDoc.'/'.$serieCorrelativo;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
        return $carpeta;        
    }
}
<?php
namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Result;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Auth\Model\Auth;
use Auth\Form\LoginForm;
use Auth\Form\recuperarpassForm;

class LoginController extends AbstractActionController {
    public function authenticationAction() {
        $frmlogin = new LoginForm('login-form');
        $frmlogin->get('submit')->setValue('Login');
        $formpass = new recuperarpassForm('pass-form');
        $messages = null;
        $request = $this->getRequest();
        if ($request->isPost()) {
            $authFormFilters = new Auth();
            $frmlogin->setInputFilter($authFormFilters->getInputFilter());
            $frmlogin->setData($request->getPost());
            if ($frmlogin->isValid()) {
                $data = $frmlogin->getData();
                $sm = $this->getServiceLocator();
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $authAdapter = new AuthAdapter($dbAdapter, 
                    'usuario', // there is a method setTableName to do the same
                    'username', // there is a method setIdentityColumn to do the same
                    'password' // there is a method setCredentialColumn to do the same
                );
                $authAdapter->setIdentity($data['username'])
                            ->setCredential(sha1($data['password']));
                $auth = new AuthenticationService();
                $result = $auth->authenticate($authAdapter);
                switch ($result->getCode()) {
                    case Result::FAILURE_IDENTITY_NOT_FOUND:
                        break;
                    case Result::FAILURE_CREDENTIAL_INVALID:
                        break;
                    case Result::SUCCESS:
                        $storage = $auth->getStorage();
                        $storage->write($authAdapter->getResultRowObject(null, 'password'));
                        $time = 1209600; // 14 days 1209600/3600 = 336 hours => 336/24 = 14 days
                        if ($data['rememberme']) {
                            $sessionManager = new \Zend\Session\SessionManager();
                            $sessionManager->rememberMe($time);
                        }
                        return $this->redirect()->toUrl($this->getRequest()->getBaseUrl().'/inicio');
                    default:
                        break;
                }
                foreach ($result->getMessages() as $message) {
                    $this->flashMessenger()->addSuccessMessage('Tu usuario o contraseña son incorrectos');
                    return $this->redirect()->toUrl($this->getRequest()->getBaseUrl().'/authentication');
                }
            }
            $this->flashMessenger()->addSuccessMessage('Tu usuario o contraseña son incorrectos');
            return $this->redirect()->toUrl($this->getRequest()->getBaseUrl().'/authentication');
        }
        $valores = array(
            'form' => $frmlogin, 'formpass' => $formpass
        );
        $view = new ViewModel($valores);
        $this->layout('layout/authentication.phtml');
        return $view;
    }
    public function logoutAction()
    {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
        }
        $auth->clearIdentity();
        $sessionManager = new \Zend\Session\SessionManager();
        $sessionManager->forgetMe();
        return $this->redirect()->toUrl($this->getRequest()->getBaseUrl().'/authentication');
    }
}

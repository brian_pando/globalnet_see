<?php
namespace Auth\Form;

use Zend\Form\Form;

class AuthForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('auth');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Username',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'email',
                'placeholder'   => 'Ingresa correo',
                'class'         => 'input_usu',
                'required'      => 'required',
                'id'            => 'email',
            )
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'          => 'password',
                'required'      => 'required',
                'class'         => 'input_usu',
                'placeholder'   => 'Ingresa password',
            )
        ));
        $this->add(array(
            'name' => 'rememberme',
            'type' => 'checkbox', // 'Zend\Form\Element\Checkbox',			
            'attributes' => array( 
                'class'         => 'lbl_remenber',
            ),
            'options' => array(
                'label' => 'Recordarme?',
//		'checked_value' => 'true', without value here will be 1
//		'unchecked_value' => 'false', // witll be 1
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'class' => 'btn_enviar',
                'id' => 'submitbutton',
            ),
        )); 
    }
}
<?php
namespace Auth\Form;

use Zend\Form\Form;

class ForgottenPasswordForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('registration');
        $this->setAttribute('method', 'post');
		
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'          => 'email',
                'placeholder'   => 'Ingrese su correo',
                'class'         => 'form-control placeholder-no-fix',
                'required'      => 'required',
                'id'            => 'email',
                'autocomplete'  => 'off',
            ),
        ));	
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Go',
                'id' => 'submitpass',
            ),
        )); 
    }
}
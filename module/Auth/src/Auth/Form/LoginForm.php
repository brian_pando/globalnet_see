<?php
namespace Auth\Form;

use Zend\Form\Form;

class LoginForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('auth');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type'          => 'text',
                'placeholder'   => 'Ingrese su usuario',
                'class'         => 'form-control placeholder-no-fix',
                'required'      => 'required',
                'id'            => 'user',
                'autocomplete'  => 'off',
            )
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'          => 'password',
                'required'      => 'required',
                'class'         => 'form-control placeholder-no-fix',
                'placeholder'   => 'Ingrese su contraseña',
                'autocomplete'  => 'off',
            )
        ));
        $this->add(array(
            'name' => 'rememberme',
            'type' => 'checkbox', // 'Zend\Form\Element\Checkbox',
            'attributes' => array(
                'class'         => 'lbl_remenber',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Ingresar',
                'class' => 'btn btn-warning pull-right',
                'id' => 'submitbutton',
            ),
        ));
    }
}
<?php
namespace Auth\Form;

use Zend\Form\Form;

class recuperarpassForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('pass-form');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'          => 'email',
                'placeholder'   => 'Ingrese su correo',
                'class'         => 'form-control placeholder-no-fix',
                'required'      => 'required',
                'id'            => 'email',
                'autocomplete'  => 'off',
            )
        ));
        $this->add(array(
            'name' => 'recuperar',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Recuperar contraseña',
                'class' => 'btn btn-warning pull-right',
                'id' => 'submitpass',
            ),
        ));
    }
}
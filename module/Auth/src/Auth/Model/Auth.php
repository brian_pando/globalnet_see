<?php
namespace Auth\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
// the object will be hydrated by Zend\Db\TableGateway\TableGateway
class Auth implements InputFilterAwareInterface
{
    public $usuarioId;
    public $username;
    public $password;
    public $nombre;
    public $email;
    public $estado;
    public $foto;
    public $rolId;
    public $dni;
    public $fecha_modificacion;
    public $fecha_creacion;

    // Hydration
    // ArrayObject, or at least implement exchangeArray. For Zend\Db\ResultSet\ResultSet to work
    public function exchangeArray($data)
    {
        $this->usuarioId          = (!empty($data['usuarioId'])) ? $data['usuarioId'] : null;
        $this->username           = (!empty($data['username'])) ? $data['username'] : null;
        $this->password           = (!empty($data['password'])) ? $data['password'] : null;
        $this->nombre             = (!empty($data['nombre'])) ? $data['nombre'] : null;
        $this->email              = (!empty($data['email'])) ? $data['email'] : null;
        $this->estado             = (!empty($data['estado'])) ? $data['estado'] : null;
        $this->fecha_creacion     = (!empty($data['fecha_creacion'])) ? $data['fecha_creacion'] : null;
        $this->fecha_modificacion = (!empty($data['fecha_modificacion'])) ? $data['fecha_modificacion'] : null;
        $this->rolId              = (!empty($data['rolId'])) ? $data['rolId'] : null;
        $this->dni                = (!empty($data['dni'])) ? $data['dni'] : null;
    }

	// Extraction. The Registration from the tutorial works even without it.
	// The standard Hydrator of the Form expects getArrayCopy to be able to bind
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    protected $inputFilter;

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name'     => 'username',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 4,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'password',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 6,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
}
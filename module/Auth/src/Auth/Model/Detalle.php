<?php
namespace Auth\Model;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class Detalle extends TableGateway {

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null)
    {
        return parent::__construct('t_detalle', $adapter, $databaseSchema, $selectResultPrototype);
    }

    public function getTipousuario(){
        $data = $this->select(function (Select $select){
            $select->columns(array('detalleId','descripcion'));
            $select->where(array('masterId' => 1));
        });
        $result = $data->toArray();
        $selectData = array();
        foreach ($result as $row){
            $selectData[$row['detalleId']] = $row['descripcion'];
        }
        return $selectData;
    }

    public function getTipoBaseDatos(){
        $data = $this->select(function (Select $select){
            $select->columns(array('detalleId','descripcion'));
            $select->where(array('masterId' => 2));
        });
        $result = $data->toArray();
        $selectData = array();
        foreach ($result as $row){
            $selectData[$row['detalleId']] = $row['descripcion'];
        }
        return $selectData;
    }
}
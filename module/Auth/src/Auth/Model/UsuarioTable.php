<?php
namespace Auth\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class UsuarioTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getUser($usr_id) {
        $usr_id = (int) $usr_id;
        $rowset = $this->tableGateway->select(array('usuarioId' => $usr_id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getUserByToken($token) {
        $rowset = $this->tableGateway->select(array('usr_registration_token' => $token));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $token");
        }
        return $row;
    }

    public function activateUser($usr_id)
    {
        $data['usr_active'] = 1;
        $data['usr_email_confirmed'] = 1;
        $this->tableGateway->update($data, array('usuarioId' => (int)$usr_id));
    }

    public function getUserByEmail($usr_email)
    {
        $rowset = $this->tableGateway->select(array('email' => $usr_email));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $usr_email");
        }
        return $row;
    }

    public function changePassword($usr_id, $password)
    {
        $data['password'] = $password;
        $this->tableGateway->update($data, array('usuarioId' => (int)$usr_id));
    }

    public function saveUser(Auth $auth)
    {
        $data = array(
            'username'          => $auth->username,
            'password'          => sha1($auth->password),
            'nombre'            => $auth->nombre,
            'email'             => $auth->email,
            'dni'               => $auth->dni,
            'rolId'             => $auth->rolId,
            'estado'            => $auth->estado,
        );
        $usr_id = (int)$auth->usuarioId;
        if ($usr_id == 0) {
            $rolId = (int)$auth->rolId;
            $this->tableGateway->insert($data);
        }else {
            if ($this->getUser($usr_id)) {
                $this->tableGateway->update($data, array('usuarioId' => $usr_id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteUser($id)
    {
        $this->tableGateway->delete(array('usr_id' => $usr_id));
    }
}
<?php

namespace Auth\Model;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;

class Usuario extends TableGateway{
    private $usuarioId;
    private $nombre;
    private $dni;
    private $username;
    private $password;
    private $email;
    private $rolId;
    private $estado;

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null)
    {
        return parent::__construct('usuario', $adapter, $databaseSchema, $selectResultPrototype);
    }

    public function saveUser(Auth $auth){
        $data = array(
            'username'           => $auth->username,
            'password'           => sha1($auth->password),
            'nombre'             => $auth->nombre,
            'email'              => $auth->email,
            'dni'                => $auth->dni,
            'rolId'              => $auth->rolId,
            'estado'             => $auth->estado,
            'fecha_creacion'     => $auth->fecha_creacion,
            'fecha_modificacion' => $auth->fecha_modificacion,
        );
        $usr_id = (int)$auth->usuarioId;
        if ($usr_id == 0) {
            $rolId = (int)$auth->rolId;
            $this->insert($data);
        }else {
            $this->update($data, array('usuarioId' => $usr_id));
        }
    }

    public function getUsuarios(){
        $data = $this->select(function(Select $select){
            $select->columns(array('usuarioId','username','nombre','email','dni','rolId','fecha_creacion'));
            $select->join('t_detalle', 't_detalle.detalleId = usuario.rolId', array('rol' => 'descripcion'), 'left');
            $select->where(array('usuario.estado' => 1));
        });
        return $data->toArray();
    }

    public function getUsuariosById($usuarioId){
        $data = $this->select(function(Select $select) use($usuarioId){
            $select->columns(array('usuarioId','username','nombre','email','dni','rolId','fecha_creacion'));
            $select->join('t_detalle', 't_detalle.detalleId = usuario.rolId', array('rol' => 'descripcion'), 'left');
            $select->where(array('usuarioId' => $usuarioId));
        });
        return $data->current();
    }

    public function existeEmail($email, $rolId){
        $data = $this->select(function(Select $select) use($email, $rolId){
            $select->columns(array('email'));
            $select->where(array('email' => $email, 'rolId' => $rolId));
        });
        return $data->count();
    }

    public function changePass($email, $password){
        $data=array('password' => sha1($password));
        $this->update($data, array('email' => $email));
    }

    public function loginUser($email, $pass){
        $data = $this->select(function(Select $select) use($email, $pass){
            $select->columns(array('email'));
            $select->where(array('email' => $email, 'password' => $pass));
        });
        return $data->count();
    }

    public function loginUserAdmin($email, $pass){
        $data = $this->select(function(Select $select) use($email, $pass){
            $select->columns(array('rolId'));
            $select->where(array('email' => $email, 'password' => $pass));
        });
        return $data->current();
    }

    public function deleteUser($usuarioId){
        $data=array('estado' => 0);
        $this->update($data, array('usuarioId' => $usuarioId));
    }
}
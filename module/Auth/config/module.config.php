<?php
return array(
    'san_captcha' => array(
        'class' => 'image',
        'options' => array(
            'imgDir' => sys_get_temp_dir(),
            'fontDir' => __DIR__.'/../fonts',
            'width' => 200,
            'height' => 50,
            'dotNoiseLevel' => 40,
            'lineNoiseLevel' => 3
        ),
    ),

//  'static_salt' => 'aFGQ475SDsdfsaf2342', // I am going to move it to global.php. It should be accessable everywhere
    'controllers' => array(
        'invokables' => array(
            'Auth\Controller\Admin'         => 'Auth\Controller\AdminController',
            'Auth\Controller\Captcha'       => 'Auth\Controller\CaptchaController',
            'Auth\Controller\Login'         => 'Auth\Controller\LoginController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'auth' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/usuarios',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Auth\Controller',
                        'controller' => 'Admin',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/][:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                    'captcha_form_generate' => array(
                        'type'    => 'segment',
                        'options' => array(
                            'route'    =>  '/captcha/[:id]',
                            'defaults' => array(
                                '__NAMESPACE__' => 'Auth\Controller',
                                'controller' => 'Captcha',
                                'action' => 'generate',
                            ),
                        ),
                    ),
                ),
            ),
            'authentication' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/authentication',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Auth\Controller',
                        'controller'    => 'Login',
                        'action'        => 'authentication',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'auth' => __DIR__ . '/../view'
        ),
        'display_exceptions' => true,
        //para usar Json
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
    ),
);
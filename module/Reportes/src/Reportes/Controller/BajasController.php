<?php
namespace Reportes\Controller;
use Reportes\Controller\ReportesController;
use Zend\View\Model\ViewModel;
use Reportes\TableConfig;

class BajasController extends ReportesController
{
    public $dbAdapter;
    protected $docBajaTable;
    
    public function indexAction(){
        //$this->identidad();
        $view = new ViewModel();
        $view->setVariables(array('active' => 'baja'));
        $this->layout('layout/sfe.phtml');
        return $view;
    }
    
    public function showBajaAction(){
        $table = new TableConfig\Baja();
        $table->setAdapter($this->getDbAdapter())
              ->setSource($this->getDocBaja())
              ->setParamAdapter($this->getRequest()->getPost());
        return $this->htmlResponse($table->render());
    }

    public function getDocBaja(){
        return $this->DocBajaTable()->fetchAllSelect();
    }
}

<?php
namespace Reportes\Controller;

use Reportes\Controller\ReportesController;
use Zend\View\Model\ViewModel;
use Reportes\TableConfig;
use DOMPDFModule\View\Model\PdfModel;

class FacturasController extends ReportesController
{
    public function indexAction()
    {
        //$this->identidad();
        $view = new ViewModel();
        $view->setVariables(array('active' => 'factura'));
        $this->layout('layout/sfe.phtml');
        return $view;
    }
    
    public function showComprobantesAction(){
        $tipoDoc = $this->request->getPost('tipoDoc');
        $table = new TableConfig\Comprobantes('factura');
        $table->setAdapter($this->getDbAdapter())
              ->setSource($this->getComprobantes($tipoDoc))
              ->setParamAdapter($this->getRequest()->getPost());
        return $this->htmlResponse($table->render());
    }
    
    public function getComprobantes($tipoDoc){
        return $this->CompElectTable()->fetchAllSelect($tipoDoc);
    }
    
    public function docpdfAction(){  
        $id = $this->params()->fromRoute('id');
        $dataDoc = $this->CompElectTable()->getDataDoc($id);
        $ubigeo = $dataDoc['ubigeo'];
        $ubicacion = $this->getUbigeo($ubigeo);
        $pdf = new PdfModel();
        $pdf->setOption('filename', $dataDoc['nomRelativo'].'-'.$dataDoc['seriecorre']); // Triggers PDF download, automatically appends ".pdf"
        $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"
        $dataCabecera = $this->CompElectTable()->fetchOneSelect($id);
        $dataDetalle = $this->CompElectDetalleTable()->getDataDetalle($id);
        $dataEmpresa = $this->EmpresaTable()->getDataEmpresa();
        if($dataCabecera['tipoMoneda'] == 'PEN'){
            $nomMoneda = 'Soles';
        }else{
            $nomMoneda = 'Dólares estadounidenses';
        }
        $importeTotalLetra  = $this->numerotexto($dataCabecera['importeTotal']);
        $pdf->setVariables(array(
            'cabecera' => $dataCabecera,
            'detalle'  => $dataDetalle,
            'empresa'  => $dataEmpresa,
            'totalLetras'   => $importeTotalLetra.' '.$nomMoneda,
            'ubicacion'     => $ubicacion
        ));
        return $pdf;
    }
}
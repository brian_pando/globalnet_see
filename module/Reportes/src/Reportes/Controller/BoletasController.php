<?php
namespace Reportes\Controller;

use Reportes\Controller\ReportesController;
use Zend\View\Model\ViewModel;
use Reportes\TableConfig;
use DOMPDFModule\View\Model\PdfModel;

class BoletasController extends ReportesController
{
    public function indexAction()
    {
        //$this->identidad();
        $view = new ViewModel();
        $view->setVariables(array('active' => 'boleta'));
        $this->layout('layout/sfe.phtml');
        return $view;
    }
    
    public function showComprobantesAction(){
        $tipoDoc = $this->request->getPost('tipoDoc');
        $table = new TableConfig\Comprobantes('boleta');
        $table->setAdapter($this->getDbAdapter())
              ->setSource($this->getComprobantes($tipoDoc))
              ->setParamAdapter($this->getRequest()->getPost());
        return $this->htmlResponse($table->render());
    }
    
    public function getComprobantes($tipoDoc){
        return $this->CompElectTable()->fetchAllSelect($tipoDoc);
    }
    
    public function docpdfAction(){
        $id = $this->params()->fromRoute('id');
        $dataDoc = $this->CompElectTable()->getDataDoc($id);
        $ubigeo = $dataDoc['ubigeo'];
        $ubicacion = $this->getUbigeo($ubigeo);
        $pdf = new PdfModel();
        $pdf->setOption('filename', $dataDoc['nomRelativo'].'-'.$dataDoc['seriecorre']); // Triggers PDF download, automatically appends ".pdf"
        $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"
        $dataCabecera = $this->CompElectTable()->fetchOneSelect($id);
        $dataDetalle = $this->CompElectDetalleTable()->getDataDetalle($id);
        $dataEmpresa = $this->EmpresaTable()->getDataEmpresa();
        if($dataCabecera['tipoMoneda'] == 'PEN'){
            $nomMoneda = 'Soles';
        }else{
            $nomMoneda = 'Dólares estadounidenses';
        }
        $importeTotalLetra  = $this->numerotexto($dataCabecera['importeTotal']);
        $pdf->setVariables(array(
            'cabecera' => $dataCabecera,
            'detalle'  => $dataDetalle,
            'empresa'  => $dataEmpresa,
            'totalLetras'   => $importeTotalLetra.' '.$nomMoneda,
            'ubicacion'     => $ubicacion
        ));
        return $pdf;
    }

    public function pdfProvisionalAction(){                         
        $data = json_decode(file_get_contents('php://input'))->data;    // Se gurdan en una variable los datos de entrada      
        $zipfilename = '/opt/lampp/htdocs/pdfs-comprimidos-financiamiento/financiamientos.zip';
        $zip = new \ZipArchive();  
        $opened = $zip->open($zipfilename,\ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        if ( $opened !== true) {
            die("cannot open {$zipFileName} for writing.");
        }

        foreach ($data as $value) {
            // -----  Datos para el contenido del PDF   ------
            $dataDoc = $this->CompElectTable()->getDataDoc($value->id);            
            $ubigeo = $dataDoc['ubigeo'];  
            $ubicacion = $this->getUbigeo($ubigeo); 

            $dataCabecera = $this->CompElectTable()->fetchOneSelect($value->id);
            $dataDetalle = $this->CompElectDetalleTable()->getDataDetalle($value->id);
            $dataEmpresa = $this->EmpresaTable()->getDataEmpresa();

            if($dataCabecera['tipoMoneda'] == 'PEN'){
                $nomMoneda = 'Soles';
            }else{
                $nomMoneda = 'Dólares estadounidenses';
            }
            
            $importeTotalLetra  = $this->numerotexto($dataCabecera['importeTotal']);  


            // -----------  Creando nombre de los pdfs  ------------------
            $descripTelf=$dataDetalle[0]['descripcion'];            
            $pdfname = 'comprobante';            
            $año = date("Y",strtotime($dataCabecera['fechaEmision']));            
            $mes = date("m",strtotime($dataCabecera['fechaEmision']));            
            $dia = date("d",strtotime($dataCabecera['fechaEmision']));                                 
            $fecha = $dia.$mes.$año;        
            $telefono = substr($descripTelf,strpos($descripTelf,'No:')+3);  // Cortamos todo el contenido de descripción y nos quedamos con la parte de telefono
            $serie = str_replace('-','_',$dataDoc['seriecorre']);                      
            if ( (substr($dataDoc['seriecorre'], 0,4) == 'BA04') || (substr($dataDoc['seriecorre'], 0,4) == 'FA04')) {
                
                $pdfname = 'TF_GLOBALNETCOMUNICACIONESSAC_'.$fecha.'_'.$telefono.'_'.$serie;

            }else{
                $pdfname = 'AG_GLOBALNETCOMUNICACIONESSAC_'.$fecha.'_'.$telefono.'_'.$serie;
            }

            
            // -------------------  Creando el PDF  ------------------------
            $pdf = new PdfModel();
            $pdf->setOption('filename', $pdfname); // Triggers PDF download, automatically appends ".pdf"
            $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
            $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"
            
            

            // -----------   Preparando el render del PDF   ---------------------            
            $tipoComprobante = (substr($dataDoc['seriecorre'], 0,1)) == 'B' ? 'boletas' : 'facturas';
            $template = 'reportes/'.$tipoComprobante.'/docpdf.phtml';            
            $pdfView = new ViewModel( $pdf );
            $pdfView->setTerminal(true)
                ->setTemplate($template)
                ->setVariables(array(                    
                    'cabecera'=> $dataCabecera,
                    'detalle'=> $dataDetalle,
                    'empresa'=> $dataEmpresa,
                    'totalLetras'=> $importeTotalLetra.' '.$nomMoneda,
                    'ubicacion'=> $ubicacion
                ));

            $html = $this->getServiceLocator()->get('viewpdfrenderer')->getHtmlRenderer()->render($pdfView);               
            $eng = new \Dompdf();                             
                          
            $eng->load_html($html);          
            $eng->render();   

            $pdfCode = $eng->output();            
            file_put_contents('/opt/lampp/htdocs/pdfs-financiamiento/tmp-'.$pdfname.'.pdf', $pdfCode);    

            // ---------------------  Comprimiendo PDFS  --------------------
            $zip->addFile('/opt/lampp/htdocs/pdfs-financiamiento/tmp-'.$pdfname.'.pdf', $pdfname.'.pdf');                                  

        }

        $zip->close();

        $response =  array( 
            // Ruta fija, corregir si es necesari (Hay una tarea programada que elimina los comprimidos diariamente)               
            'ruta' => 'http:\\\\158.69.5.252\\pdfs-comprimidos-financiamiento\\financiamientos.zip',
        );        
        echo json_encode($response);
    
    } 
}

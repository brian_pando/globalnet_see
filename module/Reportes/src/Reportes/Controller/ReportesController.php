<?php

namespace Reportes\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class ReportesController extends AbstractActionController
{
    protected $dbAdapter;
    protected $compElectTable;
    protected $docResumenTable;
    protected $docBajaTable;
    protected $compElectDetalleTable;
    protected $empresaTable;
    protected $ubigeoTable;
    
    public function htmlResponse($html){
        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent($html);
        return $response;
    }
    
    public function getDbAdapter(){
        if (!$this->dbAdapter) {
            $sm = $this->getServiceLocator();
            $this->dbAdapter = $sm->get('zfdb_adapter');
        }
        return $this->dbAdapter;
    }
    
    public function CompElectTable(){
        if (!$this->compElectTable){
            $sm = $this->getServiceLocator();
            $this->compElectTable = $sm->get('Reportes\Model\CompElectTable');
        }
        return $this->compElectTable;
    }
    
    public function CompElectDetalleTable(){
        if (!$this->compElectDetalleTable){
            $sm = $this->getServiceLocator();
            $this->compElectDetalleTable = $sm->get('Reportes\Model\CompElectDetalleTable');
        }
        return $this->compElectDetalleTable;
    }
    
    public function DocResumenTable(){
        if (!$this->docResumenTable){
            $sm = $this->getServiceLocator();
            $this->docResumenTable = $sm->get('Reportes\Model\DocResumenTable');
        }
        return $this->docResumenTable;
    }
    
    public function DocBajaTable(){
        if (!$this->docBajaTable){
            $sm = $this->getServiceLocator();
            $this->docBajaTable = $sm->get('Reportes\Model\DocBajaTable');
        }
        return $this->docBajaTable;
    }
    
    public function EmpresaTable(){
        if (!$this->empresaTable){
            $sm = $this->getServiceLocator();
            $this->empresaTable = $sm->get('Reportes\Model\EmpresaTable');
        }
        return $this->empresaTable;
    }
    
    public function UbigeoTable(){
        if (!$this->ubigeoTable){
            $sm = $this->getServiceLocator();
            $this->ubigeoTable = $sm->get('Reportes\Model\UbigeoTable');
        }
        return $this->ubigeoTable;
    }
    
    public function identidad(){
        $user = $this->identity();
        $rol = $user->rolId;
        if(!$user or $rol != 1){
            return $this->redirect()->toUrl($this->getRequest()->getBaseUrl().'/authentication');
        }
    }
    
    public function getUbigeo($ubigeo){
        if($ubigeo){
            $dataUbi = str_split($ubigeo, 2);
            $departamento = $dataUbi[0];
            $provincia = $dataUbi[1];
            $distrito = $dataUbi[2];
            $nomDep = $this->UbigeoTable()->getDataDepart($departamento);
            $nomPro = $this->UbigeoTable()->getDataProvin($departamento,$provincia);
            $nomDis = $this->UbigeoTable()->getDataDistri($departamento,$provincia,$distrito);
            $ubicacion = $nomDep.' '.$nomPro.' '.$nomDis;
        }else{
            $ubicacion = '';
        }
        return $ubicacion;
    }
    
    public function numerotexto($numero) {
        $extras= array("/[\$]/","/ /","/,/","/-/");
        $limpio=preg_replace($extras,"",$numero);
        $partes=explode(".",$limpio);
        if (count($partes)>2) {
            return "Error, el n&uacute;mero no es correcto";
            exit();
        }
        $digitos_piezas=chunk_split($partes[0],1,"#");
        $digitos_piezas=substr($digitos_piezas,0,strlen($digitos_piezas)-1);
        $digitos=explode("#",$digitos_piezas);
        $todos=count($digitos);
        $grupos=ceil (count($digitos)/3);
        $unidad = array    ('un','dos','tres','cuatro','cinco','seis','siete','ocho','nueve');
        $decenas = array ('diez','once','doce', 'trece','catorce','quince');
        $decena = array    ('dieci','veinti','treinta','cuarenta','cincuenta','sesenta','setenta','ochenta','noventa');
        $centena = array    ('ciento','doscientos','trescientos','cuatrocientos','quinientos','seiscientos','setecientos','ochocientos','novecientos');
        $resto=$todos;
        if(($partes[0] * 1) != 0){
            for ($i=1; $i<=$grupos; $i++) {
                if ($resto>=3) {
                    $corte=3; } else {
                        $corte=$resto;
                    }
                    $offset=(($i*3)-3)+$corte;
                    $offset=$offset*(-1);
                    $num=implode("",array_slice ($digitos,$offset,$corte));
                    $resultado[$i] = "";
                    $cen = (int) ($num / 100);              //Cifra de las centenas
                    $doble = $num - ($cen*100);             //Cifras de las decenas y unidades
                    $dec = (int)($num / 10) - ($cen*10);    //Cifra de laa decenas
                    $uni = $num - ($dec*10) - ($cen*100);   //Cifra de las unidades
                    if ($cen > 0) {
                        if ($num == 100) $resultado[$i] = "cien";
                        else $resultado[$i] = $centena[$cen-1].' ';
                    }//end if
                    if ($doble>0) {
                        if ($doble == 20) {
                            $resultado[$i] .= " veinte";
                        }elseif (($doble < 16) and ($doble>9)) {
                            $resultado[$i] .= $decenas[$doble-10];
                        }else {
                            if($dec==0)
                            {}
                            else
                            {
                                $resultado[$i] .=' '. $decena[$dec-1];
                            }
                        }//end if
                        if ($dec>2 and $uni<>0) $resultado[$i] .=' y ';
                        if (($uni>0) and ($doble>15) or ($dec==0)) {
                            if ($i==1 && $uni == 1) $resultado[$i].="uno";
                            else $resultado[$i].=$unidad[$uni-1];
                        }
                    }
                    switch ($i) {
                        case 2:
                            $resultado[$i].= ($resultado[$i]=="") ? "" : " mil ";
                            break;
                        case 3:
                            $resultado[$i].= ($num==1) ? " mill&oacute;n " : " millones ";
                            break;
                    }
                    $resto-=$corte;
            }
        }else{
            $resultado[1] = 'cero';
        }
        $resultado_inv= array_reverse($resultado, TRUE);
        $final="";
        foreach ($resultado_inv as $parte){
            $final.=$parte;
        }
        $posicion_punto= strrpos($numero,".");
        if($posicion_punto){

            $dec_2=substr($numero,$posicion_punto + 1 ,2);
        }else {
            $dec_2='00';
        }
        //$posicion_punto=strpos('.',$dec_2);
        if(!$final){
            $final='CERO ';
        }else{
            if(strlen($dec_2)==1){$dec_2=$dec_2."0";};
            $final = $final." con ".$dec_2."/100  ";
        }
        $final = strtoupper($final);
        return $final;
    }
}

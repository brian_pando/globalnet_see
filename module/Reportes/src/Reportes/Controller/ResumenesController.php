<?php
namespace Reportes\Controller;

use Reportes\Controller\ReportesController;
use Zend\View\Model\ViewModel;
use Reportes\TableConfig;

class ResumenesController extends ReportesController
{   
    public function indexAction(){
        $view = new ViewModel();
        $view->setVariables(array('active' => 'resumen'));
        $this->layout('layout/sfe.phtml');
        return $view;
    }
    
    public function showResumenAction(){
        $table = new TableConfig\Resumen();
        $table->setAdapter($this->getDbAdapter())
              ->setSource($this->getDocResumen())
              ->setParamAdapter($this->getRequest()->getPost());
        return $this->htmlResponse($table->render());
    }

    public function getDocResumen(){
        return $this->DocResumenTable()->fetchAllSelect();
    }
}

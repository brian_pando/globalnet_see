<?php
namespace Reportes\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class DocBajaTable extends AbstractTableGateway
{    
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'documentobaja';

    public function fetchAllSelect() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('baj' => 'documentobaja'))->columns(array('documentoBajaId', 'codigoIdentificador', 'fechaEmision', 'fechaDocumentoBaja', 'estadoSunat'))
               ->order('documentoBajaId DESC');
        return $select;
    }
}
?>
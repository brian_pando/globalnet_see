<?php
namespace Reportes\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class UbigeoTable extends AbstractTableGateway
{
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'ubigeo';
    
    public function getDataDepart($departamento){
        $data = $this->select(function(Select $select) use($departamento){
            $select->columns(array('Nombre'))
                   ->where(array('CodDpto' => $departamento, 'CodProv' => '00', 'CodDist' => '00'));
        });
        $result = $data->current();
        return $result['Nombre'];
    }
    
    public function getDataProvin($departamento,$provincia){
        $data = $this->select(function(Select $select) use($departamento,$provincia){
            $select->columns(array('Nombre'))
                   ->where(array('CodDpto' => $departamento, 'CodProv' => $provincia, 'CodDist' => '00'));
        });
        $result = $data->current();
        return $result['Nombre'];
    }
    
    public function getDataDistri($departamento,$provincia,$distrito){
        $data = $this->select(function(Select $select) use($departamento,$provincia,$distrito){
            $select->columns(array('Nombre'))
                   ->where(array('CodDpto' => $departamento, 'CodProv' => $provincia, 'CodDist' => $distrito));
        });
        $result = $data->current();
        return $result['Nombre'];
    }
}

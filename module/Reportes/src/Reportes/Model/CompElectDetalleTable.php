<?php
namespace Reportes\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

class CompElectDetalleTable extends AbstractTableGateway
{
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'comprobantedetalleelectronico';

    public function getDataDetalle($comprobanteEleId){
        $data = $this->select(function (Select $select) use($comprobanteEleId){
            $select->columns(array('unidadMedida','cantidad','descripcion','valorUnitario','precioUnitario','igvItem','iscItem','valorVenta','numeroOrden','codigo'));
            $select->where(array('comprobanteId' => $comprobanteEleId));
        });
        return $data->toArray();
    }
}

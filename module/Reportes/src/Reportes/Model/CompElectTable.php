<?php
namespace Reportes\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class CompElectTable extends AbstractTableGateway
{
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'comprobanteelectronico';

    public function fetchAllSelect($tipoDoc){
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('ce' => 'comprobanteelectronico'))->columns(array('*'));
        if($tipoDoc != 'notas'){
            $select->where(array('tipoDocumento' => $tipoDoc));
        }else{
            $select->where->in('tipoDocumento', array('07','08'));
        }
        $select->order('id DESC');
        return $select;
    }   
    
    Public function fetchOneSelect($id){
        $data = $this->select(function(Select $select) use($id){
            $select->columns(array('*'))
                   ->join(array('suc' => 'sucursal'), 'suc.sucursalId = comprobanteelectronico.sucursalId', array('direccion','telefono'))
                   ->where(array('id' => $id));
        });
        return $data->current();
    }
    
    public function getDataDoc($id){
        $data = $this->select(function(Select $select) use($id){
            $select->columns(array('seriecorre' => new Expression("CONCAT(Serie,'-',correlativo)")))
                   ->join(array('td' => 'tipodocumentos'), 'td.codigo = comprobanteelectronico.tipoDocumento', array('nomRelativo'))
                   ->join(array('suc' => 'sucursal'), 'suc.sucursalId = comprobanteelectronico.sucursalId', array('ubigeo'))
                   ->where(array('id' => $id));
        });
        return $data->current();
    }
}

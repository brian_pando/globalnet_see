<?php
namespace Reportes\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;

class DocResumenTable extends AbstractTableGateway
{
    private $documentoResumenId, $identificadorResumen, $correlativoResumen, $fechaEmision, $fechaDocumentoResumen, $fechaCreacion ,$estadoSunat;
    
    public function __construct(Adapter $adapter){
        $this->adapter = $adapter;
        $this->initialize();
    }
    protected $table = 'documentoresumen';

    public function fetchAllSelect() {
        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array('res' => 'documentoresumen'))
               ->columns(array('documentoResumenId', 'codigoIdentificador', 'fechaEmision', 'fechaDocumentoResumen', 'estadoSunat'))
               ->order('documentoResumenId DESC');
        return $select;
    }
}
?>
<?php
namespace Reportes\TableConfig;

use ZfTable\AbstractTable;

class Notas extends AbstractTable
{
    protected $document;
    public function __construct($tipo){
        $this->tipo = $tipo;
    }
    
    protected $config = array(
        'showPagination' => true,
        'showQuickSearch' => false,
        'showItemPerPage' => true,
        'itemCountPerPage' => 20,
        'showColumnFilters' => true,
        'valuesOfItemPerPage' => array(10, 20, 25, 50, 100, 200),
    );

    /**
     * @var array Definition of headers
     */
    protected $headers = array(
        //'id' => array('title' => 'Id', 'width' => 50) ,
        'tipoDocumento' => array('title' => 'Tipo Doc.', 'width' => 80, 'filters' => array(null => 'Todos', '07' => 'Nota de Crédito', '08' => 'Nota de Débito')),
        'fechaEmision' => array('title' => 'F. Emisión','width' => 80, 'filters' => 'text'),
        'serie' => array('title' => 'Serie','width' => 60, 'filters' => 'text'),
        'correlativo' => array('title' => 'Correlativo','width' => 80, 'filters' => 'text'),
        'numeroDocCliente' => array('title' => 'Doc. Cliente','filters' => 'text','width' => 90),
        'razonSocialCliente' => array('title' => 'Cliente','width' => 260,'filters' => 'text'),
        'importeTotal' => array('title' => 'Total','width' => 100),
        'tipoMoneda' => array('title' => 'Moneda','width' => 60),
        'estadoSunat' => array('title' => 'Sunat','width' => 80),
        'Actions' =>  array('title' => 'Sunat','width' => 40, 'sortable' => false),
    );

    public function init()
    {
        $this->getHeader('tipoDocumento')->getCell()->addDecorator('mapper', array(
            '07' => 'Nota de Crédito',
            '08' => 'Nota de Débito',
        ));
        
        if($this->tipo == 'nota'){
            $this->getHeader('Actions')->getCell()->addDecorator('template', array(
                'template' => '<a href="docpdf/%s" class="btn-editar" title="descargar PDF"><i class="fa fa-file-pdf-o"></i></a> ',
                'vars' => array('id')
            ));
        }
        
        
        $this->getRow()->addClass('fila-factura');
        $this->addAttr('id', 'tableFactura');
        
        //$this->getHeader('id')->getCell()->addClass('text-center');
        $this->getHeader('tipoDocumento')->getCell()->addClass('text-center');
        $this->getHeader('fechaEmision')->getCell()->addClass('text-center');
        $this->getHeader('serie')->getCell()->addClass('text-center');
        $this->getHeader('correlativo')->getCell()->addClass('text-center');
        $this->getHeader('numeroDocCliente')->getCell()->addClass('text-center');
        $this->getHeader('importeTotal')->getCell()->addClass('success text-right');
        $this->getHeader('tipoMoneda')->getCell()->addClass('text-center');
        $this->getHeader('estadoSunat')->getCell()->addClass('text-center');
        $this->getHeader('Actions')->getCell()->addClass('text-center');
    }

    protected function initFilters($query)
    {
        if ($value = $this->getParamAdapter()->getValueOfFilter('tipoDocumento')) {
            $query->where("tipoDocumento like '%" . $value . "%' ");
        }
        
        if ($value = $this->getParamAdapter()->getValueOfFilter('fechaEmision')) {
            $query->where("fechaEmision like '%" . $value . "%' ");
        }
        
        if ($value = $this->getParamAdapter()->getValueOfFilter('serie')) {
            $query->where("serie like '%" . $value . "%' ");
        }

        if ($value = $this->getParamAdapter()->getValueOfFilter('correlativo')) {
            $query->where("correlativo like '%" . $value . "%' ");
        }

        if ($value = $this->getParamAdapter()->getValueOfFilter('razonSocialCliente')) {
            $query->where("razonSocialCliente like '%" . $value . "%' ");
        }
        
        if ($value = $this->getParamAdapter()->getValueOfFilter('numeroDocCliente')) {
            $query->where("numeroDocCliente like '%" . $value . "%' ");
        }
    }
}

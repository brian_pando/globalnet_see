<?php
namespace Reportes\TableConfig;

use ZfTable\AbstractTable;

class Resumen extends AbstractTable
{
    protected $config = array(
        'showPagination' => true,
        'showQuickSearch' => false,
        'showItemPerPage' => true,
        'itemCountPerPage' => 20,
        'showColumnFilters' => true,
        'valuesOfItemPerPage' => array(10, 20, 25, 50, 100, 200),
    );

    /**
     * @var array Definition of headers
     */
    protected $headers = array(
        'documentoResumenId' => array('title' => 'Id', 'width' => 30) ,
        'codigoIdentificador' => array('title' => 'Código','width' => 120, 'filters' => 'text'),
        'fechaEmision' => array('title' => 'F. Emision','width' => 120,'filters' => 'text'),
        'fechaDocumentoResumen' => array('title' => 'F. Doc Resumen','width' => 120,'filters' => 'text'),
        'estadoSunat' => array('title' => 'Sunat','width' => 100),
    );

    public function init()
    {
        $this->getHeader('documentoResumenId')->getCell()->addClass('text-center');
        $this->getHeader('codigoIdentificador')->getCell()->addClass('text-center');
        $this->getHeader('fechaEmision')->getCell()->addClass('text-center');
        $this->getHeader('fechaDocumentoResumen')->getCell()->addClass('text-center');
        $this->getHeader('estadoSunat')->getCell()->addClass('text-center');
    }

    protected function initFilters($query)
    {
        if ($value = $this->getParamAdapter()->getValueOfFilter('fechaEmision')) {
            $query->where("fechaEmision like '%" . $value . "%' ");
        }
        
        if ($value = $this->getParamAdapter()->getValueOfFilter('identificadorResumen')) {
            $query->where("identificadorResumen like '%" . $value . "%' ");
        }

        if ($value = $this->getParamAdapter()->getValueOfFilter('correlativoResumen')) {
            $query->where("correlativoResumen like '%" . $value . "%' ");
        }
    }
}

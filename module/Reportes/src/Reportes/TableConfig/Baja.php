<?php
namespace Reportes\TableConfig;

use ZfTable\AbstractTable;

class Baja extends AbstractTable
{
    protected $config = array(
        'showPagination' => true,
        'showQuickSearch' => false,
        'showItemPerPage' => true,
        'itemCountPerPage' => 15,
        'showColumnFilters' => true,
    );

    /**
     * @var array Definition of headers
     */
    protected $headers = array(
        'documentoBajaId' => array('title' => 'Id', 'width' => 30) ,
        'codigoIdentificador' => array('title' => 'Serie','width' => 120, 'filters' => 'text'),
        'fechaEmision' => array('title' => 'F. Emision','width' => 120,'filters' => 'text'),
        'fechaDocumentoBaja' => array('title' => 'F. Doc Resumen','width' => 120,'filters' => 'text'),
        'estadoSunat' => array('title' => 'Sunat','width' => 100),
    );

    public function init()
    {

    }

    protected function initFilters($query)
    {
        if ($value = $this->getParamAdapter()->getValueOfFilter('fechaEmision')) {
            $query->where("fechaEmision like '%" . $value . "%' ");
        }
        
        if ($value = $this->getParamAdapter()->getValueOfFilter('fechaDocumentoBaja')) {
            $query->where("fechaDocumentoBaja like '%" . $value . "%' ");
        }
        
        if ($value = $this->getParamAdapter()->getValueOfFilter('codigoIdentificador')) {
            $query->where("codigoIdentificador like '%" . $value . "%' ");
        }
    }
}

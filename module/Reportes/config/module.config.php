<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Reportes\Controller\Reportes'  => 'Reportes\Controller\ReportesController',
            'Reportes\Controller\Facturas'  => 'Reportes\Controller\FacturasController',
            'Reportes\Controller\Boletas'   => 'Reportes\Controller\BoletasController',
            'Reportes\Controller\Notas'     => 'Reportes\Controller\NotasController',
            'Reportes\Controller\Resumenes' => 'Reportes\Controller\ResumenesController',
            'Reportes\Controller\Bajas'     => 'Reportes\Controller\BajasController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'reportes' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/reportes',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Reportes\Controller',
                        'controller'    => 'Reportes',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/][:action[/:id]]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'         => '[0-9]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Reportes' => __DIR__ . '/../view',
        ),
    ),
);

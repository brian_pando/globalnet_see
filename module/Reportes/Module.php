<?php
namespace Reportes;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Reportes\Model\CompElectTable;
use Reportes\Model\DocResumenTable;
use Reportes\Model\DocBajaTable;
use Reportes\Model\CompElectDetalleTable;
use Reportes\Model\EmpresaTable;
use Reportes\Model\UbigeoTable;

class Module implements AutoloaderProviderInterface
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                )
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Reportes\Model\CompElectTable' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new CompElectTable($dbAdapter);
                    return $table;
                },
                'Reportes\Model\CompElectDetalleTable' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new CompElectDetalleTable($dbAdapter);
                    return $table;
                },
                'Reportes\Model\DocResumenTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new DocResumenTable($dbAdapter);
                    return $table;
                },
                'Reportes\Model\DocBajaTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new DocBajaTable($dbAdapter);
                    return $table;
                },
                'Reportes\Model\EmpresaTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new EmpresaTable($dbAdapter);
                    return $table;
                },
                'Reportes\Model\UbigeoTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new UbigeoTable($dbAdapter);
                    return $table;
                }
            ),
            'aliases' => array(
                'zfdb_adapter' => 'Zend\Db\Adapter\Adapter',
            ),
        );
    }
}

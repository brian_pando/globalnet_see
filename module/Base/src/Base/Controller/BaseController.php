<?php

namespace Base\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class BaseController extends AbstractActionController
{
    public function indexAction(){
        $view = new ViewModel();
        $view->setVariables(array('active' => 'inicio'));
        $this->layout('layout/sfe.phtml');
        return $view;
    }
    
}

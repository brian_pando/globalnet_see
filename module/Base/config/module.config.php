<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Base\Controller\Base' => 'Base\Controller\BaseController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'base' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/inicio',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Base\Controller',
                        'controller'    => 'Base',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Base' => __DIR__ . '/../view',
        ),
    ),
);
